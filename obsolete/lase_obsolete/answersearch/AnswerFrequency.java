package lase.answersearch;

public class AnswerFrequency implements Comparable<AnswerFrequency>{
	private Answer answer;
	private int frequency;
	
	public AnswerFrequency(Answer answer) {
		this.answer = answer;
		this.frequency = 1;
	}
	
	public void increaseCounter() {
		frequency++;
	}
	
	public void increaseCounter(int number) {
		frequency += number;
	}
	
	public int getFrequency() {
		return frequency;
	}
	
	public Answer getAnswer() {
		return answer;
	}
	
	// FIXME compareTo by mělo vracet nulu pouze v případě, že equals vrací
	// true. Jenomže to by v tomto případě byl nesmysl. Místo implementace 
	// Comparable bude třeba použít samostaný Comaparator.
	public int compareTo(AnswerFrequency o) {
		if (frequency > o.frequency)
			return 1;
		else if (frequency < o.frequency)
			return -1;
		else 
			return 0;
	}
	
	public String toString() {
		return answer.toString() + " (" + frequency + ")";
	}
}
