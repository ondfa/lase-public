package lase_obsolete.template;

import java.util.ArrayList;

import lase.template.QueryToken;
import lase.util.StringsUtils;


public class QueryDeprecX extends ArrayList<QueryToken> {
	private static final long serialVersionUID = 1L;

	public QueryDeprecX() {
		
	}
	
	public QueryDeprecX(int initialCapacity) {
		super(initialCapacity);
	}
	
	public String toString() {
		return String.format("Query[%s]", StringsUtils.concat(this, ","));
	}
}
