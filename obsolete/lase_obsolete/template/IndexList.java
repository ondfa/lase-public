/**
 * 
 */
package lase.template;

import java.util.Arrays;
import java.util.Iterator;

public class IndexList implements Iterable<Integer>{
	private int[] list;
	private int size;
	
	IndexList() {
		this(1);
	}
	
	void add(int newIndex) {
		ensureCapaticy(size + 1);
		list[size++] = newIndex;
	}
	
	public int get(int index) {
		return list[index];
	}
	
	public IndexList(int initialCapacity) {
		list = new int[initialCapacity];
	}
	
	public void ensureCapaticy(int minCapacity) {
		if (list.length < minCapacity)
			list = Arrays.copyOf(list, minCapacity);
	}
	
	public void trimToSize() {
		if (list.length > size)
			list = Arrays.copyOf(list, 2 * size);
	}

	@Override
	public Iterator<Integer> iterator() {
		return new Iterator<Integer>() {
			private int position;
			
			@Override
			public boolean hasNext() {
				return position < size;
			}

			@Override
			public Integer next() {
				return list[position++]; 
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		};
	}
}