package lase_obsolete.questiontrans;

/**
 * Výjimka <code>TransformerException</code> je vyhozena v případě, že při
 * volání metody {@link QuestionTransformer#transform(String)} dojde k chybě a
 * převod otázka na začátek odpovědi není z nějakého důvodu možné dokončit.
 * 
 * @author Rudolf Šíma
 * 
 */
public class TransformerException extends Exception {
	private static final long serialVersionUID = 1L;

	public TransformerException() {
	}

	public TransformerException(String message, Throwable cause) {
		super(message, cause);
	}

	public TransformerException(String message) {
		super(message);
	}

	public TransformerException(Throwable cause) {
		super(cause);
	}
}
