package lase_obsolete.websearch.download;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Manažer vícevláknového stahování stránek z HTTP. Umožňuje průběžné přidávání stránek
 * a vyzvedávání již stažených. 
 * 
 * @author Rudolf Šíma
 *
 */
public class DownloadManager {
	/** maximální čas vymezený pro stažení jedné stránky */
	private int timeout;
	/** odkazy čekající na stažení */
	private BlockingQueue<String> waitingLinks = new LinkedBlockingQueue<String>();
	/** fronta již stažených stránek */
	private BlockingQueue<String> finishedPages = new LinkedBlockingQueue<String>();
	/** seznam odkazů, které se nepodařilo stáhnout */
	private List<String> unreachableLinks = new ArrayList<String>();
	/** počet odkazů, které zbývá stáhnout */
	private AtomicInteger linksToGo = new AtomicInteger(0);
	/**
	 * Callback pro stahovací vlákno. Po dostažení stránky přidá její obsah do
	 * fronty stažených stránek.
	 */
	private DownloadThreadCallback downloadThreadCallback = new DownloadThreadCallback() {
		/**
		 * Přidá obsah střánky do fronty stažených stránek.
		 */
		public void downloadFinished(String pageContent) {
			int ltg = linksToGo.decrementAndGet(); 
			if (pageContent == null) {
				// kvůli odblokování fronty při selhání posledního stahování
				if (ltg == 0)
					queuePut(finishedPages, "");
			} else {
				// standardní přidání do fronty stažených stránek 
				queuePut(finishedPages, pageContent);
			}
		}
	};
	
	/**
	 * Vytvoří nový stahovací manažer.
	 * 
	 * @param timeout
	 *            - maximální doba vymezená pro pokus o stažení jedné stránky.
	 */
	public DownloadManager(int timeout) {
		this.timeout = timeout;
	}
	
	/**
	 * Přidá další odkaz do fronty stránek čekajících na stažení. Stránka se
	 * začne stahovat hned, jak to bude možné a její obsah je potom možné získat
	 * voláním metody <code>getNextPage()</code>.
	 * 
	 * @param link
	 *            - URL stránky, která má býť stažena.
	 */
	public void addLink(String link) {
		linksToGo.incrementAndGet();
		queuePut(waitingLinks, link);
		runDownloadThread();
	}
	
	/**
	 * Vytvoří a spustí další stahovací vlákno pro novou stránku.
	 */
	private void runDownloadThread() {
		// Dočasné vlákno sloužící pouze k vybrání prvku z blokující fronty bez
		// zablokování volajícího vlákna. 
		// Není to úplně ideální řešení, ale funguje a režie vytvoření vlákna
		// navíc je ve srovnání se stažením stránky zanedbatelná. Bylo by vhodné
		// to přepsat na čitelnější kód.
		Runnable starter  = new Runnable() {
			public void run() {
				String waitingLink = null;
				try {
					waitingLink = waitingLinks.take();
					// Teprve tady se spustí vlastní stahovací vlákno.
					new DownloadThread(waitingLink, timeout, downloadThreadCallback).start();
				} catch (Exception e) {
					if (waitingLink != null)
						unreachableLinks.add(waitingLink);
					//finishedPages.add(null);
				}
			}
		};
		new Thread(starter).start();
	}

	/**
	 * Pokusí se vrátit další staženou stránku nebo se zablokuje, pokud se na
	 * stažení ještě čeká.
	 * 
	 * @return Vrací obsah stažené stránky nebo null, pokud již byly staženy
	 *         všechny stránky, které bylo možné stáhnout.
	 */
	public String getNextPageContent() {
		try {
			String result = finishedPages.take();
			if (linksToGo.get() == 0)
				return null;
			return result;
		} catch (InterruptedException e) {
			return null;
		}
	}
	
	/**
	 * Vloží prvek do blokující fronty. Ignoruje
	 * <code>InterruptedException</code>, protože volající vlákno nikdo
	 * přerušovat nehodlá.
	 * 
	 * @param queue
	 *            - fronta
	 * @param element
	 *            - prvek, který má být přidán.
	 */
	private void queuePut(BlockingQueue<String> queue, String element) {
		try {
			queue.put(element);
		} catch (InterruptedException e) {
			e.printStackTrace();
			assert false;
		}
	}
}

