package lase_obsolete.websearch.download;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import lase_obsolete.websearch.utils.HTTPClient;

/**
 * Vlákno provádějící stažení stránky z Internetu. Po dostažení stránky je její obsah předán
 * zpětným voláním metody {@link DownloadThreadCallback#downloadFinished(String)}.
 * 
 * @author Rudolf Šíma
 *
 */
class DownloadThread extends Thread {
	/** URL stránky, která má být stažena. */
	private URL url;
	/** Maximální doba vymezená na pokus o stažení stránky */
	private int timeout;
	/** Reference an objekt, kterému je zpětným voláním předán stažený obsah */
	private DownloadThreadCallback callback;
	/** Proměnná, do které je uložena výjimka {@link IOException}, pokud nastala. */
	private IOException ioException;
	
	/**
	 * Vytvoří nové stahovací vlákno připravené ke stažení stránky dané
	 * parametrem <code>url</code>.
	 * 
	 * @param url
	 *            - URL stránky, které má být stažena.
	 * @param timeout
	 *            - Maximální doba vymezená na pokus o stažení stránky
	 * @param callback
	 *            - Reference an objekt, kterému je zpětným voláním předán
	 *            stažený obsah.
	 *            
	 * @throws MalformedURLException
	 *             Vyhozena v případě, že identifikátor URL není platný.
	 */
	public DownloadThread(String url, int timeout,
			DownloadThreadCallback callback) throws MalformedURLException {
		this.url = new URL(url);
		this.timeout = timeout;
		this.callback = callback;
		this.setDaemon(true);
	}
		
	/**
	 * Provede stažení stránky a předání jejího obsahu zpětným voláním. Pokud se stahování
	 * nezdaří z důvodu chyby nebo vyprší <code>timeout</code>, předá zpětným voláním 
	 * <code>null</code>.
	 */
	public void run() {
		String pageContent = null;
		try {
			pageContent = HTTPClient.download(url, timeout);			
		} catch (IOException e) {
			synchronized(this) {
				ioException = e;
			}
		}
		callback.downloadFinished(pageContent);
	}
	
	/**
	 * Vrací výjimku {@link IOException}, pokud při stahování nastala (a to
	 * kvůli ní bylo přerušeno) nebo <code>null</code>, poukud žádná nenastala.
	 * 
	 * @return Vrací výjimku {@link IOException}, pokud při stahování nastala
	 *         nebo <code>null</code>, poukud žádná nenastala.
	 */
	public synchronized IOException getIOException() {
		return ioException;
	}
	

}
