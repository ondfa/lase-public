package lase.morphology;

import java.util.Arrays;
import java.util.Collection;

import lase.app.SharedObjects;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 * Test lematizátoru..
 * 
 * @author Rudolf Šíma
 *
 */
@RunWith(value=Parameterized.class)
public class StandardLemmatizerTest {
	Lemmatizer lematizer;
	String expectedOutput;
	String lematizerInput;
	
	public StandardLemmatizerTest(String lematizerInput, String expectedOutput) {
		this.expectedOutput = expectedOutput;
		this.lematizerInput = lematizerInput;
	} 
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		BasicConfigurator.configure();
		Logger.getRootLogger().setLevel(Level.WARN);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		lematizer = new StandardLemmatizer(SharedObjects.getMorphologicalAnalyser());
	}

	@After
	public void tearDown() throws Exception {
		lematizer.close();
		lematizer = null;
	}
	
	@Parameters
	public static Collection<Object[]> parameters() {
		return Arrays.asList(new Object[][] {
			{"Zkouška lematizátoru", "zkouška lematizátor" }	
		});
	}
	
	@Test
	public void test() throws Exception {
		String lematizerOutput = lematizer.lematize(lematizerInput);
		Assert.assertEquals(expectedOutput, lematizerOutput);
	}
}
