package lase.testing;

import java.util.*;

import lase.answersearch.AbstractAnswerMatcher;
import lase.entrec.NamedEntityGraph;
import lase.entrec.NamedEntityGraph.Node;
import lase.template.QueryToken;
import lase.template.SearchPattern;
import lase.template.Token.Modifier;

public class EntityGraphSearchTest {
	private static int markColor = 1;
	
	void runTest() throws Exception {
		SearchPattern pattern = initPattern();
		NamedEntityGraph graph = initGraph();
		
		Set<Integer> indices = new HashSet<Integer>();
		indices.add(0);
		
		search(graph.getRootNode(), pattern, indices, new ArrayList<Node>());
	}
	
	private static void search(Node root, SearchPattern pattern, Set<Integer> indices, List<Node> history) {
		history.add(root);
		if (indices.isEmpty()) {
			System.out.println("found: " + history);
		}
		for (int index : indices) {
			List<NodeOccurance> occurrences = findOccurrences(root, pattern
					.get(index), markColor++, 2);
			for (NodeOccurance no : occurrences) {
				System.out.println(no.node + " " + history);
				Set<Integer> newIndices = findFollowSet(pattern, index);
				search(no.node, pattern, newIndices, new ArrayList<Node>(
						history));
			}
		}
	}
	
	private static SearchPattern initPattern() {
		QueryToken qt;
		List<QueryToken> qtList = new ArrayList<QueryToken>();
		qt = new QueryToken("b");
		qtList.add(qt);
//		qt = new QueryToken("g");
//		qt.getToken().addModifier(Modifier.OPT);
//		qtList.add(qt);
		qt = new QueryToken("l");
		qt.getToken().addModifier(Modifier.OPT);
		qtList.add(qt);
		qt = new QueryToken("f");
		qtList.add(qt);
		qt = new QueryToken("h");
		qtList.add(qt);
		SearchPattern pattern = new SearchPattern(qtList);
		return pattern;
	}

	private static NamedEntityGraph initGraph() {
		@SuppressWarnings("unused")
		Node a,b1,b2,g1,g2,f1,f2,f3,l1,l2,h1;
		NamedEntityGraph gr = new NamedEntityGraph();

		a = gr.getRootNode();
		
		b1 = gr.appendNode(a, new QueryToken("b"));
		g1 = gr.appendNode(a, new QueryToken("g"));
		b2 = gr.appendNode(a, new QueryToken("b"));
		
		g2 = gr.appendNode(b1, new QueryToken("g"));
		gr.connectNodes(g1, g2);
		l1 = gr.appendNode(g1, new QueryToken("l"));
		l2 = gr.appendNode(b2, new QueryToken("l"));
		gr.connectNodes(g1, l2);
		
		f3 = gr.appendNode(g2, new QueryToken("f"));
		f1 = gr.appendNode(g2, new QueryToken("f"));
		gr.connectNodes(g2, f1);
		f2 = gr.appendNode(l2, new QueryToken("f"));
		
		h1 = gr.appendNode(f1, new QueryToken("h"));
		
		return gr;
	}
	
	private static Set<Integer> findFollowSet(SearchPattern pattern, int index) {
		Set<Integer> followSet = new HashSet<Integer>();
		while(++index < pattern.size() && pattern.get(index).getToken().isOptional()) {
			followSet.add(index);
		}
		if (index < pattern.size())
			followSet.add(index);
		return followSet;
	}
	
	private static class NodeOccurance {
		Node node;
		int distance;

		public NodeOccurance(Node node, int distance) {
			this.node = node;
			this.distance = distance;
		}
	}
	
	private static List<NodeOccurance> findOccurrences(Node root, QueryToken nodeContent, int markColor, int distanceLimit) {
		List<NodeOccurance> occurrences = new ArrayList<NodeOccurance>();
		Queue<NodeOccurance> queue = new ArrayDeque<NodeOccurance>();
		queue.offer(new NodeOccurance(root, 0));
		while(!queue.isEmpty()) {
			NodeOccurance occ = queue.poll();
			if (occ.node.getContent() != null) {
				if (AbstractAnswerMatcher.tokensMatch(nodeContent, occ.node.getContent())) {
					occurrences.add(occ);
				}
			}
			for (Node succ : occ.node.getSuccessors()) {
				if (occ.distance < distanceLimit && succ.getColor() != markColor) {
					succ.setColor(markColor);
					queue.offer(new NodeOccurance(succ, occ.distance + 1));
				}
			}
		}
		return occurrences;
	}
}
