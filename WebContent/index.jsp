<%@page contentType="text/html; charset=UTF-8" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<%@page import="java.util.List"%>
<%@page import="lase.servlet.AnswerBeanList" %>
<%@page import="lase.servlet.AnswerBean" %>
<%@page import="lase.servlet.ResultsBean" %>

<jsp:include page="header.jsp" />

<script language="javascript">
function explain(divId, aId) {
	var expl = document.getElementById(divId);
	var link = document.getElementById(aId);
	if (expl.style.visibility != 'visible') {
		expl.style.visibility = 'visible';
		expl.style.height = 'auto';
		link.innerHTML = "skrýt vysvětlení";
	} else {
		expl.style.visibility = 'hidden';
		expl.style.height = '0px';
		link.innerHTML = "vysvětlit";
	}
}
</script>

<c:if test="${results.valid}">
<div class="answer">
<c:choose>
	<c:when test="${empty results}">
	Odpověď na otázku "<c:out escapeXml="true" value="${results.question}" />" nebyla nalezena.
	</c:when>
<c:otherwise>
	<p style="color:gray">Otázka: <c:out escapeXml="true" value="${results.question}" /></p>
	<div class="grey">Odpověď: <strong><c:out escapeXml="true" value="${results.bestAnswer.shortAnswer}" /></strong>
	<a href="${results.bestAnswer.sourceUrl}">zdroj</a><span class="linksep"> | </span><a href="javascript:explain('bestAnsExpl', 'bestAnsExplLink')" id="bestAnsExplLink">vysvětlit</a><br/>
	<div id="bestAnsExpl" class="explanation">
		<c:forEach var="a" items="${results.bestGroup}">
		<a href="${a.sourceUrl}">
		<c:out value="${a.longAnswer}"></c:out><br/>
		</a>
		</c:forEach>
	</div>
	<span style="font-size:8pt"><c:out escapeXml="true" value="${results.bestAnswer.longAnswer}"/></span>
	</div>
	<input type="button" value="správně" onclick="location.href='rate?q=${results.question}&a=${results.bestAnswer.shortAnswer}&r=T'">
	<input type="button" value="špatně" onclick="location.href='rate?q=${results.question}&a=${results.bestAnswer.shortAnswer}&r=F'">
	<input type="button" value="nevím" onclick="location.href='rate?q=${results.question}&a=${results.bestAnswer.shortAnswer}&r=0'">
	<c:if test="${fn:length(results) gt 1}">
	<hr/>
	<div style="color:gray;">Další nalezené odpovědi</div>
	<div style="margin-left:50px">
	
	<c:forEach var="answerList" items="${results}" varStatus="status">
		<c:if test="${other}">
		<c:out escapeXml="true" value="${answerList.bestAnswer.shortAnswer}" /> <a href="${answerList.bestAnswer.sourceUrl}">zdroj</a><span class="linksep"> | </span><a href="javascript:explain('bestAnsExpl${status.count}', 'bestAnsExplLink${status.count}')" id="bestAnsExplLink${status.count}">vysvětlit</a><span class="linksep"> | </span><a href="rate?q=${results.question}&a=${answerList.bestAnswer.shortAnswer}&r=OT">Tato odpověď je správná</a><br/>
		<div id="bestAnsExpl${status.count}" class="explanation">
			<c:forEach var="a" items="${answerList}">
				<a href="${a.sourceUrl}">
				<c:out value="${a.longAnswer}"></c:out><br/>
				</a>
			</c:forEach>
		</div>
		</c:if>
		<c:set var="other" value="true" />
	</c:forEach>
	</div>
	</c:if>
</c:otherwise>
</c:choose>
</div>
</c:if>

<c:choose>
<c:when test="${not empty results.question}">

<!-- <div style="margin-left:120px"> -->
<!-- <iframe src="http://www.google.cz/search?q="${results.question}" hspace="100"></iframe> -->
<iframe src="http://www.bing.com/search?q=${results.question}" hspace="100" ></iframe>
<!-- </div> -->

</c:when>
<c:otherwise>

<div id="main">
	<div class="text toLeft">
	<h2>Ukázkové otázky / Sample questions</h2>
	<p/>
	<ul style="float: left;">
		<li><a href="?q=Kdy se narodil Karel IV.?" class="question">
		Kdy se narodil Karel IV.?</a><br/>
		<span class="i">When Karel IV. was born?</span>
		</li>

		<li><a href="./?q=Kdy byl založen Microsoft?" class="question">
		Kdy byl založen Microsoft?</a><br/>
		<span class="i">When was Microsoft founded?</span>
		</li>

		<li><a href="?q=Kdo byl první prezident USA?" class="question">
		Kdo byl první prezident USA?</a><br/>
		<span class="i">Who was the first president of USA?</span>
		</li>

		<li><a href="?q=Kde se nachází Kolumbie?" class="question">
		Kde se nachází Kolumbie?</a><br/>
		<span class="i">Where is the Country of Columbia?</span>
		</li>

		<li><a href="?q=Co je to autoportrét?" class="question">
		Co je to autoportrét?</a><br/>
		<span class="i">What does it mean autoportrait?</span>
		</li>

		<li><a href="?q=Kdy zemřel Jan Hus?" class="question">
		Kdy zemřel Jan Hus?</a><br/>
		<span class="i">When did Jan Hus die?</span>
		</li>

		<li><a href="?q=Kdo je nejlepší kuchař?" class="question">
		Kdo je nejlepší kuchař?</a><br/>
		<span class="i">Who is the best cook?</span>
		</li>

		<li><a href="?q=kde je O2 arena?" class="question">
		Kde je 02 Arena?</a><br/>
		<span class="i">Where is situated 02 Arena?</span>
		</li>

		<li><a href="?q=Kdo objevil Ameriku?" class="question">
		Kdo objevil Ameriku?</a><br/>
		<span class="i">Who discovered America?</span>
		</li>

	</ul>
	<ul style="float: right;position: relative; right: 80px;">
		<li><a href="?q=Kde je Eiffelova věž?" class="question">
		Kde je Eiffelova věž?</a><br/>
		<span class="i">Where is the Eiffel Tower?</span>
		</li>

		<li><a href="?q=Kdo byl prezidentem ČSR?" class="question">
		Kdo byl prezidentem ČSR?</a><br/>
		<span class="i">Who was the president of ČSR?</span>
		</li>

		<li><a href="?q=Kdy se narodil Karel Gott?" class="question">
		Kdy se narodil Karel Gott?</a><br/>
		<span class="i">When was Karel Gott born?</span>
		</li>

		<li><a href="?q=Kde je Budapešť?" class="question">
		Kde je Budapešť?</a><br/>
		<span class="i">Where is Budapest?</span>
		</li>

		<li><a href="?q=Kdo vynalezl dynamit?" class="question">
		Kdo vynalezl dynamit?</a><br/>
		<span class="i">Who invented dynamit?</span>
		</li>

		<li><a href="?q=Kdy vznikla Česká republika?" class="question">
		Kdy vznikla Česká republika?</a><br/>
		<span class="i">When was Czech Republic independent?</span>
		</li>

		<li><a href="?q=Kde sídlí NATO" class="question">
		Kde sídlí NATO?</a><br/>
		<span class="i">Where is NATO?</span>
		</li>

		<li><a href="?q=Kdo je prezidentem USA?" class="question">
		Kdo je prezidentem USA?</a><br/>
		<span class="i">Who is the president of USA?</span>
		</li>

		<li><a href="?q=Kdy se narodil Barack Obama?" class="question">
		Kdy se narodil Barack Obama?</a><br/>
		<span class="i">When was Barack Obama born?</span>
		</li>

	</ul>
	<div style="clear:both;"></div>
	</div>
	</div>


</c:otherwise>
</c:choose>

</body>
</html>