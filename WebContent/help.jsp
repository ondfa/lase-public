<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<jsp:include page="header.jsp" />

	<div class="text">
	<h1>Nápověda</h1>

	<h2>Funkce</h2><p/>
	LASE (Lingvo Answer Search Engine) je aplikace pro hledání odpovědí na internetu
	na dotazy kladené v přirozeném jazyce.<p/>
	Po zadání dotazu do vyhledávacího pole se aplikace pokusí
	vyhledat odpovědi a zobrazit je uživateli v okně prohlížeče.<p/>

	<h2>Otázky</h2><p/>
	LASE je navržen k vyhledávání otázek v českém jazyce začínajících na slova:
	<ul>
		<li>Kdo</li>
		<li>Kde</li>
		<li>Kdy</li>
		<li>Co</li>
		<li>Proč</li>
	</ul>
	Na jinak položené otázky nedokáže vyhledat odpověď. V případě, že ani na správně
	položenou otázku nenajde odpověď, pokuste se o jinou formulaci otázky.<p/>
	Pro zpřesnění odpovědi je vhodné klást otázky spíše konkrétně než obecně, například
	je vhodnější zadat <span class="i">"Kde leží Paříž"</span> než <span class="i">"Kde je Paříž"</span>.<p/>

	<h2>Hodnocení</h2><p/>
	U každé nalezené odpovědi jsou zobrazena tři tlačítka pro vyhodnocení Vašeho
	názoru na správnost odpovědi. Váš názor nám pomůže při ladění a zlepšování
	aplikace.

	</div>
</body>
</html>