-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Počítač: 127.0.0.1
-- Vygenerováno: Úte 29. dub 2014, 21:27
-- Verze serveru: 5.5.32
-- Verze PHP: 5.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databáze: `lase2`
--
CREATE DATABASE IF NOT EXISTS `lase2` DEFAULT CHARACTER SET utf8 COLLATE utf8_czech_ci;
USE `lase2`;

-- --------------------------------------------------------

--
-- Struktura tabulky `answer_rating`
--

CREATE TABLE IF NOT EXISTS `answer_rating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(45) COLLATE utf8_czech_ci DEFAULT NULL,
  `answer` varchar(45) COLLATE utf8_czech_ci DEFAULT NULL,
  `rating` varchar(45) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=14 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `cache`
--

CREATE TABLE IF NOT EXISTS `cache` (
  `key` int(11) NOT NULL,
  `cache` longblob NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
