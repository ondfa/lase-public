package lase.morphology;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import lase.cache2.Cache;
import lase.cache2.CacheResult;
import lase.util.ArrayWrapper;
import cz.zcu.fav.liks.morphology.client.MorphologyRPCWebServiceClient;
import cz.zcu.fav.liks.morphology.data.MorphologicAnalyser;
import cz.zcu.fav.liks.morphology.data.MorphologyAnalysisException;

/**
 * Morfologický analyzátor využívající webovou službu morfologické analýzy
 * poskytovanou LIKS.
 * 
 * @author Rudolf Šíma
 * 
 */
public class LiksWebServiceMorphologicalAnalyser implements MorphologicalAnalyser {
	
	/**
	 * Morfologický analyzátor z knihovny webové služby.	
	 */
	private MorphologicAnalyser analyser;
	
	/**
	 * Vytvoří nový morfologický analyzátor a nastaví URL webové služby.
	 * 
	 * @param url
	 *            - URL webové služby morfologické analýzy.
	 *            
	 * @throws IOException
	 *             Vyhozena, pokud při inicializaci klienta webové služby
	 *             nastala chyba.
	 */
	public LiksWebServiceMorphologicalAnalyser(String url) throws IOException {
		analyser = new  MorphologyRPCWebServiceClient(url);
	}
	
	/* (non-Javadoc)
	 * @see lase.morphology.MorphologicalAnalyser#analyse(java.util.List)
	 */
	@Override
	public List<MorphWord> analyse(List<String> text) throws MorphologyException {
		return analyse(text.toArray(new String[text.size()]));
	}
	
	/* (non-Javadoc)
	 * @see lase.morphology.MorphologicalAnalyser#analyse(java.lang.String[])
	 */
	@SuppressWarnings("unchecked")
	public List<MorphWord> analyse(String[] text) throws MorphologyException {
		List<cz.zcu.fav.liks.morphology.data.MorphWord> words;
		Cache cache = Cache.getCache(analyser.getClass(), "analyseSentence", String[].class);
		try {
			CacheResult cr = cache.get(new ArrayWrapper<String>(text));
			if (cr.cacheHit()) {
				words = (List<cz.zcu.fav.liks.morphology.data.MorphWord>) cr.getValue();
			} else {
				words = analyser.analyseSentence(text);
				cr.updateCache(words);
			}
		} catch (MorphologyAnalysisException e) {
			throw new MorphologyException(e);
		}
		
		List<MorphWord> result = new ArrayList<MorphWord>(words.size());
		for (cz.zcu.fav.liks.morphology.data.MorphWord w : words) {
			// Slovní druh určuje první písmeno v tagu
			char posTag = w.getTag().charAt(0);
			MorphWord resWord = new MorphWord(w.getWord(), w.getLemma(), posTag);
			result.add(resWord);
		}
		return result;
	}
	
	/**
	 * Zavře morfologický analyzátor. V případě
	 * {@link LiksWebServiceMorphologicalAnalyser} tato metoda neprovádí žádnou
	 * akci.
	 */
	@Override
	public void close() throws IOException {
	}
	
	@Override
	public List<MorphWord> analyse(String text) throws MorphologyException {
		throw new UnsupportedOperationException("Not implemented");
	}
}
