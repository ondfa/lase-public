package lase.morphology;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * Optimalizovaná obdoba třídy {@link SimpleTokenizer} oproti které pracuje
 * přibližné dvakrát rychleji.
 * </p>
 * 
 * <p>
 * Tokenizace řetězců v systému LASE není z hlediska rychlosti kritická, takže
 * je prakticky jedno, zda se použije {@link SimpleTokenizer} nebo
 * <code>FastSimpleTokenizer</code>. Tato třída byla původně vytvořena k jinému
 * účelu.
 * </p>
 * 
 * @author Rudolf Šíma
 * 
 */
public class FastSimpleTokenizer extends SimpleTokenizer {

	/**
	 *	Množina oddělovačů tokenizátoru
	 */
	private static final boolean delimSet[] = new boolean['~' + 1];
	static {
		for (byte c = ' '; c <= '~'; c++) {
			if (!Character.isLetterOrDigit(c)) {
				delimSet[c] = true;
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public List<String> tokenize(String text) {
		int len = text.length();
		List<String> tokenList = new ArrayList<String>();
		CharAppender tokenBuilder = new CharAppender();
		for (int i = 0; i < len; i++) {
			char c = text.charAt(i);
			if (c < delimSet.length && delimSet[c]) {
				if (tokenBuilder.length() > 0) {
					tokenList.add(tokenBuilder.flushToString());
				}
				if (c != ' ')
					tokenList.add(String.valueOf(c));
			} else {
				tokenBuilder.append(c);
			}
		}
		return tokenList;
	}
	
	
	/**
	 * Rychlejší alternativa k třídě {@link StringBuilder} použitelná
	 * v případě, že se na konec řetězce znaky přidávají vždy po jednom.
	 * 
	 * @author Rudolf Šíma
	 *
	 */
	private static class CharAppender {
		
		/**
		 * Buffer, do kterého se přidávají znaky.
		 */
		private char[] chars = new char[64];
		
		/**
		 * Délka řetězce, tj. počet platných znaků v bufferu.
		 */
		private int length = 0;
		
		/**
		 * Přidá do bufferu jeden znak.
		 * 
		 * @param c
		 *            - Přidávaný znak.
		 */
		public void append(char c) {
			if (length >= chars.length)
				chars = Arrays.copyOf(chars, chars.length * 2);
			chars[length++] = c;
		}
		
		/**
		 * Vrátí kopii řetězce v bufferu a vyprázndní buffer.
		 * 
		 * @return Vrací kopii řetězce v bufferu.
		 */
		public String flushToString() {
			String str = String.valueOf(chars, 0, length);
			length = 0;
			return str;
		}
		
		/**
		 * Vrací kopii řetězce v bufferu.
		 */
		public String toString() {
			return String.valueOf(chars, 0, length);
		}
		
		/**
		 * Vrací délku řetězce v bufferu.
		 * 
		 * @return Vrací délku řetězce.
		 */
		public int length() {
			return length;
		}
	}
}
