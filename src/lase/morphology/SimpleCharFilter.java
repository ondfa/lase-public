package lase.morphology;


/**
 * Slouží k nahrazení znaků sémantickými ekvivalenty. Například všechny délky
 * pomlček musí být sjednoceny.
 * 
 * Toto je pouze jednoduchá verze nahrazující některé znaky. Pokud se problém
 * sémantické ekvivalence ukáže jako závažný, bude nutné implementovat
 * sjednocení ekvivalentních znaků, jak je definováno ve standardu Unicode.
 * 
 * @author Rudolf Šíma
 * 
 */
public class SimpleCharFilter extends CharFilter {
	
	public String substitute(String original) {
		StringBuilder sb = new StringBuilder(original.length());
		boolean changed = false;
		int length = original.length();
		for (int i = 0; i < length; i++) {
			char c = original.charAt(i);
			char newC = c;
			if (c == '–' || c == '—' || c == '―') {
				changed = true;
				newC = '-';
			}
			sb.append(newC);
		}
		if (changed)
			return sb.toString();
		return original;
	}
}
