package lase.morphology;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * pro jednu větu simuluje činnost morfologického analyzátoru
 * @author Ondřej Pražák
 * @since 2.0
 *
 */
public class FakeMorphologicalAnalyser implements MorphologicalAnalyser {

	@Override
	@Deprecated
	public List<MorphWord> analyse(String[] text) throws MorphologyException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<MorphWord> analyse(List<String> text)
			throws MorphologyException {
		List<MorphWord> list = new ArrayList<MorphWord>();
		if(text.get(0).equals("Kdy"))
		{
			list.add(new MorphWord("Kdy", "kdy", 'A'));
			list.add(new MorphWord("se", "se", 'P'));
			list.add(new MorphWord("narodil", "narodit", 'W'));
			list.add(new MorphWord("Josef", "josef", 'N'));
			list.add(new MorphWord("Čapek", "čapek", 'N'));
		}
		else
		{
			list.add(new MorphWord("Josef", "josef", 'N'));
			list.add(new MorphWord("Čapek", "čapek", 'N'));
			list.add(new MorphWord("se", "se", 'P'));
			list.add(new MorphWord("narodil", "narodit", 'W'));
			list.add(new MorphWord("5", "5", 'C'));
			list.add(new MorphWord(".", ".", 'C'));
			list.add(new MorphWord("1", "1", 'C'));
			list.add(new MorphWord(".", ".", 'C'));
			list.add(new MorphWord("1890", "1890", 'C'));
		}
		return list;
	}

	@Override
	public void close() throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public List<MorphWord> analyse(String text) throws MorphologyException {
		throw new UnsupportedOperationException("Not implemented");
	}

}
