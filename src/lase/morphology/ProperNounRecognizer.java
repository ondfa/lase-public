package lase.morphology;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import lase.util.StringUtils;
import lase.util.SubarraysGenerator;

@Deprecated
public class ProperNounRecognizer {


	public ProperNounRecognizer() {
	}
	
	@Deprecated
	public static class ProperNoun extends ArrayList<MorphWord>{
		private static final long serialVersionUID = 1L;
		private String wordsCache;
		private String lemmasCache;
		private int wordCacheModCount;
		private int lemmaCacheModCount;
		private int position;

		public ProperNoun(int position, MorphWord... names) {
			super(3);
			this.position = position;
			for (MorphWord name : names) {
				add(name);
			}
		}
		
		public int getPosition() {
			return position;
		}
		
		public String toString() {
			return StringUtils.concat(this, " ");
		}

		public String getWords() {
			if (wordCacheModCount != modCount) {
				StringBuilder sb = new StringBuilder();
				for (MorphWord mw : this) {
					sb.append(mw.getWord());
					sb.append(" ");
				}
				wordsCache = sb.toString().trim();
				wordCacheModCount = modCount;
			}
			return wordsCache;
		}

		public Object getLemmas() {
			if (lemmaCacheModCount != modCount) {
				StringBuilder sb = new StringBuilder();
				for (MorphWord mw : this) {
					sb.append(mw.getWord());
					sb.append(" ");
				}
				lemmasCache = sb.toString().trim();
				lemmaCacheModCount = modCount;
			}
			return lemmasCache;
		}
	}
	
	public List<ProperNoun> analyse(List<MorphWord> morphWords) {
		
		List<MorphWord> candidate = new ArrayList<MorphWord>();
		List<ProperNoun> properNouns = new ArrayList<ProperNoun>();
		int index = 0;
		boolean prevRoman = false;
		for (MorphWord mw : morphWords) {
			String word = mw.getWord();
			if (prevRoman && ".".equals(word)) {
				candidate.add(mw);
			} else {
				if (isCandidate(mw)) {
					candidate.add(mw);
				} else {
					flushCandidate(candidate, properNouns, index);
				}
			}
			prevRoman = isRomanNumeral(word);
			index++;
		}
		flushCandidate(candidate, properNouns, index);
		return properNouns;
	}
	
	@SuppressWarnings("unused")
	private boolean containsRomanNumeral(List<MorphWord> morphWords) {
		for (MorphWord mw : morphWords) {
			if (isRomanNumeral(mw.getWord()))
				return true;
		}
		return false;
	}
	
	@SuppressWarnings("unused")
	private List<MorphWord> fixRomanNumerals(List<MorphWord> morphWords) {
		List<MorphWord> newMorphWords = new ArrayList<MorphWord>(morphWords.size());
		ListIterator<MorphWord> it = morphWords.listIterator();
		while(it.hasNext()) {
			MorphWord mw = it.next();
			if (isRomanNumeral(mw.getWord())) {
				if (it.hasNext()) {
					MorphWord peekMw = it.next();
					if (".".equals(peekMw.getWord())) {
						newMorphWords.add(new MorphWord(mw.getWord() + peekMw.getWord(), 
								mw.getLemma() + peekMw.getLemma(), mw.getPosTag()));
					} else {
						newMorphWords.add(mw);
						newMorphWords.add(peekMw);
					}
				}
			} else {
				newMorphWords.add(mw);
			}
		}
		return newMorphWords;
	}

	private static boolean isRomanNumeral(CharSequence word) {
		// zjednodušené, zatím bohatě stačí
		for (int i = 0; i < word.length(); i++) {
			switch(word.charAt(i)) {
			case 'I':
			case 'V':
			case 'X':
			case 'L':
			case 'C':
			case 'D':
			case 'M':
				break;
			default:
				return false;
			}
		}
		return true;
	}
	
	@SuppressWarnings("unused")
	private static boolean isRomanOrdinal(CharSequence word) {
		int len = word.length();
		return len >= 2 && '.' == word.charAt(len-1) && isRomanNumeral(word.subSequence(0, len-1)); 
	}
	
	private static boolean isCandidate(MorphWord mw) {
		String word = mw.getWord();
		int length = word.length() ;
		if (length <= 0) {
			assert false;
			return false;
		}
		if (mw.getPosTag() == 'C' && isRomanNumeral(word))
			return true;
		// test, zda je první písmeno velké, ostatní malá
		if (!Character.isUpperCase(word.charAt(0)))
			return false;
		for (int i = 1; i < length; i++) {
			if (Character.isUpperCase(word.charAt(i)))
				return false;
		}
		return true;
	}

	
	private static  void flushCandidate(List<MorphWord> candidate, List<ProperNoun> properNouns, int index) {
		if (candidate.isEmpty())
			return;
		int position = index - candidate.size();
		List<ProperNoun> newProperNouns = createProperNoun(position, candidate);
		properNouns.addAll(newProperNouns);
		candidate.clear();
	}
	
	private static List<ProperNoun> createProperNoun(int position, List<MorphWord> candidate) {
		List<ProperNoun> result = new ArrayList<ProperNoun>();
		// Delší jméno než Jean Claude Van Damme snad nikdo nemá.
		SubarraysGenerator subarrGen = new SubarraysGenerator(2, 4);
		Object[] canArr = candidate.toArray();
		List<Object[]> subArrays = subarrGen.generate(canArr);
		for (Object[] arr : subArrays) {
			MorphWord[] mwArr = new MorphWord[arr.length];
			System.arraycopy(arr, 0, mwArr, 0, arr.length);
			
			int offset = refIndexOf(canArr, arr[0]);
			
			//workaround -- zatím velmi dobře fungující
			if (!".".equals(mwArr[0].getWord())) {
				if (!(isRomanNumeral(mwArr[0].getWord()) && ".".equals(mwArr[1].getWord()))) {
					ProperNoun pn = new ProperNoun(position + offset, mwArr);			
					result.add(pn);
				}
			}
		}
		return result;
	}
	
	private static int refIndexOf(Object[] haystack, Object needle) {
		int len = haystack.length;
		for (int i = 0; i < len; i++) {
			if (needle == haystack[i])
				return i;
		}
		return -1;
	}
	
//	public static void main(String[] argv) throws Exception {
//		String s = "Je autorem tohoto textu je Rudolf Zkouška Test Šíma?";
//		SharedObjects.initializeDefaults();
//		MorphologicalAnalyser analyser = SharedObjects.getMorphologicalAnalyser();
//		List<MorphWord> mws = analyser.analyse(Tokenizer.tokenize(s));
//		ProperNounRecognizer pnr = new ProperNounRecognizer();
//		for(ProperNoun pn : pnr.analyse(mws)) {
//			System.out.println(pn);
//			System.out.println(pn.getPosition());
//			System.out.println(pn.size());
//		}
//	}

//	public List<ProperNoun> analyse(String sentence, boolean thisMethodIsBackupOnly) throws MorphologyException {
//		List<MorphWord> words = analyser.analyse(sentence);
//		List<ProperNoun> results = new ArrayList<ProperNoun>(1);
//		
//		MorphWord prev = null;
//		for (MorphWord word : words) {
//			if (word.getWordType() == 'N') {
//				char firstChar = word.getWord().charAt(0);
//				if (Character.isUpperCase(firstChar)) {
//					if (prev != null) {
//						ProperNoun name = new ProperNoun(prev.getLemma(), word.getLemma());
//						results.add(name);
//					}
//					prev = word;
//				}
//			}
//			if (prev != word)
//				prev = null;
//		}
//		return results;
//	}
}
