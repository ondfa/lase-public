package lase.morphology;

import lase.util.LaseException;

/**
 * Výjimka <code>MorphologyException</code> je vyhozena v případě, že
 * morfologický analyzátor, který implementuje rozhraní
 * {@link MorphologicalAnalyser}, vyhodil výjimku. Tuto původní výjimku by mělo
 * být možné získa voláním metody <code>getCause()</code>, pokud byl lematizátor
 * implementován správně.
 * 
 * @author Rudolf Šíma
 * 
 */
public class MorphologyException extends LaseException {
	private static final long serialVersionUID = 5897993260149900613L;

	/**
	 * Vytvoří novou výjimku bez podrobné zprávy a nastavení příčiny.
	 */
	public MorphologyException() {
		super();
	}

	/**
	 * Vytvoří novou výjimku, nastaví podrobnou zprávu a příčinu.
	 */
	public MorphologyException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Vytvoří novou výjimku a nastaví podrobnou zprávu.
	 */
	public MorphologyException(String message) {
		super(message);
	}

	/**
	 * Vytvoří novou výjimku a nastaví příčinu.
	 */
	public MorphologyException(Throwable cause) {
		super(cause);
	}
}
