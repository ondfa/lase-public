package lase.morphology;

import java.io.IOException;
import java.util.List;

/**
 * <p>
 * Rozhraní lematizátoru textu.
 * <p>
 * 
 * <p>
 * Slouží k převodu všech slov v textu na základní tvar.
 * ("tento být příklad text po správný lematizace")
 * </p>
 * 
 * @author Rudolf Šíma
 * 
 */
public interface Lemmatizer {

	/**
	 * Provede lematizaci všech slov textu a vrátí výsledný text.
	 * 
	 * @param text
	 *            - Slova (tokeny) textu, který je předmětem lemmatizace.
	 * 
	 * @return Vrací seznam lemmat slov zadaných parametrem <code>text</code>.
	 * 
	 * @throws MorphologyException
	 *             Vyhozena v případě, že lematizace skončí chybou. Výjimka,
	 *             který tuto chybu způsobila je nastavena jako příčina (
	 *             <code>cause</code>) výjimky <code>MorphologyException</code>.
	 */
	@Deprecated
	public List<String> lematize(String[] text) throws MorphologyException;

	/**
	 * Provede lematizaci všech slov textu a vrátí výsledný text.
	 * 
	 * @param text
	 *            - Slova (tokeny) textu, který je předmětem lemmatizace.
	 * 
	 * @return Vrací seznam lemmat slov zadaných parametrem <code>text</code>.
	 * 
	 * @throws MorphologyException
	 *             Vyhozena v případě, že lematizace skončí chybou. Výjimka,
	 *             který tuto chybu způsobila je nastavena jako příčina (
	 *             <code>cause</code>) výjimky <code>MorphologyException</code>.
	 */
	public List<String> lematize(List<String> tokens) throws MorphologyException;
	
	/**
	 * Zavře lematizátor. Instance se od volání metody <code>close</code> stane
	 * nepoužitelnou.
	 */
	public void close() throws IOException;
}
