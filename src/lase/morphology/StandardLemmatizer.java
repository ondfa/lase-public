package lase.morphology;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Zjednodušené rozhraní morfologického analyzátoru, který plní funkci
 * lemmatizátoru. (Lemmata zadaných slov jsou jedním z výsledů analýzy
 * morfologickým analyzátorem.)
 * 
 * @author Rudolf Šíma
 * 
 */
public class StandardLemmatizer implements Lemmatizer {
	/** 
	 * Morfologický analyzátor.
	 */
	MorphologicalAnalyser analyser;
	
	/**
	 * Vytvoří nový lematizátor.
	 * 
	 * @param analyser
	 *            - morfologický analyzátor, který se při lematizaci použije.
	 */
	public StandardLemmatizer(MorphologicalAnalyser analyser) {
		this.analyser = analyser;
	}
	
	/**
	 * Provede lemmatizaci zadaného textu (seznamu slov) a vrátí seznamu lemmat
	 * odpovídající slovům. Délka vstupního i výstupního seznamu tedy musí být
	 * stejná.
	 */
	@Override
	public List<String> lematize(List<String> text) throws MorphologyException {
		return lematize(text.toArray(new String[text.size()]));
	}
	
	/**
	 * Provede lemmatizaci zadaného textu (seznamu slov) a vrátí seznamu lemmat
	 * odpovídající slovům. Délka vstupního i výstupního seznamu tedy musí být
	 * stejná.
	 */
	@Deprecated
	public List<String> lematize(String[] text) throws MorphologyException {
		List<MorphWord> analyserResult = analyser.analyse(text);
		List<String> lemmatizerResult = new ArrayList<String>(analyserResult.size());
		for (MorphWord mw : analyserResult) {
			lemmatizerResult.add(mw.getLemma());
		}
		return lemmatizerResult;
	}
	
	/**
	 * Volá metodu <code>close()</code> použitého morfologického analyzátoru.
	 */
	public void close() throws IOException {
		analyser.close();
	}
}
