package lase.learning;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import lase.answersearch.Countable;
import lase.answersearch.SizeComparator;
import lase.app.SharedObjects;
import lase.entrec.NamedEntityGraph;
import lase.entrec.NamedEntityRecognitionException;
import lase.entrec.NamedEntityRecognizer;
import lase.entrec.NamedEntityGraph.Node;
import lase.template.Query;
import lase.template.QueryToken;
import lase.template.ReferenceToken;
import lase.template.SearchPattern;
import lase.template.Token;
import lase.template.Token.Modifier;
import lase.util.ArrayIterator;
import lase.util.Corpus;
import lase.util.DebugGraphWriter;
import lase.util.GraphMergingTool;

/**
 * Testovací implementace učicího algoritmu.
 *
 * @author Rudolf Šíma
 * 
 */
public class TestingLearningAlgorithm extends AbstractLearningAlgorithm {

	@Override
	protected List<SearchPattern> retrievePatterns(LearningCorporaBundle learningCorporaBundle) throws LearningException {
		
		List<MergedGraph> mergedGraphs = createMergedGraphs(learningCorporaBundle);
		
		List<AnswerPathRecord> answerPaths = new ArrayList<AnswerPathRecord>();
		for (MergedGraph graph : mergedGraphs) {
			answerPaths.add(new AnswerPathRecord(findAnswerPaths(graph), graph.getQuestion()));
		}
		if (answerPaths.size() != 2)
			throw new AssertionError();
		
		AnswerPathRecord firstPaths = answerPaths.get(0);
		AnswerPathRecord secondPaths = answerPaths.get(1);
		
		List<NodePair> nodePairs = new ArrayList<NodePair>();
		for (AnswerPath fAp : firstPaths) {
			for (AnswerPath sAp : secondPaths) {
				NodePair np = findCommonPredecessors(fAp.getFirst(), sAp.getFirst());
				if (np != null) {
					nodePairs.add(np);
				}
			}
		}

		if(nodePairs.isEmpty())
			return Collections.emptyList();
		Collections.sort(nodePairs, new SizeComparator(true));
		Set<SearchPattern> searchPatterns = createSearchPatterns(nodePairs, firstPaths.question, secondPaths.question);
		return new ArrayList<SearchPattern>(searchPatterns);
	}
	
	private class AnswerPathRecord implements Iterable<AnswerPath> {
		public AnswerPathRecord(List<AnswerPath> answerPath, Query question) {
			this.answerPath = answerPath;
			this.question = question;
		}
		List<AnswerPath> answerPath;
		Query question;
		
		@Override
		public Iterator<AnswerPath> iterator() {
			return answerPath.iterator();
		}
	}

	private Set<SearchPattern> createSearchPatterns(List<NodePair> nodePairs, Query q1, Query q2) {
		Set<SearchPattern> searchPatterns = new HashSet<SearchPattern>();
		for (NodePair np : nodePairs) {
			List<QueryToken> spTokens = new ArrayList<QueryToken>();
			NodePair x = np;
			while(x != null) {
				QueryToken qt;
				QueryToken qt1 = x.n1.getContent();
				QueryToken qt2 = x.n2.getContent();
				Token token;
				int ref = searchQuestion(q1, qt1);
				if (ref >= 0) {
					token = new ReferenceToken(ref+1);
					qt = new QueryToken(token, null, null);
				} else {
					if (qt1.getOriginalWord().equals(qt2.getOriginalWord())) {
						qt = qt1;
					} else {
						token = qt1.getToken();
						qt = new QueryToken(token, null, null);
					}
				}
				spTokens.add(qt);
				x = x.prev;
			}
			searchPatterns.add(new SearchPattern(spTokens));
		}
		return searchPatterns;
	}
	
	private static int searchQuestion(Query question, QueryToken qt1) {
		for (int i = 0; i < question.size(); i++) {
			QueryToken qt = question.get(i);
			if (qt.typeEquals(qt1) && qt.subtypeEquals(qt1) && qt.getSemanticValue().equals(qt1.getSemanticValue())) {
				return i;
			}
		}
		return -1;
	}

	private static NodePair findCommonPredecessors(Node n1, Node n2) {
		int longestLength = 0;
		NodePair longest = null;
		
		Deque<NodePair> stack = new ArrayDeque<NodePair>();
		stack.push(new NodePair(n1, n2, null));
		while(!stack.isEmpty()) {
			NodePair np = stack.pop();
			for (Node p1 : np.n1.getPredecessors()) {
				for (Node p2 : np.n2.getPredecessors() ) {
					if (areSimilarNodes(p1, p2)) {
						stack.push(new NodePair(p1, p2, np));
					} else {
						if (np.length > longestLength) {
							longestLength = np.length;
							longest = np;
						}
					}
				}
			}
		}
		return longest;
	}
	
	private static class NodePair implements Countable {
		public NodePair(Node n1, Node n2, NodePair prev) {
			this.n1 = n1;
			this.n2 = n2;
			this.prev = prev;
			if (prev == null)
				length = 0;
			else
				length = prev.length + 1;
		}
		int length;
		Node n1;
		Node n2;
		NodePair prev;
		
		@Override
		public int size() {
			return length;
		}
		
		@Override
		public String toString() {
			return String.format("(%s|%s)", n1, n2);
		}
	}

	private static boolean areSimilarNodes(Node p1, Node p2) {
		QueryToken qt1 = p1.getContent();
		QueryToken qt2 = p2.getContent();
		if (qt1 == null || qt2 == null)
			return false;
		if (qt1.typeEquals(qt2))
		{
			Token t1 = qt1.getToken();
			Token t2 = qt2.getToken();
			if (t1.getType() == Token.Type.NAMED_ENTITY) {
				return t1.subtypeEquals(t2);
			} else if (t1.getType() == Token.Type.WORD) {
				return qt1.getSemanticValue().equals(qt2.getSemanticValue());
			}
		}
		return false;
	}
	
	private static List<AnswerPath> findAnswerPaths(MergedGraph graph) {
		List<AnswerPath> answerPaths = new ArrayList<AnswerPath>();
		Deque<StackRecord> stack = new ArrayDeque<StackRecord>();
		stack.push(new StackRecord(0, graph.getRootNode()));
		Query answer = graph.getAnswer();
		if (answer.size() != 1)
			throw new AssertionError("Testing implementation.");
		
		while(!stack.isEmpty()) {
			StackRecord sr = stack.pop();
			
			QueryToken qt = answer.get(0);
			if (sr.node != graph.getRootNode()) {
				if (compareNodeWithToken(sr.node, qt)) {
					sr.node.getContent().getToken().addModifier(Modifier.ANSWER);
					answerPaths.add(new AnswerPathAdapter(sr.node));
				}
			}
			
			for (Node successor : sr.node.getSuccessors()) {
				if (!graph.isMarked(successor)) {
					graph.markNode(successor);
					stack.push(new StackRecord(0, successor));
				}
			}
		}
		
		return answerPaths;
	}
	
	private static boolean compareNodeWithToken(Node n, QueryToken qt) {
		QueryToken qt1 = n.getContent();
		if (qt.typeEquals(qt1)) {
			Token t = qt.getToken();
			Token t1 = qt1.getToken();
			if (t.getSubtype() == null && t1.getSubtype() == null
					|| t.subtypeEquals(t1)) {
				if (qt.getSemanticValue().equals(qt1.getSemanticValue())) {
					return true;
				}
			}
		}
		return false;
	}
	
	private static List<MergedGraph> createMergedGraphs(LearningCorporaBundle learningCorporaBundle) throws LearningException {
		NamedEntityRecognizer recognizer = SharedObjects.getNamedEntityRecognizer();
		List<MergedGraph> mergedGraphs = new ArrayList<MergedGraph>(learningCorporaBundle.size());
		for (LearningCorporaBundle.Entry bundleEntry : learningCorporaBundle) {
			GraphMergingTool graphMergingTool = new GraphMergingTool(bundleEntry.getQuestion(), bundleEntry.getAnswer());  
			for (Corpus.Entry corpusEntry : bundleEntry.getCorpus()) {
				try {
					NamedEntityGraph graph = recognizer.analyse(corpusEntry.getText());
					graphMergingTool.addGraph(graph);
				} catch (NamedEntityRecognitionException e) {
					throw new LearningException(e.getMessage(), e);
				}
			}
			mergedGraphs.add(graphMergingTool.getMergedGraph());
			DebugGraphWriter.write(graphMergingTool.getMergedGraph());
		}
		return mergedGraphs;
	}

	private static class StackRecord {
	
		public StackRecord(int tokenIndex, Node node) {
			//this.tokenIndex = tokenIndex;
			this.node = node;
		}
		//int tokenIndex;
		Node node;
	}
	
	private static interface AnswerPath extends Iterable<Node> {
		public Node getFirst();
		public Node getLast();
	}
	
	private static class AnswerPathAdapter implements AnswerPath {
		private Node node;
		
		public AnswerPathAdapter(Node n) {
			this.node = n;
		}

		@Override
		public Iterator<Node> iterator() {
			return new ArrayIterator<Node>(new Node[] {node});
		}

		@Override
		public Node getFirst() {
			return node;
		}

		@Override
		public Node getLast() {
			return node;
		}
	}
}
