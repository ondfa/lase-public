package lase.learning;

import lase.entrec.NamedEntityGraph;
import lase.template.Query;

public class MergedGraph extends NamedEntityGraph {

	private Query question;
	private Query answer;

	public MergedGraph(Query question, Query answer) {
		this.question = question;
		this.answer = answer;
	}
	
	public MergedGraph(Query question, Query answer, NamedEntityGraph graph) {
		this.setRootNode(graph.getRootNode());
		this.answer = answer;
	}
	
	public Query getAnswer() {
		return answer;
	}
	
	public Query getQuestion() {
		return question;
	}
}
