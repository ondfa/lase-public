package lase.answersearch;

import java.util.List;

@Deprecated
public interface CorpusTokenizer {

	public boolean hasNext();

	public List<String> next();

}