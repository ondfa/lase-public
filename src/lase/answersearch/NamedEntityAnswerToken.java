package lase.answersearch;

import lase.template.NamedEntityToken;
import lase.template.NamedEntityToken.Subtype;
import lase.util.LaseCalendar;

/**
 * Token odpovědi obsahující pojmenovanou entitu určenou podtypem a sémantickou
 * hodnotou, tj. objektem nesoucím hodnotu příslušné pojmenované entity. Tedy
 * např. pro datum je to instance třídy {@link LaseCalendar}, pro geografický
 * název instance třídy {@link String} obsahující lemma názvu, atd...
 * 
 * @author Rudolf Šíma
 * 
 * @see AnswerToken
 * @see NamedEntityToken
 * @see NamedEntityToken.Subtype
 * 
 */
public class NamedEntityAnswerToken extends AnswerToken {
	
	/**
	 * Podtyp pojmenované entity.
	 */
	private Subtype subtype;
	
	
	/**
	 * Sémantická hodnota tokenu.
	 */
	private Object sematicValue;

	/**
	 * Vytvoří a inicializuje nový token odpovědi s pojmenovanou entitou.
	 * 
	 * @param subtype
	 *            - Podtypo pojmenované entity.
	 * @param value
	 *            - Původní text pojmenované entity z korpusu.
	 * @param semanticValue
	 *            - Sémantická hodnota tj. objekt nesoucí hodnotu příslušné
	 *            pojmenované entity. Tedy např. pro datum je to instance třídy
	 *            {@link LaseCalendar}, pro geografický název instance třídy
	 *            {@link String} obsahující lemma názvu, atd...
	 */
	public NamedEntityAnswerToken(Subtype subtype, String value, Object semanticValue) {
		super(Type.NAMED_ENTITY, value);
		this.subtype = subtype;
		this.sematicValue = semanticValue;
		
	}

	/**
	 * Vrací sémantickou hodnotu pojmenované entity.
	 * 
	 * @return Vrací sémantickou hodnotu.
	 */
	public Object getSematicValue() {
		return sematicValue;
	}

	/**
	 * Vrací podtyp pojmenované entity.
	 * 
	 * @return Vrací podtyp pojmenované entity.
	 */
	public Subtype getSubtype() {
		return subtype;
	}
	
	/**
	 * Vrací řetězcem reprezentovaný obsah tokenu pro účely ladění.
	 */
	public String toString() {
		return String.format("NamedEntityAnswerToken[Subype:%s,Value:%s]", subtype, getText());
	}

	/* (non-Javadoc)
	 * @see lase.answersearch.AnswerToken#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((sematicValue == null) ? 0 : sematicValue.hashCode());
		result = prime * result + ((subtype == null) ? 0 : subtype.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see lase.answersearch.AnswerToken#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		/* 
		 * Pozor, chybí: if (!super.equals(obj)) return false;
		 * Pro dosažení rovnosti není požadována rovnost předka, protože tam 
		 * je equals implementováno dost nevhodně. Tento přístup je funguje, 
		 * ale nebylo od věci to celé přepsat.
		 */
		if (getClass() != obj.getClass())
			return false;
		NamedEntityAnswerToken other = (NamedEntityAnswerToken) obj;
		if (sematicValue == null) {
			if (other.sematicValue != null)
				return false;
		} else if (!sematicValue.equals(other.sematicValue))
			return false;
		if (subtype == null) {
			if (other.subtype != null)
				return false;
		} else if (!subtype.equals(other.subtype))
			return false;
		return true;
	}
}
