package lase.answersearch;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.RandomAccess;

import lase.util.Corpus.SourcePage;

/**
 * <p>
 * Odpověď nalezená systémem LASE. Skládá se z tokenů {@link AnswerToken}.
 * Tokeny mohou být buď {@link TextAnswerToken} nebo
 * {@link NamedEntityAnswerToken}.
 * </p>
 * 
 * <p>
 * Kromě tokenů nalezené odpověďi nese odpověď informaci o webové stránce, odkud
 * informace pochází ({@link SourcePage})
 * <p>
 * 
 * @author Rudolf Šíma
 * 
 */
public class Answer implements Iterable<AnswerToken> {
	
	/**
	 * Seznam tokenů, ze kterých se odpověď skládá.
	 */
	private List<AnswerToken> answerTokens;
	
	/**
	 * Stránka, ze které odpověď pochází.
	 */
	private SourcePage sourcePage;
	
	/**
	 * Část textu, ze kterého byla odpověď získána. Například text snippetu
	 * webového vyhledávače.
	 */
	private String context;
	
	
	/**
	 * Cache krátké textové podoby odpovědi, tj. zřetězení textu všech tokenů
	 * odpovědi oddělených mezerami.
	 */
	private String shortAnswerCache;

	/**
	 * <p>
	 * Vytvoří novou odpověď ze seznamu tokenů. 
	 * </p>
	 * 
	 * <p>
	 * <strong>Poznámka:</strong> Pokud
	 * je seznam tokenů instancí rozhraní {@link RandomAccess}, seznam se
	 * nekopíruje. V opačném případě se vytvoří nový seznam typu
	 * {@link ArrayList}, do kterého se tokeny zkopírují.
	 * </p>
	 * 
	 * @param answerTokens
	 *            - Seznam tokenů odpovědi.
	 * @param source
	 *            - Stránka, ze které informace pochází.
	 * @param context
	 *            - Část textu stránky, kterého byla odpověď získána. Např. text
	 *            snippetu webového vyhledávače.
	 */
	public Answer (List<AnswerToken> answerTokens, SourcePage source, String context) {
		this(answerTokens, source);
		this.context = context;
	}
	
	/**
	 * <p>
	 * Vytvoří novou odpověď ze seznamu tokenů. 
	 * </p>
	 * 
	 * <p>
	 * <strong>Poznámka:</strong> Pokud
	 * je seznam tokenů instancí rozhraní {@link RandomAccess}, seznam se
	 * nekopíruje. V opačném případě se vytvoří nový seznam typu
	 * {@link ArrayList}, do kterého se tokeny zkopírují.
	 * </p>
	 * 
	 * @param answerTokens
	 *            - Seznam tokenů odpovědi.
	 * @param source
	 *            - Stránka, ze které informace pochází.
	 */
	public Answer (List<AnswerToken> answerTokens, SourcePage source) {
		if (!(answerTokens instanceof RandomAccess))
			answerTokens = new ArrayList<AnswerToken>(answerTokens);

		this.answerTokens = Collections.unmodifiableList(answerTokens);
		this.sourcePage = source;
	}
	
	/**
	 * Vrací token odpovědi na pozici <code>index</code>.
	 * 
	 * @param index
	 *            - pozice tokenu v odpovědi (první je 0).
	 *            
	 * @return Vrací token odpovědi.
	 */
	public AnswerToken getToken(int index) {
		return answerTokens.get(index);
	}

	/**
	 * Vrací informace o zdrojové stránce, ze které byla odpověď získána.
	 * 
	 * @return Vrací informace o zdrojové stránce, ze které byla odpověď
	 *         získána.
	 */
	public SourcePage getSource() {
		return sourcePage;
	}
	
	/**
	 * Vrací část textu, ze kterého byla odpověď získána. Například text snippetu
	 * webového vyhledávače.
	 * 
	 * @return Vrací část textu, ze kterého byla odpověď získána.
	 */
	public String getContext() {
		return context;
	}

	/**
	 * Vrací počet tokenů v odpovědi.
	 * 
	 * @return Vrací počet tokenů v odpovědi.
	 */
	public int size() {
		return answerTokens.size();
	}
	
	/**
	 * Vrací text tokenů odpovědi spojený mezerami.
	 */
	public String toString() {
	if (shortAnswerCache == null) {
			StringBuilder sb = new StringBuilder();
			int size = answerTokens.size();
			for (int i = 0; i < size; i++) {
				if (i > 0)
					sb.append(" ");
				sb.append(answerTokens.get(i).getText());
			}
			shortAnswerCache = sb.toString();
	}
	return shortAnswerCache;
}

	/* (non-Javadoc)
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<AnswerToken> iterator() {
		return answerTokens.iterator();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((answerTokens == null) ? 0 : answerTokens.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Answer other = (Answer) obj;
		if (answerTokens == null) {
			if (other.answerTokens != null)
				return false;
		} else if (!answerTokens.equals(other.answerTokens))
			return false;
		return true;
	}

	/**
	 * Zjistí shodnost dvou odpovědí bez ohledu na velikost písmen. Požadován je
	 * stejný počet tokenů, stejné typy a podtypy jednotlivých tokenů, textová
	 * shodnost jednotlivých tokenů bez ohledu na velikost písmen.
	 * 
	 * @param other
	 *            - Odpověď, se kterou bude tato porovnána.
	 * 
	 * @return Vrací <code>true</code>, jsou-li odpověďi vyhodnoceny jako
	 *         shodné. Jinak vrací <code>false</code>.
	 */
	public boolean equalsIgnoreCase(Answer other) {
		if (answerTokens == null) {
			return other.answerTokens == null;
		}
		int otherSize = other.size();
		if (otherSize != size())
			return false;
		for (int i = 0; i < otherSize; i++) {
			AnswerToken token = getToken(i);
			AnswerToken otherToken = other.getToken(i);
			if (token.getType() != otherToken.getType())
				return false;
			String text = token.getText();
			String otherText = otherToken.getText();
			if (text == null) {
				/* Následující řádek dříve vypadal takto
				 * *** if (otherToken != null ***
				 * Opravil jsem otherToken na otherText, protože to tak, předpokládám,
				 * bylo myšleno. otherToken na tomto místě nemůže být null, protože
				 * by to způsobylo vyhození NullPointerException o pár řádků dřív.
				 */
				if (otherText != null)
					return false;
			} else {
				if (!text.equalsIgnoreCase(otherText))
					return false;
			}
		}
		return true;
	}
}
