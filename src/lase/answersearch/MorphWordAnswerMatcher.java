package lase.answersearch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lase.template.QueryToken;
import lase.template.SearchPattern;
import lase.template.Token;
import lase.template.Token.Type;
import lase.util.CharsForStringsSubstitution;
import lase.util.Corpus;

@Deprecated
public class MorphWordAnswerMatcher extends AbstractAnswerMatcher {

	/**
	 * Jak dlouhý může být seznam tokenů, který není typu WORD. Např. pro
	 * "21.5.1986 21:03:00" je délka 9.
	 */
	private static final int nonWordMax = 9;
	private static final String unknownPartSubstitute = String.format(".{1,%d}", nonWordMax);
	
	private SearchPattern searchPattern;
	private CorpusTokenizer tokenizer; 

	public MorphWordAnswerMatcher(Corpus corpus, SearchPattern searchPattern) {
		this.tokenizer = new MorphWordCorpusTokenizer(corpus);
		this.searchPattern = searchPattern;
	}
	
	@Override
	protected Answer findNextAnswer() {
		if (!tokenizer.hasNext())
			return null;
		
		List<String> words = tokenizer.next();

		// připojit množinu slov z vyhledávacího vzoru
		List<String> combinedWords = new ArrayList<String>(words);
		int addedWords = 0;
		for (QueryToken qt : searchPattern) {
			if (qt.getToken().getType() == Token.Type.WORD) {
				combinedWords.add(qt.getOriginalWord());
				addedWords++;
			}
		}

		CharsForStringsSubstitution strSubst = new CharsForStringsSubstitution(combinedWords);
		char[] substChars = strSubst.getCharArrayResult();
		int i = 0;

		CharArraySplitter splitArray = new CharArraySplitter(substChars, substChars.length - addedWords - 1);
		String corpusSubstStr = String.valueOf(splitArray.firstPart);

		// vytvoření vyhledávacího regulárního výrazu
		StringBuilder regexBuilder = new StringBuilder();
		for (QueryToken qt : searchPattern) {
			Token token = qt.getToken();

			String rStr;
			if (token.getType() == Type.WORD) {
				rStr = String.valueOf(splitArray.secondPart[i++]);
			} else {
				rStr = unknownPartSubstitute;
			}

			if (token.hasModifier(Token.Modifier.OPT)) {
				regexBuilder.append("(");
				regexBuilder.append(rStr);
				regexBuilder.append(")?");
			} else {
				regexBuilder.append(rStr);
			}
		}
		
		Pattern pattern = Pattern.compile(regexBuilder.toString());
		Matcher matcher = pattern.matcher(corpusSubstStr);
		
		if (matcher.find()) {
			String group = matcher.group();
			System.out.println(group);			
		}
		return null;
	}
	
	private static class CharArraySplitter {
		char[] firstPart;
		char[] secondPart;
		
		public CharArraySplitter(char[] array, int splitIndex) {
			firstPart = Arrays.copyOf(array, splitIndex + 1);
			secondPart = Arrays.copyOfRange(array, splitIndex, array.length-1);
		}
	}
	
//	public static void main(String[] argv) {
//		Corpus corpus = new Corpus();
//		corpus.addPart("a a b c a a b", "", "");
//		
//		SearchPattern sp = new SearchPattern();
//		sp.add(new QueryToken(new WordToken(0, "a")));
//		Token t = new NamedEntityToken(0, NamedEntityToken.Subtype.GEOGRAPHY);
//		t.addModifier(Modifier.OPT);
//		sp.add(new QueryToken(t, "prdel všech prdelí", null));
//		sp.add(new QueryToken(new WordToken(0, "a")));
//		
//		MorphWordAnswerMatcher matcher = new MorphWordAnswerMatcher(corpus, sp);
//		
//		while (matcher.hasNextAnswer()) {
//			Answer answer = matcher.nextAnswer();
//			System.out.println(answer);
//		}
//	}
	
	
}
