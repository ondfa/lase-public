package lase.answersearch;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import lase.util.Corpus;
import lase.util.Corpus.Entry;

@Deprecated
public class SimpleCorpusTokenizer implements CorpusTokenizer {
	private static final String delimiters;
	static {
		final char lBound  = ' ';
		final char uBound  = '~';
		StringBuilder sb = new StringBuilder(uBound - lBound); 
		for(char c = lBound; c <= uBound; c++) {
			if (!Character.isLetterOrDigit(c))
			sb.append(c);
		}
		delimiters = sb.toString();
	}
	
	public static void main(String[] asdf) {
		System.out.println(delimiters);
	}
	
	private Iterator<Entry> corpusIterator;
	
	public SimpleCorpusTokenizer(Corpus corpus) {
		this.corpusIterator = corpus.iterator();
	}
	
	@Override
	public boolean hasNext() {
		return corpusIterator.hasNext();
	}

	@Override
	public List<String> next() {
		Corpus.Entry entry = corpusIterator.next();
		String text = entry.getText();
		StringTokenizer tokenizer = new StringTokenizer(text, delimiters, true);
		List<String> tokens = new ArrayList<String>();
		while(tokenizer.hasMoreTokens()) {
			String t = tokenizer.nextToken();
			if (t.length() != 1 || !Character.isWhitespace(t.charAt(0)))
				tokens.add(t);
		}
		return tokens;
	}
}
