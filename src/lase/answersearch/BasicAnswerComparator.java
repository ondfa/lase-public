package lase.answersearch;

import static lase.util.PrecisionComparator.Precision.EQUAL;
import static lase.util.PrecisionComparator.Precision.INCOMPARABLE;
import static lase.util.PrecisionComparator.Precision.LESS_PRECISE;
import static lase.util.PrecisionComparator.Precision.MORE_PRECISE;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import lase.answersearch.AnswerToken.Type;
import lase.template.NamedEntityToken.Subtype;
import lase.util.LaseCalendar;
import lase.util.PrecisionComparator;

/**
 * Základní komparátor odpovědí podle přesnosti. Porovnání přesnosti se provádí
 * pouze s časovými údaji, u ostatních se rozhoduje pouze mezi rovností a
 * neporovnatelností.
 * 
 * @author Rudolf Šíma
 * 
 * @see PrecisionComparator
 * 
 */
public class BasicAnswerComparator implements AnswerComparator {
	private static final long serialVersionUID = 1L;

	/**
	 * Zjistí, zda odpověď obsahuje alespoň jednu pojmenovanou entitu.
	 * 
	 * @param a
	 *            - Odpověď.
	 * 
	 * @return Vrací <code>true</code>, pokud odpověď obsahuje alespoň jednu
	 *         pojmenovanou entitu. Jinak vrací <code>false</code>.
	 */
	private static boolean containsNamedEntity(Answer a) {
		for (AnswerToken at : a) {
			if (at.getType() == Type.NAMED_ENTITY)
				return true;
		}
		return false;
	}
	
	/**
	 * Pár pojmenovaných entit stejného podtypu.
	 * 
	 * @author Rudolf Šíma
	 *
	 */
	private static class NamedEntityPair {
		
		/**
		 *	První token typu pojmenovaná entity v páru.
		 */
		NamedEntityAnswerToken net1;
		
		/**
		 * První token typu pojmenovaná entity v páru.
		 */
		NamedEntityAnswerToken net2;

		/**
		 * Vytvoří a inicializuje nový pár tokenů.
		 * 
		 * @param net1
		 *            - první prvek
		 * @param net2
		 *            - druhý token
		 * 
		 * @throws IllegalAccessException
		 *             Pokud se podtyp pojmenovaných entit obou tokenů
		 *             neshoduje.
		 */
		NamedEntityPair(NamedEntityAnswerToken net1, NamedEntityAnswerToken net2) {
			if (net1.getSubtype() != net2.getSubtype())
				throw new IllegalArgumentException();
			this.net1 = net1;
			this.net2 = net2;
		}
		
		
		/**
		 * Vrací společný podtyp obou pojmenovaných entit.
		 * 
		 * @return Vrací společný podtyp obou pojmenovaných entit. 
		 */
		Subtype getSubtype() {
			return net1.getSubtype();
		}
	}

	/**
	 * <p>
	 * Vyhledá odpovídající páry pojmenovaných entit v odpovědích
	 * <code>o1</code> a </code>o2</code>, mají-li odpověďi "podobnou" strukturu.
	 * </p>
	 * </p>
	 * Odpovědi s podobnou strukturou jsou takové, které obsahují stejný počet tokenů typu
	 * pojmenovaná entita, jejichž podtypy na příslušných pozicích se shodují. Například
	 * tedy odpověďi:<br/>
	 * <ul>
	 * <li>[slovo] [slovo] [datum] [slovo] [geografický název]</li>
	 * <li>[datum] [slovo] [geografický název]</li>
	 * </ul>
	 * jsou podobné. Naopak odpovědi:
	 * <ul>
	 * <li>[geografický název] [slovo] [datum]</li>
	 * <li>[datum] [slovo] [geografický název]</li>
	 * </ul>
	 * podobné nejsou.
	 * </p>
	 * 
	 * @param o1
	 *            - první odpověď
	 * @param o2
	 *            - druhá odpověď
	 * 
	 * @return Vrací seznam odpovídajících párů pojmenovaných entit nebo
	 *         <code>null</code>, pokud odpovědi nejsou "podobné" (viz výše).
	 */
	private static List<NamedEntityPair> matchNamedEntities(Answer o1, Answer o2) {
		// Možné optimalizovat
		List<NamedEntityPair> result = new ArrayList<NamedEntityPair>();
		for (AnswerToken at1 : o1) { 
			if (at1.getType() == Type.NAMED_ENTITY) {
				NamedEntityAnswerToken neat1 = (NamedEntityAnswerToken) at1;
				boolean matched = false;
				int index = 0;
				for (AnswerToken at2 : o2) {
					index++;
					if (at2.getType() == Type.NAMED_ENTITY) {
						NamedEntityAnswerToken neat2 = (NamedEntityAnswerToken) at2;
						if (neat1.getSubtype() == neat2.getSubtype()) {
							result.add(new NamedEntityPair(neat1, neat2));
							matched = true;
						}
					}
				}
				if (!matched)
					return null; // o2 má něco navíc
			}
		}
		for (AnswerToken at2 : o2) {
			if (at2.getType() == Type.NAMED_ENTITY) {
				NamedEntityAnswerToken neat2 = (NamedEntityAnswerToken) at2;
				boolean matched = false;
				for (AnswerToken at1 : o1)  {
					if (at1.getType() == Type.NAMED_ENTITY) {
						NamedEntityAnswerToken neat1 = (NamedEntityAnswerToken) at1;
						if (neat1.getSubtype() == neat2.getSubtype()) {
							matched = true;
						}
					}
				}
				if (!matched)
					return null; // o1 má něco navíc
			}
		}
		return result;
	}

	/**
	 * Provede porovnání podle odpovědí podle přesnosti. Každý odpovídající pár
	 * pojmenovaných entit (viz {@link #matchNamedEntities(Answer, Answer})
	 * porovná podle přesnosti a čítá počet různých případů, které nastaly.
	 * Celkový výsledek porovnání se potom určí podle toho, kterých případů je
	 * víc. (Viz kód metody.)
	 * 
	 * @param o1
	 *            - První odpověď.
	 * @param o2
	 *            - Druhá odpověď.
	 * 
	 * @return Vrací výsledek porovnání odpovědí podle přesnosti.
	 */
	private static Precision compareNamedEntities(Answer o1, Answer o2) {
		List<NamedEntityPair> pairs = matchNamedEntities(o1, o2);
		if (pairs == null)
			return INCOMPARABLE;
		Set<NamedEntityAnswerToken> skipList = new HashSet<NamedEntityAnswerToken>();
		int incomparable = 0, comparisonResult = 0, equal = 0, all = 0;
		for (NamedEntityPair pair : pairs) {
			Precision precision;
			switch(pair.getSubtype())
			{
			case TIME:
			case DATE:
			case DATE_TIME:
				precision = compareDates(pair);
				break;
			default:
				precision = pair.net1.equals(pair.net2) ? EQUAL : INCOMPARABLE;
			}
			switch(precision) {
			case INCOMPARABLE:
				incomparable++;
				all++;
				break;
			case MORE_PRECISE:
				comparisonResult++;
				all++;
				break;
			case LESS_PRECISE:
				comparisonResult--;
				all++;
				break;
			case EQUAL:
				if (!skipList.contains(pair.net1)) {
					skipList.add(pair.net1);
					equal++;
					all++;
				}
			}
		}
		if (equal + incomparable == all) {
			if (equal >= incomparable)
				return EQUAL;
			else
				return INCOMPARABLE;
		} else {
			if (comparisonResult == 0)
				return EQUAL;
			else
				return comparisonResult > 0 ? MORE_PRECISE : LESS_PRECISE;
		}
	}

	/**
	 * Porovná časové údaje podle přesnosti. Např. "1. 1. 1993" je přesnější než
	 * "leden 1993" atp.
	 * 
	 * @param pair
	 *            - pár pojmenovaných entity obsahujících časové údaje.
	 *            
	 * @return Vrací výsledek porovnání podle přesnosti.
	 */
	private static Precision compareDates(NamedEntityPair pair) {
		Object o1 = pair.net1.getSematicValue();
		Object o2 = pair.net2.getSematicValue();
		
		if (o1.equals(o2))
			return EQUAL;
		
		assert o1 instanceof LaseCalendar && o2 instanceof LaseCalendar;
		LaseCalendar c1 = (LaseCalendar) o1;
		LaseCalendar c2 = (LaseCalendar) o2;
		
		//assert c1.isSet(Calendar.YEAR) && c2.isSet(Calendar.YEAR);
		
		Precision fieldResult;
		fieldResult = compareCalenarField(c1, c2, LaseCalendar.YEAR);
		if (fieldResult != EQUAL)
			return fieldResult;
		fieldResult = compareCalenarField(c1, c2, LaseCalendar.MONTH);
		if (fieldResult != EQUAL)
			return fieldResult;
		fieldResult = compareCalenarField(c1, c2, LaseCalendar.DAY);
		if (fieldResult != EQUAL)
			return fieldResult;
		fieldResult = compareCalenarField(c1, c2, LaseCalendar.HOUR);
		if (fieldResult != EQUAL)
			return fieldResult;
		fieldResult = compareCalenarField(c1, c2, LaseCalendar.MINUTE);
		if (fieldResult != EQUAL)
			return fieldResult;
		fieldResult = compareCalenarField(c1, c2, LaseCalendar.SECOND);
		if (fieldResult != EQUAL)
			return fieldResult;
		
		return EQUAL;
	}
	
	/**
	 * Porovná dva kalendáře podle přesnosti v jednom konkrétním údaji (den, měsíc, rok, hodina, minuta). 
	 * 
	 * @param c1 - první kalendář
	 * @param c2 - druhý kalendář.
	 * 
	 * @param field - údaj v kalendáři ( {@link LaseCalendar} ).
	 * 
	 * @return Vrací výsledek porovnání podle přesnosti.
	 */
	private static Precision compareCalenarField(LaseCalendar c1, LaseCalendar c2, int field) {
		if (c1.isSet(field)) {
			if (c2.isSet(field))
				return c1.get(field) == c2.get(field) ? EQUAL : INCOMPARABLE;	
			return MORE_PRECISE;
		}
		if (c2.isSet(field))
			return LESS_PRECISE;
		return EQUAL;
	}
	
	/**
	 * Provede porovnání podle odpovědí podle přesnosti. Každý odpovídající pár
	 * pojmenovaných entit (viz {@link #matchNamedEntities(Answer, Answer})
	 * porovná podle přesnosti a čítá počet různých případů, které nastaly.
	 * Celkový výsledek porovnání se potom určí podle toho, kterých případů je
	 * víc.
	 * 
	 * @param o1
	 *            - První odpověď.
	 * @param o2
	 *            - Druhá odpověď.
	 * 
	 * @return Vrací výsledek porovnání odpovědí podle přesnosti.
	 * 
	 * @see AnswerComparator
	 */
	public Precision compare(Answer o1, Answer o2) {
		boolean cne1 = containsNamedEntity(o1);
		boolean cne2 = containsNamedEntity(o1);
		
		if (cne1 != cne2)
			return INCOMPARABLE;
		
		if (cne1) {
			return compareNamedEntities(o1, o2);
		} else {
			return o1.equalsIgnoreCase(o2) ? EQUAL : INCOMPARABLE;
		}
	}
}
 