package lase.entrec;

import java.util.List;

import lase.morphology.MorphWord;

/**
 * <p>
 * Rozhraní třídy implementující rozpoznávač pojmenovaných entity nějakého podtypu
 * v předzpracovaném textu, tj. v seznam tokenů vrácených morfologickým
 * analyzátorem.
 * </p>
 * 
 * @author Rudolf Šíma
 * 
 * @see NamedEntityRecognizer
 * @see PositionedEntity
 * 
 */
public interface EntityMatcher {

	/**
	 * Vyhledá v seznamu morfologických slov (seznam tokenů vrácený
	 * morfologickým rozpoznávačem) pojmenované entity nějakého typu. (Různé
	 * implementace tohoto rozhraní se specializují na různé typy pojmenovaných
	 * entit.)
	 * 
	 * @param tokens
	 *            Seznam morfologických slov, ve kterém se vyhledává.
	 * 
	 * @return Vrací seznam nalezených pojmenovaných entit s pozicí, na které
	 *         byly nalezeny a délkou (počtem tokenů, které obsahují).
	 * 
	 * @see PositionedEntity
	 */
	public List<PositionedEntity> search(List<MorphWord> tokens);
	
}
