package lase.entrec;

import java.util.ArrayList;
import java.util.List;

import lase.util.Corpus;

@Deprecated
public class CorpusNamedEntityGraph {
	
	private List<Entry> graphs; 
	
	public CorpusNamedEntityGraph(Corpus corpus, NamedEntityRecognizer recognizer) throws NamedEntityRecognitionException {
		graphs = new ArrayList<Entry>(corpus.size());
		for (Corpus.Entry e : corpus) {
			NamedEntityGraph newGraph = recognizer.analyse(e.getText());
			graphs.add(new Entry(e, newGraph));
		}
	}
	
	public static class Entry {
		private Corpus.Entry originalEntry;
		private NamedEntityGraph graph;
		
		public Entry(Corpus.Entry originalEntry, NamedEntityGraph graph) {
			this.originalEntry = originalEntry;
			this.graph = graph;
		}

		public Corpus.Entry getOriginalEntry() {
			return originalEntry;
		}

		public NamedEntityGraph getGraph() {
			return graph;
		}
	}
	
}
