package lase.entrec;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Wrapper pole.
 * 
 * @author Rudolf Šíma
 * 
 * @param <T>
 *            Typ prvků pole.
 * 
 * @deprecated Použít {@link lase.util.ArrayWrapper}
 */
@Deprecated
public class CacheArrayWrapper<T> implements Serializable {
	private static final long serialVersionUID = 1L;

	private T[] array;
	
	public CacheArrayWrapper(T[] array) {
		this.array = array;
	}

	public T[] getArray() {
		return array;
	}

	@Override
	public int hashCode() {
		return Arrays.hashCode(array);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CacheArrayWrapper<?> other = (CacheArrayWrapper<?>) obj;
		if (!Arrays.equals(array, other.array))
			return false;
		return true;
	}
}
