package lase.entrec;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lase.morphology.MorphWord;
import lase.template.NamedEntityToken.Subtype;
import lase.util.LaseCalendar;
import lase.util.StringUtils;

/**
 * <p>
 * Rozpoznávač časových údajů. Slouží k nalezení pojmenovaných entit podtypu
 * {@code DATE}, {@code TIME} a {@code DATE_TIME} v textu resp. v seznamu
 * morfologických slov, které vrátí morfologický analyzátor.
 * </p>
 * 
 * <p>
 * Výsledkem analýzy je seznam pojmenovaných entit (s pozicí v textu) obsahující
 * nalezené časové údaje.
 * </p>
 * 
 * @author Rudolf Šíma
 * 
 */
public class DateMatcher extends NumberMatcher {
	
	/**
	 * Mapa čísel měsíců podle jejich názvu.
	 */
	private static final Map<String, Integer> months = new HashMap<String, Integer>();
	static {
		int i = 0;
		months.put("leden", ++i);
		months.put("únor", ++i);
		months.put("březen", ++i);
		months.put("duben", ++i);
		months.put("květen", ++i);
		months.put("červen", ++i);
		months.put("červenec", ++i);
		months.put("srpen", ++i);
		months.put("září", ++i);
		months.put("říjen", ++i);
		months.put("listopad", ++i);
		months.put("prosinec", ++i);
	}
	
	/**
	 * Token signalizující konec vstupní posloupnosti morfologických slov.
	 */
	private static final String endToken = "\0";
	
	/**
	 * Seznam pojmenovaných entit s pozicí, který se při analýze vytváří.
	 */
	private List<PositionedEntity> entityList;
	
	/**
	 * Seznam morfologických slov vrácený morfologickým analyzátorem.
	 */
	private List<MorphWord> originalTokenList;
	
	/**
	 * Seznam lemmat získaných ze seznamu {@link #originalTokenList} ukončený
	 * tokenem {@link #endToken}.
	 */
	private ArrayList<String> tokenList;
	
	/**
	 * Aktuální pozice z hlediska průběhu analýzu (index aktuálního tokenu).
	 */
	private int curPos;

	/**
	 * Aktuálně rozpoznávané datum a/nebo čas. Vytváří se v průběhu rozpoznávání
	 * časového údaje. Po jeho dokončení je z něj vytvořena pojmenované entita.
	 */
	private DateBuilder curDate;
	
	/**
	 * Určuje, zda bylo rozpoznávání dokončeno (vstupní text byl celý zpracován)
	 */
	private boolean finished;
	
	
	/**
	 * Historie rozpoznávání. Umožňuje zapamatovat stav, do kterého se lze vrátit.
	 * 
	 * @see #mark()
	 * @see #rollback()
	 */
	private Deque<HistoryStackEntry> history;

	/**
	 * Vrací token, který je v daném stavu rozpoznávání aktuální, tzn. leží na
	 * pozici {@link #curPos}.
	 * 
	 * @return Vrací aktuální token.
	 */
	private String getToken() {
		return tokenList.get(curPos);
	}

	/**
	 * Vrací původní slovo (původní formulaci v textu, nikoli lemma)
	 * odpovídající aktuálnímu tokenu vrácenému metodou {@link #getToken()}.
	 * 
	 * @return Vrací aktuální původní slovo.
	 */
	private String getOriginalWordForCurrentToken() {
		return originalTokenList.get(curPos).getWord();
	}

	/**
	 * Inkrementuje ukazatel na aktuální slovo, tzn. posune se o jeden token ve
	 * vstupním seznamu dopředu.
	 */
	private void readToken() {
		// inkrementuje curPos, pokud je menší než počet tokenů
		if (++curPos >= tokenList.size()) {
			curPos--;
			finished = true;
		}
	}

	/**
	 * Zapamatuje si stav rozpoznávání, do kterého se lze vrátit voláním metody
	 * {@link #rollback()}.
	 */
	private void mark() {
		history.push(new HistoryStackEntry(curPos, curDate.clone()));
	}

	/**
	 * Ze zásobníku historie vyjme poslední stav rozpoznávání zapamatovaný
	 * voláním metody {@link #mark()} a vrátí se do tohoto stavu.
	 */
	private void rollback() {
		HistoryStackEntry entry = history.pop();
		curDate = entry.markDate;
		curPos = entry.markPos;
	}

	/**
	 * Odstraní z historie poslední zapamatovaný stav rozpoznávání uložený
	 * metodou {@link #mark()}.
	 */
	private void deleteMark() {
		history.pop();
	}

	/**
	 * Metoda {@code accept()} využívaná při rekurzivním sestupu ve standardním
	 * významu. Je-li slovo akceptování, vrací {@code true} a posune se o jeden
	 * token vpřed. Jinak vrací {@code false} a ponechá aktuální pozici ve
	 * vstupním seznamu.
	 * 
	 * @param token
	 *            Token, který má být akceptován (nebo zamítnut).
	 * 
	 * @return Vrací {@code true}, je-li token akceptován. Jinak vrací {@code
	 *         false}.
	 */
	private boolean accept(String token, boolean time) {
		String ct = getToken();
		if (token.equals(ct)) {
			curDate.addWord(getOriginalWordForCurrentToken(), time);
			readToken();
			return true;
		}
		return false;
	}

	/**
	 * Akceptuje token, pokud je číslem značícím den v měsíci.
	 * 
	 * @return Vrací {@code true}, je-li token akceptován. Jinak vrací {@code
	 *         false}.
	 */
	private boolean acceptDay() {
		String ct = getToken();
		if (acceptNumber(1, 31, false)) {
			curDate.set(LaseCalendar.DAY, Integer.parseInt(ct)); 
			return true;
		}
		return false;
	}
	
	/**
	 * Akceptuje token, pokud je názvem měsíce.
	 * 
	 * @return Vrací {@code true}, je-li token akceptován. Jinak vrací {@code
	 *         false}.
	 */
	private boolean acceptMonthName() {
		String ct = getToken();
		if (months.containsKey(ct)) {
			curDate.addWord(getOriginalWordForCurrentToken(), false);
			curDate.set(LaseCalendar.MONTH, months.get(ct));
			readToken();
			return true;
		}
		return false;
	}
	
	/**
	 * Akceptuje token, pokud je číslem značícím měsíc.
	 * 
	 * @return Vrací {@code true}, je-li token akceptován. Jinak vrací {@code
	 *         false}.
	 */
	private boolean acceptMonth() {
		String ct = getToken();
		if (acceptNumber(1, 12, false)) {
			curDate.set(LaseCalendar.MONTH, Integer.parseInt(ct));
			return true;
		}
		return false;
	}
	
	/**
	 * Akceptuje token, pokud je číslem značícím rok.
	 * 
	 * @return Vrací {@code true}, je-li token akceptován. Jinak vrací {@code
	 *         false}.
	 */
	private boolean acceptYear() {
		mark();
		accept("v", false);
		if (accept("rok", false))
			deleteMark();
		else
			rollback();
		String ct = getToken();
		if (acceptNumber(0, 3000, false)) {
			curDate.set(LaseCalendar.YEAR, Integer.parseInt(ct)); 
			return true;
		}
		return false;
	}
	
	/**
	 * Akceptuje token, pokud je číslem značícím hodinu (0 až 23)
	 * 
	 * @return Vrací {@code true}, je-li token akceptován. Jinak vrací {@code
	 *         false}.
	 */
	private boolean acceptHour() {
		String ct = getToken();
		if (acceptNumber(0, 23, true)) {
			curDate.set(LaseCalendar.HOUR, Integer.parseInt(ct)); 
			return true;
		}
		return false;
	}
	
	/**
	 * Akceptuje token, pokud je číslem značícím minutu.
	 * 
	 * @return Vrací {@code true}, je-li token akceptován. Jinak vrací {@code
	 *         false}.
	 */
	private boolean acceptMinute() {
		String ct = getToken();
		if (acceptNumber(0, 60, true)) {
			curDate.set(LaseCalendar.MINUTE, Integer.parseInt(ct)); 
			return true;
		}
		return false;
	}
	
	/**
	 * Akceptuje token, pokud je číslem.
	 * 
	 * @return Vrací {@code true}, je-li token akceptován. Jinak vrací {@code
	 *         false}.
	 */
	private boolean acceptNumber(int min, int max, boolean time) {
		String ct = getToken();
		if (!isInteger(ct))
			return false;
		int val = Integer.parseInt(ct);
		if (min < max) {
			if (val < min || val > max) {
				return false;
			}
		}
		curDate.addWord(ct, time);
		readToken();
		return true;
	}

	/**
	 * Akceptuje posloupnost tokenů od aktuální pozice ({@link #curPos}), pokud
	 * je datem.
	 * 
	 * @return Vrací {@code true}, je-li posloupnost tokenů akceptována. Jinak
	 *         vrací {@code false}.
	 */
	private boolean acceptDate() {
		mark();
		if (acceptDay()) {
			if (accept(".", false)) {
				if (acceptMonth()) {
					if (accept(".", false)) {
						if (acceptYear()) {
							deleteMark();
							return true;
						}
					}
				} else if (acceptMonthName()){
					if (acceptYear()) {
						deleteMark();
						return true;
					}
				}
			}
		}
		if (acceptMonthName()) {
			if (acceptYear()) {
				deleteMark();
				return true;
			}
		}
		if (acceptYear()) {
			deleteMark();
			return true;
		}
		rollback();
		return false;
	}
	
	/**
	 * Akceptuje posloupnost tokenů od aktuální pozice ({@link #curPos}), pokud
	 * má význam času dne. (např.: <em>v 15:45</em>).
	 * 
	 * @return Vrací {@code true}, je-li posloupnost tokenů akceptována. Jinak
	 *         vrací {@code false}.
	 */
	private boolean acceptTime() {
		accept("v", true);
		String ct = getToken();
		if (acceptHour()) {
			if (accept("hodina", true)) {
				curDate.set(LaseCalendar.HOUR, Integer.parseInt(ct));
				return true;
			} else if (accept(":", true)) {
				ct = getToken();
				if (acceptMinute()) {
					curDate.set(LaseCalendar.MINUTE, Integer.parseInt(ct));
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Zjistí, zda je v daném kalendáři nastaveno datum, tzn. je nastaven den
	 * a/nebo měsíc a/nebo rok.
	 * 
	 * @param date
	 *            Zkoumaný kalendář.
	 * 
	 * @return Vrací {@code true}, je-li v kalendáři nastaveno datum. Jinak
	 *         vrací {@code false}.
	 */
	private static boolean isDateSet(LaseCalendar date) {
		return date.isSet(LaseCalendar.YEAR)
				|| date.isSet(LaseCalendar.MONTH)
				|| date.isSet(LaseCalendar.DAY);
	}
	
	/**
	 * Zjistí, zda je v daném kalendáři nastaven čas, tzn. je nastavena den
	 * hodina a/nebo minuta a/nebo sekunda.
	 * 
	 * @param date
	 *            Zkoumaný kalendář.
	 * 
	 * @return Vrací {@code true}, je-li v kalendáři nastaven čas. Jinak
	 *         vrací {@code false}.
	 */
	private static boolean isTimeSet(LaseCalendar date) {
		return date.isSet(LaseCalendar.MINUTE)
		|| date.isSet(LaseCalendar.HOUR)
		|| date.isSet(LaseCalendar.SECOND);
	}

	/**
	 * Vytvoří z aktuálního kalendáře {@link #curDate} pojmenovanou entitu s
	 * pozicí a přidá ji do seznamu pojmenovaných entit {@link #entityList}.
	 */
	private void storeDate() {
		int wordsCount = curDate.getWordsCount();
		int startPos = curPos - wordsCount;
		String words = curDate.getWords();
		
		Subtype subtype;
		if (isDateSet(curDate)) {
			if (isTimeSet(curDate))
				subtype = Subtype.DATE_TIME;
			else
				subtype = Subtype.DATE;
		} else if (isTimeSet(curDate)){
			subtype = Subtype.TIME;
		} else {
			throw new AssertionError();
		}
		
		// Rozdělení pojmenované entity DATE_TIME na DATE a TIME.
		if (subtype == Subtype.DATE_TIME) {
			int dateStartPos = startPos + curDate.getDateOffset();
			int timeStartPos = startPos + curDate.getTimeOffset();
			PositionedEntity datePE = new PositionedEntity(dateStartPos, curDate.getDateWordsCount(),
					curDate.getDateWords(), curDate.getDatePart(), Subtype.DATE);
			PositionedEntity timePE = new PositionedEntity(timeStartPos, curDate.getTimeWordsCount(),
					curDate.getTimeWords(), curDate.getTimePart(), Subtype.TIME);
			entityList.add(datePE);
			entityList.add(timePE);
		}
		
		PositionedEntity newPE = new PositionedEntity(startPos, wordsCount,
				words, curDate, subtype);
		entityList.add(newPE);
	}

	/**
	 * Vstupní metoda rekurzivního sestupu. Zpracuje vstupní seznam tokenů a
	 * vytvoří seznam nalezených časových údajů ({@link #entityList}).
	 */
	private void parse() {
		while(!finished) {
			if (acceptDate()) {
				mark();
				if (acceptTime())
					deleteMark();
				else
					rollback();
				storeDate();
			} else if (acceptTime()) {
				storeDate();
			} else {
				readToken();
			}
			curDate = new DateBuilder();
		}
	}

	/**
	 * Provede reset rozpoznávače časových údajů a nastaví nový vstupní seznam
	 * tokenů, který bude rozpoznán voláním metody {@link #parse()}.
	 * 
	 * @param tokenList
	 *            Nový vstupní seznam tokenů
	 */
	private void reset(List<MorphWord> tokenList) {
		this.originalTokenList = new ArrayList<MorphWord>(tokenList);
		this.tokenList = new ArrayList<String>(tokenList.size() + 1);
		for (MorphWord mw : tokenList)
			this.tokenList.add(mw.getLemma());
		this.tokenList.add(endToken);
		this.entityList = new ArrayList<PositionedEntity>(tokenList.size());
		this.curPos = 0;
		this.finished = false;
		this.curDate = new DateBuilder();
		this.history = new ArrayDeque<HistoryStackEntry>();
	}

	/**
	 * Vyhledá ve vstupním seznamu tokenů pojmenované entity podtypu {@code
	 * DATE}, {@code TIME} a {@code DATE_TIME}.
	 * 
	 */
	@Override
	public List<PositionedEntity> search(List<MorphWord> tokenList) {
		reset(tokenList);
		parse();
		return entityList;
	}

	/**
	 * Záznam zásobník historie rozpoznávání využívaný metodatmi
	 * {@link DateMatcher#mark()} a {@link DateMatcher#rollback()} Slouží k
	 * uložení stavu v určitém bodu rozpozávání pro případný pozdější návrat.
	 * 
	 * @author Rudolf Šíma
	 * 
	 */
	private static class HistoryStackEntry {
		
		/**
		 * Index aktuálního tokenu v bodě, kdy byl vytvořen záznam do historie.
		 */
		int markPos;
		
		/**
		 * Kopie kalendáře {@link DateMatcher#curDate} v bodě vytvoření záznamu do historie.
		 */
		DateBuilder markDate;

		/**
		 * Vytvoří nový záznam do historie rozpoznávání.
		 * 
		 * @param markPos
		 *            Index aktuálního tokenu v bodě, kdy byl vytvořen záznam do
		 *            historie.
		 * @param markDate
		 *            Kopie kalendáře {@link DateMatcher#curDate} v bodě
		 *            vytvoření záznamu do historie.
		 */
		public HistoryStackEntry(int markPos, DateBuilder markDate) {
			this.markPos = markPos;
			this.markDate = markDate;
		}
	}
	
	/**
	 * Kalendář {@link LaseCalendar} rozšířený o řetězec slov, ze kterých byl
	 * časový údaj (datum, čas nebo datum a čas) rozpoznán.
	 * 
	 * @author Rudolf Šíma
	 * 
	 */
	private static class DateBuilder extends LaseCalendar implements Cloneable {
		private static final long serialVersionUID = 1L;
		
		/**
		 * Slova, ze kterých byl časový údaj rozpoznán (datum i čas).
		 */
		private List<String> words;
		
		
		/**
		 * Slova, ze kterých byla rozpoznána část časového
		 * údaje dopovídající datu.
		 */
		private List<String> dateWords;
		
		/**
		 * Slova, ze kterých byla rozpoznána část časového
		 * údaje odpovídající času (ne datu),
		 */
		private List<String> timeWords;
		
		/**
		 * Index prvního slova přidaného {@link #addWord(String, boolean)},
		 * které odpovídá času.
		 */
		private int timeOffset = -1;
		
		/**
		 * Index prvního slova přidaného {@link #addWord(String, boolean)},
		 * které odpovídá datu.
		 */
		private int dateOffset = -1;
		
		/**
		 * Vytvoří nový prázdný {@code DateBuilder}.
		 */
		public DateBuilder() {
			words = new ArrayList<String>(8);
			dateWords = new ArrayList<String>(5);	
			timeWords = new ArrayList<String>(3);
		}
		
		/**
		 *  Vytvoří hlubokou kopii tohoto {@link DateBuilder}u. 
		 */
		public DateBuilder clone() {
			DateBuilder copy = (DateBuilder) super.clone();
			copy.words = new ArrayList<String>(words);
			copy.dateWords = new ArrayList<String>(dateWords);
			copy.timeWords = new ArrayList<String>(timeWords);
			return copy;
		}
		
		/**
		 * Připojí slovo k řetězci slov, ze kterých byl časový údaj rozpoznán.
		 * 
		 * @param w
		 *            Nové slovo.
		 */
		public void addWord(String w, boolean time) {
			if (time) {
				timeWords.add(w);
				if (timeOffset < 0)
					timeOffset = words.size();
			} else {
				dateWords.add(w);
				if (dateOffset < 0)
					dateOffset = words.size();
			}
			words.add(w);
		}

		/**
		 * Vrací řetězec slov, ze kterých byl časový údaj rozpoznán.
		 * 
		 * @return Vrací řetězec slov, ze kterých byl časový údaj rozpoznán.
		 */
		public String getWords() {
			return StringUtils.concat(words, " ");
		}

		/**
		 * Vrací počet slov (tokenů), ze kterých byl časový údaj rozpoznán.
		 * 
		 * @return Vrací řetězec slov, ze kterých byl časový údaj rozpoznán.
		 */
		public int getWordsCount() {
			return dateWords.size() + timeWords.size();
		}
		
		/**
		 * Vrací slova, ze kterých bylo rozpoznáno datum.
		 * @return Vrací slova, ze kterých bylo rozpoznáno datum.
		 */
		public String getDateWords() {
			return StringUtils.concat(dateWords, " ");
		}
		
		/**
		 * Vrací počet slov, ze kterých bylo rozpoznáno datum.
		 * 
		 * @return Vrací počet slov, ze kterých bylo rozpoznáno datum.
		 */
		public int getDateWordsCount() {
			return dateWords.size();
		}
		
		/**
		 * Vrací slova, ze kterých byl rozpoznán čas.
		 * 
		 * @return Vrací slova, ze kterých byl rozpoznán čas.
		 */
		public String getTimeWords(){
			return StringUtils.concat(timeWords, " ");
		}
		
		/**
		 * Vrací počet slov, ze kterých byl rozpoznán čas.
		 * 
		 * @return Vrací počet slov, ze kterých byl rozpoznán čas.
		 */
		public int getTimeWordsCount() {
			return timeWords.size();
		}

		/**
		 * Vrací index prvního slova odpovídajícího části data (ne času).
		 * 
		 * @return Vrací index prvního slova odpovídajícího části data.
		 */
		public int getDateOffset() {
			return dateOffset;
		}

		/**
		 * Vrací index prvního slova odpovídajícího části času (ne data).
		 * 
		 * @return Vrací index prvního slova odpovídajícího části času.
		 */
		public int getTimeOffset() {
			return timeOffset;
		}

		/**
		 * Vytvoří nový kalendář z části určující datum. Časová část
		 * není nastavena.
		 * 
		 * @return Vytvoří nový kalendář z části určující datum.
		 * 
		 * @see LaseCalendar
		 */
		public LaseCalendar getDatePart() {
			return new LaseCalendar(get(YEAR), get(MONTH), get(DAY),
					fieldNotSet, fieldNotSet, fieldNotSet);
		}

		/**
		 * Vytvoří nový kalendář z části určující čas. Část určující datum
		 * není nastavena.
		 * 
		 * @return Vytvoří nový kalendář z části určující čas.
		 * 
		 * @see LaseCalendar
		 */
		public LaseCalendar getTimePart() {
			return new LaseCalendar(fieldNotSet, fieldNotSet, fieldNotSet,
					get(HOUR), get(MINUTE), get(SECOND));
		}
	}
}
