package lase.entrec;

import java.util.ArrayList;
import java.util.List;
import java.util.RandomAccess;

import lase.morphology.MorphWord;
import lase.template.NamedEntityToken.Subtype;

/**
 * Rozpoznávač pojmenovaných entit podtypu {@code NUMBER}. Vyhledává v textu
 * číslovky zapsané číslicemi. Budoucí implementace mohou rozpoznávat i slovy
 * zapsaná čísla.
 * 
 * 
 * @author Rudolf Šíma
 * 
 */
public class NumberMatcher implements EntityMatcher {
	
	/**
	 * Vytvoří novou instanci rozpoznávače číslovek.
	 */
	public NumberMatcher() {
	}

	/**
	 * <p>
	 * Zjistí, zda je znaková posloupnost číslovkou. Tato verze pracuje pouze z
	 * čísly zapsanými číslicemi. Zkoumá tedy, zda jsou všechny znaky
	 * posloupnosti číslice.
	 * </p>
	 * 
	 * @param token
	 *            Zkoumaná posloupnost znaků.
	 * 
	 * @return Vrací {@code true}, je-li daná posloupnost znaků číslovkou. Jinak
	 *         vrací {@code false}.
	 */
	public boolean isInteger(CharSequence token) {
		int len = token.length();
		if (len > 9) // Větší se do Integeru nemusí vejít.
			return false;
		for (int i = 0; i < len; i++) {
			if (!Character.isDigit(token.charAt(i)))
				return false;
		}
		return len > 0;
	}

	/**
	 * Vyhledá v seznamu morfologických slov (tokenů vrácených morfologickým
	 * analyzátorem) všechny číslice.
	 */
	public List<PositionedEntity> search(List<MorphWord> tokens) {
		assert tokens instanceof RandomAccess;
		List<PositionedEntity> results = new ArrayList<PositionedEntity>();
		int i = 0;
		for (MorphWord mw : tokens) {
			String s = mw.getLemma();
			if (isInteger(s)) {
				int val = Integer.parseInt(s);
				PositionedEntity newPE = new PositionedEntity(i, 1, mw.getWord(), val, Subtype.NUMBER);
				results.add(newPE);
			}
			i++;
		}
		return results;
	}
	
}
