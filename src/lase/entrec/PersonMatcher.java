package lase.entrec;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import lase.morphology.MorphWord;
import lase.template.NamedEntityToken.Subtype;
import lase.util.PeekIterator;
import lase.util.SubarraysGenerator;

/**
 * Rozpoznávač jmen osob. Implementuje jednoduchý heuristický algoritmus, který
 * se snaží nalézt v textu jména osob, založený na posloupností slov
 * začínajících velkým písmenem, ve kterých ovšem nejsou všechna písmena velká.
 * Z těchto posloupností se potom generují všechny podposloupnosti sousedních
 * slov délky 2 až 4 (viz {@link SubarraysGenerator}), ze kterých jsou vytvořeny
 * pojmenované entity podtypu {@code PERSON}.
 * 
 * @author Rudolf Šíma
 * 
 */
public class PersonMatcher implements EntityMatcher {
	/**
	 * Množina římských číslic
	 */
	private static final Set<Character> romanDigits = new HashSet<Character>(Arrays.asList('I', 'V', 'X', 'L', 'C', 'M'));

	/**
	 * Zjistí, zda slovo začíná velkým písmenem zároveň obsahuje alespoň jedno
	 * malé písmeno.
	 * 
	 * @param word
	 *            Zkoumané slovo.
	 * @return Vrací {@code true}, začíná-li slovo velkým písmenem zároveň
	 *         obsahuje alespoň jedno malé písmeno.
	 */
	private static boolean isCapitalized(CharSequence word) {
		int len = word.length();
		if (len == 0)
			return false;
		if (!Character.isUpperCase(word.charAt(0)))
			return false;
		for (int i = 1; i < len; i++) {
			if (Character.isUpperCase(word.charAt(i)))
				return false;
		}
		return true;
	}

	/**
	 * Zjistí, zda by slovo mohla být jednopísmenná zkratka jména, tzn. skládá
	 * se z jednoho velkého písmene.
	 * 
	 * @param word
	 *            Zkoumané slovo
	 * 
	 * @return Vrací {@code true} pokud je argumentem {@code word} jedno velké
	 *         písmeno. Jinak vrací {@code false}.
	 */
	private static boolean isShortCandidate(CharSequence word) {
		return word.length() == 1 && Character.isUpperCase(word.charAt(0));
	}

	/**
	 * Zjistí, zda je slovo {@code word} tečka.
	 * 
	 * @param word
	 *            Zkoumané slovo.
	 *            
	 * @return Vrací {@code true} je-li argumentem {@code word} tečka ('.').
	 *         Jinak vrací {@code false}.
	 */
	private static boolean isDot(CharSequence word) {
		return word.length() == 1 && word.charAt(0) == '.';
	}

	/**
	 * Zjistí, zda je morfologické slovo římská číslovka. Z důvodu jednoduché
	 * implementaci algoritmus zjištuje pouze to, zda bylo slovo morfologickým
	 * analyzátorem vyhodnoceno jako číslovka a zda se skládá výhradně z písmen
	 * odpovídajících římským číslicím.
	 * 
	 * @param mw
	 *            Zkoumané morfologické slovo.
	 * 
	 * @return Vrací {@code true}, je-li zkoumané slovo římská číslovka. Jinak
	 *         vrací {@code false}.
	 */
	private static boolean isRomanNumeral(MorphWord mw) {
		if (mw.getPosTag() != 'C')
			return false;
		CharSequence word = mw.getWord();
		for (int i = 0; i < word.length(); i++) {
			char c = word.charAt(i);
			if (!romanDigits.contains(c))
				return false;
		}
		return true;
	}

	/**
	 * Zjistí, zda znaková sekvence končí tečkou.
	 * 
	 * @param word
	 *            Zkoumané slovo.
	 *            
	 * @return Vrací {@code true}, pokud slovo končí tečkou. Jinak vrací {@code
	 *         false}.
	 */
	private static boolean endsWithDot(CharSequence word) {
		int len = word.length();
		return len > 0 && word.charAt(len - 1) == '.';
	}

	/**
	 * Vyhledá v seznamu morfologických slov (seznam tokenů vrácený
	 * morfologickým rozpoznávačem) pojmenované entity podtypu {@code PERSON} na
	 * základě heuristického algoritmu sledujícího velikost písmen a výskyt
	 * římských číslovek. Pro posloupnosti slov s velkým písmenem na začátku
	 * nebo římských číslovek o délce od dvou do čtyř slov se vygenerují všechny
	 * podposloupnosti sousedních prvků o délce 2 až 4, které jsou všechny
	 * vráceny jako pojmenované entita podtypu {@code PERSON}. Cílem je správné
	 * vyřešení např. následujícího případu:
	 * <em>Král Karel IV. se narodil 14. května 1316</em><br>
	 * V tomto případě jsou nalezeny tyto osoby.
	 * <ul>
	 * <li>Král Karel</li>
	 * <li>Karel IV.</li>
	 * </ul>
	 * První osoba je nalezena chybně, což ovšem nevadí. Důležité je, aby žádná
	 * osoba v grafu entit nechyběla. Když tam budou některé osoby navíc,
	 * nezpůsobí to nejspíš žádné problémy.
	 * 
	 */
	@Override
	public List<PositionedEntity> search(List<MorphWord> tokens) {
		List<MorphWord> candidate = new ArrayList<MorphWord>();
		List<PositionedEntity> foundPersons = new ArrayList<PositionedEntity>();
		PeekIterator<MorphWord> iterator = new PeekIterator<MorphWord>(tokens.iterator());
		boolean acceptDot = false;
		int index = 0;
		while(iterator.hasNext()) {
			MorphWord mw = iterator.next();
			CharSequence word = mw.getWord();
			if (isRomanNumeral(mw) || isShortCandidate(word)) {
				if (iterator.hasNext()) {
					if (isDot(iterator.peek().getWord())) {
						acceptDot = true;
						candidate.add(mw);
					} else {
						saveCandidate(candidate, foundPersons, index); 
						acceptDot = false;
					}
				}
			} else if (acceptDot && isDot(word)) {
				candidate.add(mw);
				acceptDot = false;
			} else if (isCapitalized(word)) {
				candidate.add(mw);
				acceptDot = false;
			} else {
				saveCandidate(candidate, foundPersons, index); 
				acceptDot = false;
			}
			index++;
		}
		saveCandidate(candidate, foundPersons, index); 
		return foundPersons;
	}

	/**
	 * <p>
	 * Uloží kandidáta do seznamu nalezených entit, pokud má odpovídající délku.
	 * Tato metoda provádí generování všech podposloupností sousedních prvků v
	 * případě úspěšně rozpoznané posloupnosti slov, která by mohla obsahovat
	 * jméno osoby.
	 * </p>
	 * 
	 * @param candidate
	 *            Morfologická slova, ze kterých se skládá kandidát na odpověď.
	 *            Například morfologická slova odpovídající úseku textu:
	 *            <em>Král Karel IV.</em>.
	 * 
	 * @param foundPersons
	 *            Seznam nalezených entit, do kterého budou případně nové
	 *            vygenerované entity přidány.
	 * 
	 * @param index
	 *            Pozice prvního slova kandidáta v seznamu morfologických slov,
	 *            který vrátil morfologický analyzátor a byly předány metodě
	 *            {@link #search(List)}.
	 */
	private static void saveCandidate(List<MorphWord> candidate, List<PositionedEntity> foundPersons, int index) {
		int size = candidate.size();
		index -= size; 
		if (size < 2) {
			if (size > 0) {
				candidate.clear();
			}
			return;
		}
		
		List<MorphWord> origCandidate = candidate;
		candidate = attachDots(origCandidate);
		origCandidate.clear();
		
		MorphWord[] mwArr = candidate.toArray(new MorphWord[candidate.size()]);
		SubarraysGenerator generator = new SubarraysGenerator(2, 4);
		List<Object[]> subArrayList = generator.generate(mwArr);
		for (Object[] subArray : subArrayList) {
			StringBuilder words = new StringBuilder();
			StringBuilder lemmas = new StringBuilder();
			int extendBy = 0;
			
			int offset = 0; // určuje pozici prvního prvku podpole v poli
			for (int i = 0; i < mwArr.length; i++) {
				if (subArray[0] == mwArr[i]) {
					offset = i;
					break;
				}
			}
			for (int i = 0; i < subArray.length; i++) {
				if (i > 0) {
					words.append(' ');
					lemmas.append(' ');
				}
				MorphWord mw = (MorphWord) subArray[i];
				String word = mw.getWord();
				if (endsWithDot(word))
					extendBy++;
				words.append(word);
				lemmas.append(mw.getLemma());
			}
			PositionedEntity newPerson = new PositionedEntity(index + offset,
					subArray.length + extendBy, words.toString(), lemmas.toString(),
					Subtype.PERSON);
			foundPersons.add(newPerson);
		}
	}

	/**
	 * Připojí tečky za římské číslovky tak, že morfologické slovo obsahující
	 * tečku s pojí s předchozím slovem.
	 * 
	 * @param candidate
	 *            Seznam morfologikckých slov kandidáta.
	 *            
	 * @return Vrací seznam morfologických slov kandidáta s připojenými tečkami.
	 */
	private static List<MorphWord> attachDots(List<MorphWord> candidate) {
		List<MorphWord> result = new ArrayList<MorphWord>(candidate.size());
		PeekIterator<MorphWord> iterator = new PeekIterator<MorphWord>(candidate.iterator());
		boolean dotAttached = false;
		while(iterator.hasNext()) {
			if (dotAttached) {
				dotAttached = false;
				iterator.next();
				continue;
			}
			MorphWord newMw = null;
			MorphWord curMw = iterator.next();
			if (iterator.hasNext()) {
				MorphWord nextMw = iterator.peek();
				if(isDot(nextMw.getWord())) {
					newMw = combineMorphWords(curMw, nextMw);
					dotAttached = true;
				}
			}
			if (newMw == null) {
				result.add(curMw);
			} else {
				result.add(newMw);
			}
		}
		return result;
	}

	/**
	 * Spojí dvě morfologický slova do jednoho prostým spojením původních tvarů
	 * slova a lemmat. Jako slovní druh spojeného slova je použit slovní druh
	 * prvního ze slov.
	 * 
	 * @param firstMw
	 *            První morfologické slovo.
	 * @param secondMw
	 *            Druhé morfologické slovo.
	 *            
	 * @return Vrací nové morfologické slovo vzniklé sloučením dvou argumentů.
	 */
	private static MorphWord combineMorphWords(MorphWord firstMw, MorphWord secondMw) {
		return new MorphWord(firstMw.getWord() + secondMw.getWord(), 
				firstMw.getLemma() + secondMw.getLemma(), firstMw.getPosTag());
	}
}
