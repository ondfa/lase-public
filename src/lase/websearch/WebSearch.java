package lase.websearch;

import lase.util.Corpus;
import lase.util.LaseException;

/**
 * <p>
 * Rozhraní webového vyhledávače.
 * </p>
 * 
 * <p>
 * Místo implementace tohoto rozhraní může třída zdědit {@link AbstractWebSearch}, která
 * poskytuje metody, které lze využít pro většinu vyhledávačů. 
 * </p>
 * 
 * @author Rudolf Šíma
 *
 */
public interface WebSearch {

	/**
	 * <p>
	 * Stáhne vyhledávací korpus ze stránek nalezených webovym vyhledávačem po zadání
	 * fráze sestavené z parametrů <code>exactPhrases</code> a <code>otherWords</code>.
	 * </p>
	 * 
	 * <p>
	 * Sestavení fráze pro běžné typy vyhledávačů (např.: Google) se provede tak, že
	 * se nejprve všechny prvky <code>exactPhrases</code> vloží do uvozovek a poté
	 * se spojí mezerami do jednoho řetězce. Stejně tak <code>otherWords</code> se
	 * oddělené mezerami spojí do jednoho řetězce. Oba vzniklé řetězce se spojí
	 * do jednoho odděleného mezerou.
	 * </p> 
	 * 
	 * <h3>Příklad:</h3>
	 * <p>V případě následujících hodnot parametrů"
	 * <pre>
	 * exactPhrases[] = {"aa", "bb", "cc"}
	 * otherWords[]   = {"a", "b", "c"}
	 * </pre>
	 * bude mít vyhledávací fráze například vypadat
	 * <pre>
	 * "aa" "bb" "cc" a b c
	 * </pre>
	 * </p>
	 * 
	 * @param exactPhrases
	 *            - Pole přesných frází, které budou zadány do webového
	 *            vyhledávače (v uvozovkách).
	 * @param otherWords
	 *            - Slova, která budou do vyhledávače zadána jako dobrovolná
	 *            (bez uvozovek).
	 *            
	 * @return Vrací korpus textů stažených z internetu (např. texty snippetů),
	 *         který byl získán na základě vyhledání fráze sestavené z
	 *         <code>exactPhrases</code> a <code>otherWords</code>.
	 * 
	 * @throws LaseException
	 *             - Vyhozena v případě, že vyhledávání z důvodu nějaké chyby
	 *             není možné dokončit.
	 */
	public Corpus search(String[] exactPhrases, String[] otherWords) throws LaseException;

}
