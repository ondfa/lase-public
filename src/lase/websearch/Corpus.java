package lase.websearch;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.RandomAccess;

/**
 * Korpus získaný hledáním na webu.
 * 
 * @author Rudolf Šíma
 * 
 * @deprecated Použít {@link lase.util.Corpus}.
 */
@Deprecated
public class Corpus implements Iterable<String>, RandomAccess {
	private List<String> corpus = new ArrayList<String>();
	
	Corpus() {
	}
	
	void addPart(String part) {
		corpus.add(part);
	}

	public Iterator<String> iterator() {
		return corpus.iterator();
	}
	
	public String getPart(int index) {
		return corpus.get(index);
	}
	
	public int size() {
		return corpus.size();
	}
}
