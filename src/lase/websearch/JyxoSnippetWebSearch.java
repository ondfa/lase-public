package lase.websearch;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import javax.swing.text.AttributeSet;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.HTML.Tag;
import javax.swing.text.html.parser.ParserDelegator;

import lase.util.Corpus;
import lase.util.LaseException;
import lase.util.StringUtils;

import org.apache.commons.httpclient.Header;

/**
 * <p>
 * Rozhraní webového vyhledávače Jyxo.cz.
 * </p>
 * 
 * @author Rudolf Šíma
 * 
 * @see WebSearch
 *
 */
public class JyxoSnippetWebSearch extends AbstractWebSearch {

	/**
	 *	Předloha URL, ze kterého se stahují výsledky hledání.
	 */
	private static final String urlPattern = "http://jyxo.1188.cz/s?q=%s&d=cz&o=nostem,noacc,noduprem";
	
	/**
	 * Hlavička s cookie sloužící k nastavení počtu odkazů na stránce. Ve
	 * výchozím stavu je to pouze 10.
	 */
	private static final Header cookieHeader = new Header("Cookie", "cpr=a%3A1%3A%7Bs%3A5%3A%22prcnt%22%3Bs%3A1%3A%226%22%3B%7D");
	
	/**
	 * Stáhne korpus textů ze snippetů, které se získají vyhledáním daných frázi
	 * na vyhledávači Jyxo.
	 * 
	 * @see lase.websearch.AbstractWebSearch#search(java.lang.String[],
	 *      java.lang.String[])
	 */
	@Override
	public Corpus search(String[] exactPhrases, String[] otherWords) throws LaseException {
		String url = getSearchUrl(exactPhrases, otherWords);
		System.out.println("URL: " + url);
		String body = downloadBody(url);
		ParserDelegator delegator = new ParserDelegator();
		InputStream is = new ByteArrayInputStream(body.getBytes());
		Reader reader = new InputStreamReader(is);
		ParserCallback callback = new ParserCallback();
		try {
			delegator.parse(reader, callback, true);
		} catch (IOException e) {
			assert false;
			throw new LaseException(e);
		}
		System.out.println("Počet snippetů: " + callback.corpus.size());
		// for (String s : callback.corpus.getStringIterable()) {
		// 		System.err.println(s);
		// }
		return callback.corpus;
	}
	
	/**
	 * Callback HTML parseru, kde se zpracovává vyhledávací stránka z Jyxo
	 * a vytváří se z ní korpus. 
	 * 
	 * @author Rudolf Šíma
	 *
	 */
	private static class ParserCallback extends HTMLEditorKit.ParserCallback {
		
		/**
		 * <code>true</code>, pokud se právě načítá text snippetu.
		 */
		private boolean captureSnippet;
		
		
		/**
		 * <code>true</code>, pokud se právě načítá nadpis stránky
		 * (ze které snippet pochází). 
		 */
		private boolean captureTitle;
		
		/**
		 * StringBuilder, do kterého se ukládá text snippetu.
		 */
		StringBuilder snippetBuilder = new StringBuilder(0xFF);
		
		/**
		 * StringBuilder, do které se ukládá nadpis stránky
		 */
		StringBuilder titleBuilder = new StringBuilder(0x40);
		
		/**
		 * Řetězec, do kterého se uloží URL stránky, ze které snippet pochází.
		 */
		String url;
		
		/**
		 * Korpus, který je výsledkem zpracování vyhledávací stránky.
		 */
		Corpus corpus = new Corpus();

		/**
		 * Zjistí, zda množina atributů obsahuje atribut daného jména a zároveň
		 * má danou hodnotu.
		 * 
		 * @param set
		 *            - Množina atributů HTML tagu.
		 * @param name
		 *            - Jméno a atributu.
		 * @param value
		 *            - Požadovaná hodnota atributu.
		 * 
		 * @return Vrací <code>true</code>, pokud množina atribudů obsahuje
		 *         atribut daného jména a hodnoty. Jinak vrací
		 *         <code>false</code>
		 */
		private boolean containsAttribute(AttributeSet set, String name, String value) {			
			Object attr = set.getAttribute(HTML.Attribute.CLASS);
			return attr != null && value.equals(attr.toString());
		}
		
		/**
		 * Nastaví příslušné vlajky při nalezení určitého počátečního tagu.
		 */
		@Override
		public void handleStartTag(Tag t, MutableAttributeSet a, int pos) {
			if (t.equals(Tag.DIV)) {
				if (containsAttribute(a, "class", "re"))
					captureSnippet = true;
				else if (containsAttribute(a, "class", "r"))
					captureTitle = true;
			} else if (t.equals(Tag.A)) {
				if (containsAttribute(a, "class", "ri")) {
					url = a.getAttribute(HTML.Attribute.HREF).toString();
				}
			}
		}

		/**
		 * Při nalezení příslušného koncového tagu uloží vytvořený snippet do korpusu.
		 */
		@Override
		public void handleEndTag(Tag t, int pos) {
			
			if (t.equals(Tag.DIV)) {
				if (captureSnippet)
					flushSnippetBuilder();
				captureSnippet = false;
				captureTitle = false;
			}
		}

		/**
		 * Přidá text snippetu nebo nadpisu stránky do přislušného StringBuilderu.
		 */
		@Override
		public void handleText(char[] data, int pos) {
			if (captureSnippet)
				snippetBuilder.append(data);
			else if (captureTitle)
				titleBuilder.append(data);
		}

		/**
		 * Uloží obsah snippetu do korpusu, pokud v příslušném StringBuilderu
		 * nějaký text je.
		 */
		private void flushSnippetBuilder() {
			String snippetText = snippetBuilder.toString();
			if (snippetText.length() > 0) {
				snippetBuilder.setLength(0);
				String title = titleBuilder.toString();
				titleBuilder.setLength(0);
				if (url == null)
					url = "";
				snippetText = StringUtils.trimChar(snippetText, '.');
				snippetText = snippetText.replace(".....", "|");
				corpus.addEntry(snippetText, title, url);
			}
		}
	}

	/**
	 * Sestaví vyhledávací URL pro konkrétní řetězce, které se budou hledat.
	 * 
	 * @param exactPhrase
	 *            - Pole přesných frází, které budou zadány do vyhledávače Jyxo
	 *            v uvozovkách.
	 * @param otherWords
	 *            - Pole ostatních slov, které budou zadány bez uvozovek.
	 * 
	 * @return Vrací URL, na které Jyxo zobrazuje výsledky hledání.
	 */
	private String getSearchUrl(String[] exactPhrase, String[] otherWords) {
		String query = getSearchString(exactPhrase, otherWords);
		return String.format(urlPattern, urlEncodeUtf8(query));
	}

	/**
	 * Stáhne obsah webové stránky jako řetězec. V HTTP požadavku odešle
	 * hlavičku {@link #cookieHeader}.
	 * 
	 * @param url
	 *            - URL webové stránky.
	 *            
	 * @return Vrací webovou stránku jako řetězec.
	 * 
	 * @throws LaseException
	 *             Vyhozena v případě, že stažení stránky z nějakého důvodu
	 *             selhalo.
	 */
	private String downloadBody(String url) throws LaseException {
		return downloadBodyAsString(url, cookieHeader);
	}
}
