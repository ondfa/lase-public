package lase.websearch;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.apache.commons.httpclient.Header;
import org.freepascal.rtl.TObject;

import SecureBlackbox.Base.SBUtils;
import SecureBlackbox.SSHClient.TElSimpleSSHClient;
import SecureBlackbox.SSHCommon.TElSSHKey;
import SecureBlackbox.SSHCommon.TSSHKeyValidateEvent;
import lase.util.Corpus;
import lase.util.LaseException;
import lase.util.PathUtils;

/**
 * připojí se na server Ares.fav.zcu.cz a stránku stáhne přes něj.
 * API seznamu je totiž přístupné pouze ze školních IP
 * @author Ondřej Pražák
 * @version 1.00
 * @since 2.0
 *
 */
public abstract class AresWrapper extends AbstractWebSearch {

	private static final String address = "ares.fav.zcu.cz";
	
	private static final int port = 22;
	
	private static final String loginPath = "data" + File.separator + "login_info.txt";
	
	TSSHKeyValidateEvent.Callback OnKeyValidate = new TSSHKeyValidateEvent.Callback() {

		@Override
		public boolean tsshKeyValidateEventCallback(TObject arg0, TElSSHKey arg1) {
			return true;
		}
	};
	
	/**
	 * přihlašovací údaje načítá ze souboru {@code data\login_info.txt} nutno vyplnit.
	 */
	private String login;
	
	private String password;
	
	//private String command = "elinks -source ";
	
	/** soubor do kterého se na Aresu ukládá výsledné xml*/
	private String soubor = "result.xml";
	
	/** uloží xml do souboru na Aresu*/
	private String command = "wget -O "+soubor+" ";
	
	/** vypíše xml soubor*/
	private String command2 = "cat "+soubor;
	
	/** SSH spojení se serverem Ares.fav.zcu.cz*/
	private TElSimpleSSHClient client;
	
	public AresWrapper()
	{
		loadLogin();
	}

	@Override
	protected String downloadBodyAsString(String url, Header... headers)
			throws LaseException {
		SBUtils.setLicenseKey("010726015619C9A31054D0E501C8C1A9E84BED2AC5A7D047C7F62CE8526A4CFA13D2A05B5D4BB140D55620E25298FC518D991E0E79135FA0A10202A410CD3BB6965509280E6EFC8C24B59DF447C862543D0BAA2988CD706B6C4E08D428A4187C0AFA10AB66598BBEA7CB38453C86891693FA101B5146D22F31C33FD0D6C3C5C1CB72E0F1E6EBCD3CDF2990214D4E4D32ADE4F8D1604B5A96DEB812B85AF92202C598EC8CEAFA9828D53F79F7596B9FAD0D9CFDA7E4BEA9463C1C90827911609D0727B287059DE635E6E71A2266D64F2AAA6C2320D52535A089D4F9F1126C877BF23312F940998A5C7CF7B9B554EEF843598687303BA3BF475A1C6CAEA9072FA0");
		client = new TElSimpleSSHClient();
		client.setAddress(address);
	    client.setPort(port);
	    client.setUsername(login);
	    client.setPassword(password);
	    client.setOnKeyValidate(new TSSHKeyValidateEvent(OnKeyValidate));
	    client.setUseUTF8(true);
	    String command = this.command+url;
		
	    try
        {
	    	client.executeCommand(command);
            byte[] resp = client.executeCommand(command2);
            String output = new String(resp);
            client.close();
            return output;
        }
        catch (Exception e)
        {
        	e.printStackTrace();
        }
	    
		return null;
	}
	
	/**
	 * testovací metoda
	 * @param args
	 * @throws LaseException
	 */
	public static void main(String[] args) throws LaseException
	{
		String s = new AresWrapper() {
			
			@Override
			public Corpus search(String[] exactPhrases, String[] otherWords)
					throws LaseException {
				// TODO Auto-generated method stub
				return null;
			}
		}.downloadBodyAsString("\"http://searchapi.seznam.cz/api2/search?client=zcu&source=web&format=xml&query=Karel+Capek+se+narodil\"");
		System.out.println(s);
	}

	private void loadLogin()
	{
		BufferedReader bfr;
		try {
			bfr = new BufferedReader(new FileReader(PathUtils.convertRelativePath(loginPath)));
			login = bfr.readLine();
			password = bfr.readLine();
			bfr.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
