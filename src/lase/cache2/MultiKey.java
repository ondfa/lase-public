package lase.cache2;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Vícenáosbný klíč. Umožňuje použít více objektů jako klíč v
 * {@link java.util.Map} resp. {@link java.util.Set}.
 * 
 * @author Rudolf Šíma
 * 
 */
public final class MultiKey implements Serializable
{
	private static final long serialVersionUID = 1L;

	/**
	 * Pole obsažených objektů, ze kterých se vypočítává hash kód a provádí se
	 * nad nimi test rovnosti.
	 */
	private final Object[] values;
    
	/**
	 * Hash kód pole. Vypočte se při vytvoření instance.
	 */
	private final int hashCode;

	/**
	 * Vytvoří nový vícenásobný klíč složený z objektů {@code values}.
	 * 
	 * @param values
	 *            Objekty, které bude vícenásobný klíč obsahovat, vypočte z nich
	 *            hash kód a bude se nad nimi provádět test rovnosti.
	 */
    public MultiKey(Object... values)
    {
    	final int prime = 31;
        this.values = values;
        hashCode = prime * Arrays.hashCode(this.values);
    }

    /**
     * Provede test rovnosti nad všemi obsaženými objekty.
     */
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final MultiKey other = (MultiKey) obj;
        return Arrays.equals(values, other.values);
    }
    
    /**
     * Vrací hash kód, který byl vytvořen při vytvoření instance mnohonásobného klíče.
     */
    public int hashCode()
    {
        return hashCode;
    }
}