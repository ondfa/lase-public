package lase.cache2;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * Univerzální cache systému LASE. Slouží k uchování výsledku vráceného nějakou
 * metodou pro dané konkrétní hodnoty parametrů pro pozdější rychlé vyvolání.
 * </p>
 * 
 * <p>
 * Důvod pro použití {@code Cache} vzniká zejména při získávání výsledků z
 * pomalých webových služeb.
 * </p>
 * 
 * <p>
 * <h3>Příkad použití</h3>
 * 
 * <pre>
 * Cache cache = Cache.getCache(analyser.getClass(), 
 *               &quot;analyseSentence&quot;, String[].class);
 *
 * CacheResult cr = cache.get(new ArrayWrapper&lt;String&gt;(text));
 * if (cr.cacheHit()) {
 *     words = (List&lt;MorphWord&gt;) cr.getValue();
 * } else {
 *     words = analyser.analyseSentence(text);
 *     cr.updateCache(words);
 * }
 * </pre>
 * 
 * </p>
 * 
 * @author Rudolf Šíma
 * 
 */
public class Cache implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * Globálí vypínač cache. Nastavení na hodnotu {@code true} způsobí, že v
	 * cache nikdy není nalezen výsledek. Vhodné použít v případě podezření, že
	 * nějaký modul funguje jen díky správným výsledkům z cache nebo v situaci
	 * přesně opačné.
	 */
	private static final boolean globalDisableCache = false;

	/**
	 * Mapa instancí cache pro všechny metody, jejichž výsledky jsou uchovávány.
	 * 
	 * @see #getCache(Class, String, Class...)
	 */
	private transient static Map<String, Cache> instances = new HashMap<String, Cache>();

	/**
	 * Udává, zda byla některé cache modifikována a je proto třeba data před
	 * ukončením programu serializovat.
	 */
	private transient static volatile boolean modified;
	
	/**
	 * Mapa, do které ve které jsou uchovávána data cache. 
	 */
	private CacheMap cacheMap;

	/**
	 * Podpis instance cache. Slouží k jednoznačnému určení metody, jejíž
	 * výsledky jsou uchovávány v cache.
	 * 
	 * @see Cache#getCacheSignature(Class, String, Class...)
	 */
	private String instanceSignature;

	/**
	 * Vytvoří novou instanci cache s daným podpisem metody, jejíž výsledky jsou
	 * uchovávány v cache.
	 * 
	 * @param instanceSignature
	 *            Podpis metody, jejíž výsledky jsou uchovávány v cache.
	 * 
	 * @see #getCacheSignature(Class, String, Class...)
	 */
	private Cache(String instanceSignature) {
		this.instanceSignature = instanceSignature;
		cacheMap = new CacheMap();
	}

	/**
	 * Vrací cache pro danou metodu, který je popsána třemi parametry. Není-li
	 * taková cache zatím k dispozici, vytvoří novou prázdnou instanci.
	 * 
	 * @param clazz
	 *            Třída, ve které je deklarována metoda, jejíž výsledky jsou
	 *            uchovávány budou uchovávány v cache.
	 * @param methodName
	 *            Název metody, jejíž výsledky jsou uchovávány v cache.
	 * @param paramTypes
	 *            Typy parametru metody, jejíž výsledky jsou uchovávány v cache.
	 *            
	 * @return Vrací vytvořenou
	 */
	public static Cache getCache(Class<?> clazz, String methodName, Class<?>... paramTypes) {
		String signature = getCacheSignature(clazz, methodName, paramTypes);
		Cache inst;
		
		synchronized(instances) {
			inst = instances.get(signature);
		}
		if (inst == null) {
			inst = new Cache(signature);
			synchronized(instances) {
				instances.put(signature, inst);
			}
		}
		return inst;
	}

	/**
	 * Zjistí, zda byla některá ze všech použitých cache modifikována.
	 * 
	 * @return Vrací {@code true}, byla-li některá cache modifikována. Jinak
	 *         vrací {@code false}.
	 */
	public static boolean modified() {
		return modified;
	}

	/**
	 * <p>
	 * Nastaví hodnotu udávající, zda byla některé cache modifikována. Tato
	 * hodnota je potom vrácena metodou {@link #modified()}.
	 * </p>
	 * 
	 * <p>
	 * V případě, že je cache serializována do souboru v průběhu běhu aplikace,
	 * je vhodné zavolat tuto metodu s parametrem {@code false}, aby nebyla
	 * zbytečně nezměněná zapsána znovu.
	 * </p>
	 * 
	 * @param modified
	 *            Nová hodnota určující, zda byla cache modifikována.
	 */
	public static void setModified(boolean modified) {
		Cache.modified = modified;
	}

	/**
	 * Hledá v cache hodnotu pro dané hodnoty argumentů.
	 * 
	 * @param arguments
	 *            Hodnoty argumentů, pro které se v mapě hledá příslušný
	 *            výsledek.
	 * 
	 * @return Vrací objekt {@link CacheResult} obsahující výsledek hledání v
	 *         cache. Tento objekt obsahuje informace o tom, zda bylo hledání
	 *         úspěšné a pokud bylo, obsahuje nalezená data. Pomocí objektu
	 *         {@link CacheResult} lze také aktualizovat cache novými hodnotami.
	 */
	public synchronized CacheResult get(Object... arguments) {
		MultiKey mk = new MultiKey(arguments);
		Object cachedObject;
		if (globalDisableCache)
			cachedObject = null;
		else
			cachedObject = cacheMap.get(mk);
		return new CacheResult(cachedObject, mk, this);
	}

	/**
	 * Aktualizuje cache novými hodnotami pro daný vícenýsobný klíč. Tato metoda
	 * by měla být voláná výhradně metodou
	 * {@link CacheResult#updateCache(Object)}.
	 * 
	 * @param key
	 *            Klíč, pro který je do cache vložena nová hodnota.
	 * @param newResult
	 *            Nová hodnota vkládaná do cache.
	 */
	synchronized void updateCache(MultiKey key, Object newResult) {
		modified = true;
		synchronized (Cache.class) {
			cacheMap.put(key, newResult);
		}
	}

	/**
	 * Vrací objekt určený k serializaci. Po deserializaci tohoto objektu lze
	 * znovu vytvořit dříve serializovanou cache voláním
	 * {@link #setCacheSerializationObject(Object)}.
	 * 
	 * @return Vrací objekt určený k serializaci.
	 */
	public synchronized static Object getCacheSerializationObject() {
		return new CacheSerializationObject(instances);
	}

	/**
	 * Nastaví zdroj dat všech použitých cache, kterým je dříve serializovaný
	 * objekt {@link CacheSerializationObject} získaný metodou
	 * {@link #getCacheSerializationObject()}.
	 * 
	 * @param cacheData
	 *            Deserializovaný objekt obsahující data všech cache.
	 * 
	 * @throws ClassCastException
	 *             Vyhozena v případě, že parametr není platný dříve
	 *             serializovaný objekt.
	 */
	public synchronized static void setCacheSerializationObject(Object cacheData) {
		if (!(cacheData instanceof CacheSerializationObject))
			throw new ClassCastException("Supplied argument is not a serialized cache.");
		instances = ((CacheSerializationObject)cacheData).instances;
	}
	
	/**
	 * Vrací hash kód podpisu instance.
	 * @see #getCacheSignature(Class, String, Class...)
	 */
	@Override
	public int hashCode() {
		return instanceSignature.hashCode();
	}

	/**
	 * Provede test rovnosti podpisu instance s podpisem instance jiného objektu
	 * {@code Cache}.
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj.getClass() != getClass())
			return false;
		return instanceSignature.equals(((Cache)obj).instanceSignature);
	}

	/**
	 * Na základě tří parametrů popisujících metodu, jejíž výsledky jsou
	 * uchovávány v cache, vytvoří podpis instance cache. Tento podpis je
	 * řetězec, který jednoznačně určuje zmíněnou metodu.
	 * 
	 * @param clazz
	 *            Třída, ve které je deklarována metoda, jejíž výsledky jsou
	 *            uchovávány budou uchovávány v cache.
	 * @param methodName
	 *            Název metody, jejíž výsledky jsou uchovávány v cache.
	 * @param paramTypes
	 *            Typy parametru metody, jejíž výsledky jsou uchovávány v cache.
	 * 
	 * @return Vrací podpis instance cache.
	 */
	private static String getCacheSignature(Class<?> clazz, String methodName, Class<?>... paramTypes) {
		StringBuilder builder = new StringBuilder();
		builder.append(clazz.getCanonicalName());
		builder.append("::");
		builder.append(methodName);
		builder.append("[");
		for (Class<?> pt : paramTypes) {
			builder.append(pt.getCanonicalName());
			builder.append(";");
		}
		builder.append(']');
		return builder.toString();
	}

	/**
	 * Objekt, který slouží k serializaci a deserializaci všech v systému
	 * použitých cache najednou. Obsahuje mapu všech cache, která obšem není z
	 * kódu přístupný z důvodu zapouzdření.
	 * 
	 * @author Rudolf Šíma
	 * 
	 * @see Cache#setCacheSerializationObject(Object)
	 */
	private static class CacheSerializationObject implements Serializable {
		private static final long serialVersionUID = 1L;
		
		/**
		 * Mapa instancí cache pro všechny metody, pro které je cache použita.
		 */
		private Map<String, Cache> instances = new HashMap<String, Cache>();

		/**
		 * Vytvoří nový objekt pro serializaci a deserializaci cache.
		 * 
		 * @param instances
		 *            Mapa instancí všech v systému použitých cache.
		 */
		private CacheSerializationObject(Map<String, Cache> instances) {
			this.instances = instances; 
		}
	}
}
