package lase.template;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import lase.util.GenericEnumsWorkaround;
import lase.util.StringUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * <p>
 * Vyhledávací šablona systému LASE.
 * </p>
 * 
 * <p>
 * Skládá se ze
 * <ol>
 * <li>vzoru otázky</li>
 * <li>seznamu vzorů odpovědí</li>
 * </ol>
 * 
 * <p>
 * Využívá se k vytvoření vyhledávacích vzorů na základě uživatelem zadané
 * otázky. Pokud je nalezena šablona, jejíž vzor otázky odpovídá zadané otázce,
 * vytvoří se přiřazení tokenů vzoru otázky tokenům zadané otázky. Tokeny vzorů
 * odpovědí se odkazují na tokeny vzoru otázky, a je tedy možná sestavit
 * vyhledávací vzor na základě zmíněné mapy.
 * </p>
 * 
 * <p>
 * Příklad šablony s jedním vzorem odpovědi:
 * 
 * <pre>
 * &lt;template&gt;
 *    &lt;query&gt;
 *       &lt;token id="1"&gt;kdy&lt;/token&gt;
 *       &lt;token id="2"&gt;se&lt;/token&gt;
 *       &lt;token id="3" type="lemma"&gt;narodit&lt;/token&gt;
 *       &lt;token id="4" type="person"/&gt;
 *    &lt;/query&gt;
 *    &lt;pattern&gt;
 *       &lt;token id="1" ref="4"/&gt;
 *       &lt;token id="2" ref="2"/&gt;
 *       &lt;token id="3" ref="3"/&gt;
 *       &lt;token id="4" mod="answer" type="date"/&gt;
 *    &lt;/pattern&gt;
 * &lt;/template&gt;
 * </pre>
 * 
 * <p>
 * Otázka "Kdy se narodil Karel IV." odpovídá vzoru otázky uvedené šablony.
 * Vytvoří se mapa zjednodušeně zapsaná takto:
 * </p>
 * 
 * <table>
 * <tr>
 * <td>kdy</td>
 * <td>—&gt;</td>
 * <td>Kdy</td>
 * </tr>
 * <tr>
 * <td>se</td>
 * <td>—&gt;</td>
 * <td>se</td>
 * </tr>
 * <tr>
 * <td>narodit</td>
 * <td>—&gt;</td>
 * <td>narodil</td>
 * </tr>
 * <tr>
 * <td>{osoba}</td>
 * <td>—&gt;</td>
 * <td>Karel IV.</td>
 * </tr>
 * </table>
 * <p>
 * Vyhledávací vzor, který se okpazuje po řadě na 4., 2. a 3, token otázky tedy
 * podle této mapy nabyde podoby: "Karel IV. se narodil". Následuje token s
 * datem, který je hledanou odpovědí.
 * </p>
 * 
 * @see Template#matchQuery(Query)
 * 
 * @author Rudolf Šíma
 * @version 2.00 edit 28.4.2014 Ondřej Pražák
 * 
 */
public class Template {

	/**
	 *	Vzor otázky, tj. seznam tokenů, ze kterých se otázka musí skládat.
	 */
	private TokenList queryPattern = new TokenList();
	
	/**
	 * Vzor odpovědi, tj. seznam tokenů, které tvoří odpověd, z nichž některé
	 * mohou být tokenu typu reference, které odkazují na tokeny vzoru otázky.
	 */
	private List<TokenList> answerPatterns = new ArrayList<TokenList>();
	
	/** 
	 * Mapa sloužící vyhledávání tokenů otázky podle id. 
	 */
	private Map<Integer, Token> queryTemplateTokensById = new HashMap<Integer, Token>();

	/**
	 * Načte šablonu z XML souboru.
	 * 
	 * @param xmlFile
	 *            - cesta ke vstupnímu XML souboru
	 *            
	 * @throws SAXException
	 *             Vyhozena XML parserem v případě blíže nespecifikované chyby
	 *             při načítání XML souboru.
	 *             
	 * @throws IOException
	 *             Vyhozena v případě chyby při čtení vstupního XML souboru.
	 *             
	 * @throws BadTemplateException
	 *             Vyhozena v případě chyby v šabloně (např.: reference na
	 *             neexistující token).
	 */
	public Template(String xmlFile) throws SAXException, IOException, BadTemplateException {
		loadXmlFile(xmlFile);
	}
	
	// TODO - comment
	Template(TemplateCreator creator, Template questionPattern) {
		queryPattern = questionPattern.queryPattern;
		answerPatterns = creator.getAnswerPatterns();
	}
	
	/**
	 * Vytvoří novou šablonu z dat v objektu <code>creator</code>. Data vzor
	 * otázky ani vyhledávací vzory se nekopírují.
	 * 
	 * @param creator
	 *            - Tvůrce šablon jehož data jsou použita k vytvoření šablony.
	 * 
	 * @see TemplateCreator
	 */
	Template(TemplateCreator creator) {
		queryPattern = creator.getQueryPattern();
		answerPatterns = creator.getAnswerPatterns();
	}

	/**
	 * <p>
	 * Pokusí se najít přesnou shodu vzoru otázky s danou otázkou. Při hledání
	 * shody se bere v potaz dobrovolný výskyt tokenů ve vzoru. V případě
	 * nalezení shody je vytvořen seznam vyhledávacích vzorů.
	 * </p>
	 * 
	 * <p>
	 * Vyhledávací vzor ({@link SearchPattern}) je seznam tokenů
	 * {@link QueryToken} zapouzřujících původní shodující se {@link Token} z
	 * šablony a konkrétní slovo (a sémantickou hodnotu), která je získána z
	 * otázky.
	 * </p>
	 * 
	 * <p>
	 * <strong>Příklad:</strong>
	 * <table>
	 * <tr>
	 * <td>Uživatelem zadaná otázka:</td>
	 * <td>Kdy se narodil Karel IV.?</td>
	 * </tr>
	 * <tr>
	 * <td>Vzor otázky:</td>
	 * <td>Kdy se &lt;lemma:narodit&gt; &lt;osoba&gt;</td>
	 * </tr>
	 * <tr>
	 * <td>Vzor odpovědi:</td>
	 * <td>&lt;4.token&gt; &lt;2.token&gt; &lt;3.token&gt; &lt;datum&gt;</td>
	 * </tr>
	 * <tr>
	 * <td>Vytvořený vyhledávací vzor:</td>
	 * <td>&lt;osoba:Karel IV.&gt; se narodil &lt;datum&gt;</td>
	 * </tr>
	 * <table>
	 * Poznámka: Tento zápis je zjednodušující. Slovo "narodil" i ve
	 * vyhledávacím vzoru nese informaci o tom, že bylo nalezeno shodou s
	 * lemmatem "narodit".
	 * </p>
	 * 
	 * @param query
	 *            - Uživatelem zadaná otázka.
	 * 
	 * @return Vrací výsledek hledání shody. V případě nalezení shody tento
	 *         výsledek obsahuje vytvořené vyhledávací vzory.
	 * 
	 * @see Query
	 * @see SearchPattern
	 * @see TemplateMatchResult
	 */
	public TemplateMatchResult matchQuery(Query query) {
		
		// Mapa nalezených shod mezi tokeny ze vzoru otázky a ze zadaná otázky.
		Map<Token, List<QueryToken>> tokenMatch = new HashMap<Token, List<QueryToken>>();
		
		// Počet nalezených v zadané otázce.
		final int querySize = query.size();
		
		// Je-li zadaná otázka delší než vzor otázky, nemůže se shodovat.
		// Naopak to ovšem díky dobrovolně se vyskytujícím tokenům možné je.
//		if (queryPattern.size() < querySize)
//			return TemplateMatchResult.NOMATCH;
		
		// Index aktuálního tokenu ze zadané otázky
		int k = 0;
		// Postupně se prochází tokeny vzoru otázky a testuje se shoda
		// s tokeny zadané otázky. V případě zjištění neshody u povinně se
		// vyskytujícího tokenu je hledávání ukončeno jako neúspěšné.
		for (Token templateToken : queryPattern) {
			
			// Pokud byly otestovány všechny tokeny nalezené v zadané otázce,
			// ale ve vzoru otázky ještě nějaké zbývají, musí být pro dosažení
			// shody všechny volitelné.
			if (k >= querySize) {
				assert k == querySize;
				if (templateToken.isOptional())
					continue;
				return TemplateMatchResult.NOMATCH;
			}
			// počet tokenů otázky které se shodují s tímto tokenem vzoru
			int matchCount = 0;
			QueryToken inputQueryToken = null;
			
			// seznam tokenů ze zadané otázky, které vyhovují tomuto tokenu ze vzoru
			List<QueryToken> list = new ArrayList<QueryToken>();
			
			// iteruje přes počet možných opakování tokenu
			while(matchCount < templateToken.getMaxRepeat())
			{
				// Token ze zadané otázky
				inputQueryToken = k<query.size() ? query.get(k) : null;
				// Testování neshody tokenu vzoru otázky s aktuálním tokenem
				// ze zadané otázky.
				if (inputQueryToken==null || !templateToken.matches(inputQueryToken)) {
					// V případě neshody se ověří, zda je aktuální token
					// ze vzoru otázky povinný.
					if (templateToken.isOptional() || templateToken.getMinRepeat()<=matchCount)
						// Neshodující se dobrovolný token se přejde
						// aniž by se inkrementoval čítač k.
						break;
					// Neúspěšné vyhledávání.
					return TemplateMatchResult.NOMATCH;
				}
				// V případě shody se inkrementuje index k a počet tokenů které vyhovují tomuto tokenu ze vzoru
				k++;
				matchCount++;
				// Vložení shodujícího se tokenu do mapy pro pozdější sestavení
				// vyhledávacích vzorů.
				list.add(inputQueryToken);
			}
			tokenMatch.put(templateToken, list);
		}
		
		// V případě, že čítač k odpovídá délce uživatelem zadané otázky
		// byla nalezena přesná shoda a je vrácen výsledek obsahující
		// seznam vyhledávacích vzorů odpovídajících otázce.
		if (k == query.size())
			return new TemplateMatchResult(getSearchPatterns(tokenMatch, query));

		// Čítač k se nerovná počtu tokenů zadané otázky, nebyla nalezena shoda. 
		return TemplateMatchResult.NOMATCH;
	}
	
	/**
	 * Vytvoření seznamu vyhledávacích vzorů na základě uživatelem zadané otázky
	 * a mapy tokenů vytvořené při hledání shody vzoru otázky ze zadanou
	 * otázkou.
	 * 
	 * @param tokenMatch
	 *            - Mapa tokenů vzoru otázky na tokeny zadané otázky.
	 *            
	 * @param query
	 *            - Seznam tokenů zadané otázky.
	 * 
	 * @return Vrací seznam vyhledávacích vzorů odpovídajíchích zadané otázce
	 *         shodující se s šablonou.
	 */
	private List<SearchPattern> getSearchPatterns(Map<Token, List<QueryToken>> tokenMatch, Query query) {
		// Vytvářený seznam vyhledávacích vzorů.
		List<SearchPattern> searchPatterns = new ArrayList<SearchPattern>();
		
		// Iterace přes všechny vzory odpovědí.
		for (TokenList answerPattern : answerPatterns) {
			// Nový vyhledávací vzor 
			List<QueryToken> newSearchPattern = new ArrayList<QueryToken>();
			
			// Iterace přes všchny vzory otázky
			for (Token apToken : answerPattern) {
			
				switch(apToken.getType()) {
				case REFERENCE:
					// Token ze vzoru odpovědi je reference na token ze vzoru otázky
					// který je třeba dohledat.
					ReferenceToken ref = (ReferenceToken) apToken;
					Token referencedToken = queryTemplateTokensById.get(ref.getRefId());
					// Šablony se kontrolují při načítání, proto nemůže dojít nenalezení
					// odkazovaného tokenu. Tato situace může ovšem nastat v případě, že
					// předchozí tvrzení není pravdivé, což je dost možné. ;-)
					if (referencedToken == null)
						throw new BadTemplateAssertionError("Search pattern contains an unsatisfied reference.");
					List<QueryToken> newTokens = tokenMatch.get(referencedToken);
					for(QueryToken newToken:newTokens)
						assert newToken != null : "Faulty template matching algorithm.";
					// Přidání nových tokenů do vytvářeného vyhledávacícho vzoru.
					newSearchPattern.addAll(newTokens);
					break;
				case POS:
					// Token ve vzoru odpovědi je specifikace slovního druhu. Není tedy
					// známa konkrétní podoba slova ani sémantická hodnota, a jsou proto
					// nastaveny na null.
					newSearchPattern.add(new QueryToken(apToken, null, null));
					break;
				default:
					// V ostatních případech se jako hledané slovo použije text tokenu
					// ve vzoru odpovědi v šabloně. Pozn.: Smysluplné využití najde
					// tento řetězec pouze u tokenu typu WORD.
					newSearchPattern.add(new QueryToken(apToken, apToken.getText(), apToken.getText()));
				}
				
			}
			// Přidání nově vytvořeného vyhledávacího vzoru do seznamu.
			searchPatterns.add(new SearchPattern(newSearchPattern));
		}
		return searchPatterns;
	}
	
	/**
	 * Uložení šablony do XML souboru.
	 * 
	 * @param xmlFile
	 *            - Cílový XML soubor. (Neexistující bude vytvořen.)
	 * 
	 * @throws TransformerException
	 *             Vyhozena v případě jakékoli chyby při zápisu XML dokumentu do
	 *             souboru.
	 */
	public void saveToXmlFile(String xmlFile) throws TransformerException {
		Document document = getBuilder().newDocument();
		document.setXmlStandalone(true);
		
		Element templateNode = document.createElement("template");
		document.appendChild(templateNode);
		
		Node queryNode = document.createElement("query");
		templateNode.appendChild(queryNode);
		for (Token t : queryPattern) {
			queryNode.appendChild(createTokenElement(document, t));
		}
		
		for (TokenList pattern : answerPatterns) {
			Node patternNode = document.createElement("pattern");
			templateNode.appendChild(patternNode);
			for (Token t : pattern) {
				patternNode.appendChild(createTokenElement(document, t));
			}
		}
	
		writeXml(document, xmlFile);
	}

	/**
	 * Uloží šablonu do souboru v daném adresáři ve standardním XML formátu do
	 * souboru, jehož jméno má formát "template&lt;číslo&gt;.xml", kde číslo je
	 * nejnižší volné číslo.
	 * 
	 * 
	 * @param directory
	 *            Adresář, ve kterém bude soubor vytvořen.
	 * 
	 * @throws TransformerException
	 *             Vyhozena v případě, že při vytváření XML souboru došlo k
	 *             chybě.
	 *             
	 * @see #saveToXmlFile(String)
	 */
	public void saveToDirectory(String directory) throws TransformerException {
		final File dir = new File(directory);
		final Pattern pattern = Pattern.compile("[^0-9]+([0-9]+)\\.xml");
		File[] tempFiles = dir.listFiles();
		int maxNum = -1;
		for (File tf : tempFiles) {
			Matcher matcher = pattern.matcher(tf.getName());
			if (matcher.matches()) {
				int num = Integer.parseInt(matcher.group(1));
				if (num > maxNum)
					maxNum = num;
			}
		}
		String newName = "template" + String.format("%04d", (maxNum + 1)) + ".xml";
		saveToXmlFile(dir.getPath() + "/" + newName);
	}

	/**
	 * Načtení všech šablon v daném adresáři. Načítány jsou všechny souboru
	 * jejichž jména jsou odpovídají regulárnímu výrazu "template[0-9]+.xml".
	 * 
	 * @param dirPath
	 *            - Cesta k adresáři se šablonami.
	 * 
	 * @return Vrací seznam nanačtených šablon.
	 * 
	 * @throws SAXException
	 *             Vyhozena XML parserem v případě blíže nespecifikované chyby
	 *             při načítání XML souboru.
	 * 
	 * @throws IOException
	 *             Vyhozena v případě chyby při čtení vstupního XML souboru.
	 * 
	 * @throws BadTemplateException
	 *             Vyhozena v případě chyby v šabloně (např.: reference na
	 *             neexistující token).
	 */
	public static List<Template> loadDirectory(String dirPath) throws SAXException, IOException, BadTemplateException {
		FilenameFilter filenameFilter = new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				final Pattern filanamePattern = Pattern.compile("template[0-9]+.xml");
				final Matcher matcher = filanamePattern.matcher(name);
				return matcher.matches();
			}
		};
		File templatesDir = new File(dirPath);
		List<Template> templates = new ArrayList<Template>();
		File[] fileList = templatesDir.listFiles(filenameFilter);
		if (fileList != null) {
			for (File templateFile : fileList) {
				Template template = new Template(templateFile.getAbsolutePath());
				templates.add(template);
			}
		}
		return templates;
	}

	/**
	 * Načtení šablony z XML souboru.
	 * 
	 * @param xmlFile - Vstupní XML soubor s šablonou. 
	 * @throws SAXException
	 *             Vyhozena XML parserem v případě blíže nespecifikované chyby
	 *             při načítání XML souboru.
	 *             
	 * @throws IOException
	 *             Vyhozena v případě chyby při čtení vstupního XML souboru.
	 *             
	 * @throws BadTemplateException
	 *             Vyhozena v případě chyby v šabloně (např.: reference na
	 *             neexistující token).
	 */
	private void loadXmlFile(String xmlFile) throws SAXException, IOException, BadTemplateException {
		Document document = getBuilder().parse(new File(xmlFile));
		List<Token> queryPatternTokens = new ArrayList<Token>();
		
		// tag 'query'
		NodeList queryNodeList = document.getElementsByTagName("query");
		if (queryNodeList.getLength() != 1)
			throw new BadTemplateException("Template must contain exactly one 'query' tag.");
		Node queryNode = queryNodeList.item(0);
		for (Node tokenNode = queryNode.getFirstChild(); tokenNode != null; tokenNode = tokenNode.getNextSibling()) {
			Token token = readToken(tokenNode);
			if (token == null)
				continue;
			queryPatternTokens.add(token);
			queryTemplateTokensById.put(token.getId(), token);
		}
		this.queryPattern = new TokenList(queryPatternTokens);
		
		// tagy 'pattern'
		NodeList patternNodeList = document.getElementsByTagName("pattern");
		for (int i = 0; i < patternNodeList.getLength(); i++) {
			List<Token> answerPatternTokens = new ArrayList<Token>();
			Node patternNode = patternNodeList.item(i);
			for (Node tokenNode = patternNode.getFirstChild(); tokenNode != null; tokenNode = tokenNode.getNextSibling()) {
				Token token = readToken(tokenNode);
				if (token == null)
					continue;
				// kontrola reference
				if (token.getType() == Token.Type.REFERENCE) {
					int refId = ((ReferenceToken)token).getRefId();
					if (queryTemplateTokensById.get(refId) == null)
						throw new BadTemplateException("Bad reference: " + refId);
				}
				answerPatternTokens.add(token);
			}
			answerPatterns.add(new TokenList(answerPatternTokens));
		}
		answerPatterns =  Collections.unmodifiableList(answerPatterns);
	}

	/**
	 * Načtení tokenu z příslušného uzlu DOM stromu XML dokumentu.
	 * 
	 * @param tokenNode
	 *            - Uzel DOM.
	 * 
	 * @return Vrací nový token, nebo <code>null</code> v případě, že nejde o
	 *         správný uzel.
	 * 
	 * @throws BadTemplateException
	 *             Vyhozena v případě chyby v šabloně (např.: reference na
	 *             neexistující token).
	 */
	private static Token readToken(Node tokenNode) throws BadTemplateException  {
		if (!"token".equals(tokenNode.getNodeName()))
			return null;
		NamedNodeMap attrs = tokenNode.getAttributes();
		Node idNode = attrs.getNamedItem("id");
		if (idNode == null)
			throw new BadTemplateException("All 'token' tags must have an 'id' attribute.");
		int id;
		try {
			id = Integer.parseInt(idNode.getTextContent());
		} catch (NumberFormatException e) {
			throw new BadTemplateException("The of the 'id' attribute must be an integer.", e);
		}
		Node refNode = attrs.getNamedItem("ref");
		Node typeNode = attrs.getNamedItem("type");
		String type;
		Token token;
		if (refNode == null) {
			if (typeNode == null)
				type = "WORD";
			else
				type = typeNode.getTextContent();
			token = Token.newToken(id, type, tokenNode.getTextContent());
		} else {
			if (typeNode != null)
				throw new BadTemplateException("'token' tags with 'ref' attribute must not contian any 'type' attribute.");
			int refId;
			try {
				refId = Integer.parseInt(refNode.getTextContent());
			} catch (NumberFormatException e) {
				throw new BadTemplateException("The value of the 'ref' attribute must be an integer.", e);
			}
			token = new ReferenceToken(id, refId);
		}
		Node modNode = attrs.getNamedItem("mod");
		if (modNode != null) {
			String modValues[] = modNode.getTextContent().toUpperCase().split("\\s");
			for (String modValue : modValues) {
				try {
					token.addModifier(Token.Modifier.valueOf(modValue));
				} catch(IllegalArgumentException e) {
					throw new BadTemplateException("'" + modValue + "' is not a valid value of the 'mod' attribute.");
				}
			}
		}
		
		// nastavení maximálního počtu opakování, pokud je uveden atribut maxRepeat
		Node maxRepeatNode = attrs.getNamedItem("maxRepeat");
		if (maxRepeatNode != null) {
			try {
				int maxRepeat = Integer.valueOf(maxRepeatNode.getTextContent());
				token.setMaxRepeat(maxRepeat);
			} catch(NumberFormatException e) {
				throw new BadTemplateException("'" + maxRepeatNode.getTextContent() + "' is not a valid value of the 'maxRepeat' attribute.");
			}
		}
		
		// nastavení minimálního počtu opakování, pokud je uveden atribut maxRepeat
		Node minRepeatNode = attrs.getNamedItem("minRepeat");
		if (minRepeatNode != null) {
			try {
				int minRepeat = Integer.valueOf(minRepeatNode.getTextContent());
				token.setMinRepeat(minRepeat);
			} catch(NumberFormatException e) {
				throw new BadTemplateException("'" + minRepeatNode.getTextContent() + "' is not a valid value of the 'minRepeat' attribute.");
			}
		}
		
		token.makeImmutable();
		return token;
	}

	/**
	 * Vytvoří {@link DocumentBuilder} sloužící k vytváření XML dokumentu.
	 * 
	 * @return Vrací novou instanci {@link DocumentBuilder}u nebo
	 *         <code>null</code> v případě neúspěšného pokusu o vytvoření této
	 *         instance.
	 */
	private static DocumentBuilder getBuilder() {
		try {
			return DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			assert false;
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Zapíše XML dokument do souboru.
	 * 
	 * @param document
	 *            - XML dokument.
	 * @param filename
	 *            - Cílový soubor.
	 * @throws TransformerException
	 *             Vyhozena v případě jakékoli chyby při zápisu XML dokumentu
	 *             do souboru.
	 */
	private static void writeXml(Document document, String filename) throws TransformerException {
		try {
			Source source = new DOMSource(document);
			File file = new File(filename);
			Result result = new StreamResult(file);
			Transformer xformer = TransformerFactory.newInstance().newTransformer();
			// Nastaveno odsazování.
			xformer.setOutputProperty(OutputKeys.INDENT, "yes");
			// Velikost odsazení nastavena na 3 mezery.
			xformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "3");
			xformer.transform(source, result);
		} catch (TransformerConfigurationException e) {
			assert false;
			e.printStackTrace();
			throw new TransformerException(e);
		}
	}
	
	/**
	 * Vytvoří element DOM stromu nesoucí data tokenu šablony, tj jednoho prvku
	 * vzoru otázky nebo odpovědi.
	 * 
	 * @param document
	 *            - XML dokument, jehož element bude vytvořen.
	 *            
	 * @param t
	 *            - Token, jehož data budou použita.
	 *            
	 * @return Vrací vytvoření element.
	 */
	private static Element createTokenElement(Document document, Token t) {
		Element tokenNode = document.createElement("token");
		tokenNode.setAttribute("id", Integer.toString(t.getId()));
		if (t.getType() == Token.Type.REFERENCE) {
			ReferenceToken refT = (ReferenceToken) t;
			tokenNode.setAttribute("ref", Integer.toString(refT.getRefId()));
		} else {
			Enum<?> type = t.getSubtype();
			if (type == null)
				type = t.getType();
			/* Workaround kvůli chybě v některých překladačích.
			 * @see lase.util.GenericEnumsWorkaround */
			if (!GenericEnumsWorkaround.compare(type, Token.Type.WORD))
				tokenNode.setAttribute("type", type.toString().toLowerCase());
		}
		
		// Atribut nesoucí informaci o modifikátorech (OPTIONAL, ANSWER).
		String mod = null;
		for (Token.Modifier m : Token.Modifier.values()) {
			if (t.hasModifier(m)) {
				if (mod == null)
					mod = "";
				else
					mod += " ";
				mod += m.toString().toLowerCase();
			}
		}
		if (mod != null)
			tokenNode.setAttribute("mod", mod);
		
		// Vložení textového obsahu tokenu.
		if (!t.isAnswer())
			tokenNode.setTextContent(t.getText());
		
		return tokenNode;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return String.format("Template[%s]", StringUtils.concat(queryPattern, ","));
	}
}
