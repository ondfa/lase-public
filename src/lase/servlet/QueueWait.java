package lase.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lase.servlet.RequestQueue.LaseSearchResult;
import lase.servlet.RequestQueue.Request;
import lase.util.LaseException;

/**
 * Servlet zajišťující čekání na zobrazení odpovědi (buď ve frontě nebo při již
 * během vyhledávání otázky).
 * 
 * @author Rudolf Šíma
 * 
 */
public class QueueWait extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    /**
     * Vytvoří novou instanci servletu.
     */
    public QueueWait() {
    }

	/**
	 * Zjistí stav čekání na zobrazení výsledků, tj. buď pozici ve frontě nebo
	 * poměrnou část vyhledávání odpovědi, která již byla dokončena a předá
	 * výsledky stránce k zobrazení.
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ServletUtils.setUnicode(request, response);
		ServletUtils.chdirToWebContent(this);
		
		RequestQueue requestQueue;
		try {
			requestQueue = RequestQueue.getInstance();
		} catch (LaseException e) {
			ServletUtils.throwUp(e);
			return;
		}
		
		String idStr = request.getParameter("request");
		if (idStr == null) {
			paramError(response);
			return;
		}
		int id;
		try {
			id = Integer.parseInt(idStr);
		} catch (NumberFormatException e) {
			paramError(response);
			return;
		}
		Request req = requestQueue.getRequestById(id);
		if (req == null) {
			paramError(response);
			return;
		}
		
		try {
			if (req.searchFinished()) {
				LaseSearchResult searchResult = req.getResult();
				ResultsBean resultsBean = new ResultsBean(searchResult);
				request.setAttribute("results", resultsBean);
				ServletUtils.dispatch("/index.jsp", this, request, response);
				return;
			}
		} catch (LaseException e) {
			ServletUtils.throwUp(e);
		}
		
		response.setHeader("refresh", "3; URL=./results?request=" + id);
		
		int seqNo = req.getSequenceNumber();
		int progress = (int) StrictMath.round(req.getProgress() * 100.0);
		String message = "";
		if (seqNo > 0)
			message = String.format("Váš požadavek čeká ve frontě na %d. místě. Čekejte, prosím...", seqNo + 1);
		else if (seqNo == 0) {
			if (progress == 0)
				message = String.format("Váš požadavek se vyřizuje, čekejte prosím...");	
			else
				message = String.format("Váš požadavek se vyřizuje: %d%%", progress);
		}
		
		request.setAttribute("message", message);
		
		ServletUtils.dispatch("/wait.jsp", this, request, response);
	}

	/**
	 * Provede přesměrování na základní stránku jako reakci na chybý parametr
	 * předaný metodou GET nebo POST.
	 * 
	 * @param response
	 *            Instance {@link HttpServletResponse} pro účely přesměrování
	 * 
	 * @throws IOException
	 *             Vyhozena v případě, že při přesměrování došlo k chybě.
	 */
	private void paramError(HttpServletResponse response) throws IOException {
		response.sendRedirect("./");
	}

	/**
	 * Alias metody {@link #doGet(HttpServletRequest, HttpServletResponse)}.
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
