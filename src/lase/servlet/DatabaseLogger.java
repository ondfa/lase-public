package lase.servlet;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import lase.app.Settings;

import org.apache.log4j.Logger;

/**
 * Třída sloužící k zápisu hodnocení odpovědí uživateli do databáze.
 * 
 * @author Rudolf Šíma
 *
 */
public class DatabaseLogger {
	
	/**
	 * Instance jediné instance této třídy. 
	 */
	public static DatabaseLogger instance;
	
	/**
	 * Udává, zda byl databázový ovladač již inicializován.
	 */
	public static boolean driverInitialized = false;
	
	
	/**
	 * Připojení k databázi.
	 */
	private Connection connection;

	/**
	 * Privátní konstruktor. Při prvním volání {@link #getInstance()} vytvoří
	 * jedinou instanci této třídy.
	 * 
	 * @throws SQLException
	 *             Vyhozena v případě, že při vytváření SQL připojení došlo k
	 *             chybě.
	 */
	private DatabaseLogger() throws SQLException {
		if (!driverInitialized) {
			try {
				Class.forName("com.mysql.jdbc.Driver");
				driverInitialized = true;
			} catch (ClassNotFoundException e) {
				Logger.getLogger(getClass()).error("JDBC driver not found.", e);		
			}
		}
		String connectionUri = getConnectionUri(Settings.databaseUrl, Settings.databaseName);
		this.connection = DriverManager.getConnection(connectionUri, Settings.databaseUserName, Settings.databasePassword);
	}

	/**
	 * Vrací instanci jedinou sdílenou instanci třídy {@link DatabaseLogger}.
	 * Při prvním volání vytvoří spojení.
	 * 
	 * @return Vrací instanci třídy {@link DatabaseLogger}.
	 * 
	 * @throws SQLException
	 *             Vyhozena v případě, že při vytváření instance došlo k chybě.
	 */
	public static DatabaseLogger getInstance() throws SQLException {
		if (instance == null) {
			instance = new DatabaseLogger();
			Runtime.getRuntime().addShutdownHook(new Thread() {
				public void run() {
					try {
						instance.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			});
		}
		return instance;
	}

	/**
	 * Uzavře spojení s databází.
	 * 
	 * @throws SQLException
	 *             Vyhozena v případě, že při uzavírání spojení došlo k chybě.
	 */
	private void close() throws SQLException {
		if (connection != null)
			connection.close();
	}

	/**
	 * Přidá uživatelské hodnocení do databáze.
	 * 
	 * @param question
	 *            Otázka, ke které se hodnocení vztahuje.
	 * @param answer
	 *            Hodnocená odpověď.
	 * @param rating
	 *            Hodnocení.
	 *            
	 * @throws SQLException
	 *             Vyhozena v případě, že při zápisu do databáze došlo k chybě.
	 */
	public void addRecord(String question, String answer, AnswerRating rating) throws SQLException {
		//PreparedStatement ps = connection.prepareStatement("INSERT INTO answer_rating VALUES (0, ?, ?, ?, DEFAULT)");
		PreparedStatement ps = connection.prepareStatement("INSERT INTO answer_rating VALUES (0, ?, ?, ?)");
		ps.setString(1, question);
		ps.setString(2, answer);	
		ps.setString(3, rating.toString());
		ps.execute();
		ps.close();
	}

	/**
	 * Serializuje univerzální cache systému LASE do databáze.
	 * 
	 * @param cache
	 *            Objekt, který bude serializován.
	 * 
	 * @throws SQLException
	 *             Vyhozena v případě, že při zápisu do databáze došlo k chybě.
	 * 
	 * TODO - Tato metoda nepatří do loggeru, změnit logger na třídu pro přístup do DB.
	 */
	public void storeCache(Object cache) throws SQLException {
		PreparedStatement ps = connection.prepareStatement(
				"REPLACE INTO `cache` VALUES (0, ?)");
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos;
		try {
			oos = new ObjectOutputStream(baos);
			oos.writeObject(cache);
			oos.close();
			byte[] bytes = baos.toByteArray();
			ps.setBytes(1, bytes);
			ps.execute();
		} catch (IOException unthrowable) {
			assert false;
			unthrowable.printStackTrace();
			return;
		} finally {
			ps.close();
		}
	}
	
	/**
	 * Deserializuje univerzální cache systému LASE dříve zapsanou do databáze.
	 * 
	 * @return Vrací deserializovaný objekt, který byl načten z databáze.
	 * 
	 * @throws SQLException
	 *             Vyhozena v případě, že při načítan dat z databáze došlo k
	 *             chybě.
	 *             
	 * TODO - Tato metoda nepatří do loggeru, změnit logger na třídu pro přístup do DB.
	 */
	public Object readCache() throws SQLException {
		PreparedStatement ps = connection.prepareStatement(
				"SELECT `cache` FROM `cache` WHERE `key` = 0");
		ResultSet rs = ps.executeQuery();
		if (!rs.first())
			return null;
		byte[] cacheObjectBytes = rs.getBytes("cache");
		ByteArrayInputStream bais = new ByteArrayInputStream(cacheObjectBytes);
		ObjectInputStream ois;
		Object cache;
		try {
			ois = new ObjectInputStream(bais);
			cache = ois.readObject();
			ois.close();
		} catch (Exception e) {
			Logger.getLogger(getClass()).warn("Invalid record in the database.", e);
			return null;
		} finally {
			rs.close();
			ps.close();
		}
		return cache;
	}

	/**
	 * Na základě jména databáze a URL serveru vytvoří URI pro připojení k
	 * databázi.
	 * 
	 * @param databaseUrl
	 *            URL databázového serveru.
	 * @param databaseName
	 *            Jméno databáze.
	 *            
	 * @return Vrací URI pro připojení k databázi.
	 */
	private static String getConnectionUri(String databaseUrl, String databaseName) {
		if (!databaseUrl.endsWith("/"))
			databaseUrl += "/";
		return String.format("jdbc:%s%s?useUnicode=true&characterEncoding=utf-8",
				databaseUrl, databaseName);
	}
	
	/**
	 * Výčet možných hodnocení odpovědi uživatelem.
	 * 
	 * @author Rudolf Šíma
	 *
	 */
	public static enum AnswerRating {
		
		/**
		 * Odpověď je správná.
		 */
		TRUE,
		
		/**
		 * Odpověď je špatná.
		 */
		FALSE,
		
		/**
		 * Uživatel neví, zda je odpověď správná.
		 */
		DONTKNOW,
		
		/**
		 * Odpověď je správná, ale není to odpověď
		 * vyhodnocená systémem LASE jako nejlepší.
		 */
		OTHERTRUE;
	}
}
