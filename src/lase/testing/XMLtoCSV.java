package lase.testing;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Načte XML soubor s výsledky testu a převede ho do CSV souboru.
 * @author Hessova
 * @version 1.00
 * @since 2.0
 *
 */
public class XMLtoCSV {
	
	public static final String INPUT_FILE_NAME = "input.xml";
	public static final String OUTPUT_FILE_NAME = "output.csv";
	public static final String HEADER1 = "Číslo otázky;Otázka;Šablona nalezena (true/false);Četnost odpovědi;";			;
	static final String HEADER2 = "Nalezená odpověď;Hodnocení;URL";
	
	String testResultsCount;
	String overallRating;
	
	//u každé položky bude 8 záznamů (číslo v závorce určuje pořadí výpisu):
	//číslo otázky (0) | templateFound (2)| question (1)| answerCount (3)| shortAnswer (4)| context (6)| URL (7)| rating (5)
	public static final int NUMBER_OF_ITEMS_IN_ROW = 8;
	
	/**
	 * Načte data z XML souboru a vytvoří pole polí řetězců.
	 * @param input
	 * @return
	 */
	public String[][] loadData(String input) {
		
			String[][] rows = null;
			
			File file = new File(input);
			
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder;
			Document doc = null;
			
			try {
				dBuilder = dbFactory.newDocumentBuilder();
				doc = dBuilder.parse(file);	 
			} catch (ParserConfigurationException e1) {
				e1.printStackTrace();
				System.exit(1);
			}
			catch (SAXException e1) {
				e1.printStackTrace();
				System.exit(1);
			}
			catch (IOException e1) {
				e1.printStackTrace();
				System.exit(1);
			}
			
			doc.getDocumentElement().normalize();		
						
			try {
				
				Node testResult = (doc.getElementsByTagName("TestResults")).item(0);
				testResultsCount = testResult.getAttributes().getNamedItem("count").getNodeValue();
				overallRating = testResult.getAttributes().getNamedItem("overallRating").getNodeValue();
						
				NodeList results = doc.getElementsByTagName("Result");
				
				rows = new String[results.getLength()][];
							
				for (int i = 0; i < results.getLength(); i++) {
					
					rows[i] = new String[NUMBER_OF_ITEMS_IN_ROW];
					
					rows[i][0] = (i + 1) + ";";
					
					Node resultNode = results.item(i);
					rows[i][2] = resultNode.getAttributes().getNamedItem("templateFound").getNodeValue() + ";";  //nalezení šablony
					 
					Element resultElement = (Element)resultNode;
					
					Node questionNode = (resultElement.getElementsByTagName("Question")).item(0);
	                Element questionElement = (Element)questionNode;	
	                
	                rows[i][1] = questionElement.getTextContent() + ";"; //otázka
	                
	                Node answerNode = (resultElement.getElementsByTagName("Answer")).item(0);	               
	                rows[i][3] = answerNode.getAttributes().getNamedItem("count").getNodeValue() + ";"; //četnost odpovědí
	                Element answerElement = (Element)answerNode;
	                
	                Node shortAnswerNode = (answerElement.getElementsByTagName("ShortAnswer")).item(0);
	                rows[i][4] = shortAnswerNode.getTextContent() + ";"; //stručná odpověď
	                
	                Node contextNode = (answerElement.getElementsByTagName("Context")).item(0);
	                rows[i][7] = contextNode.getTextContent() + ";"; //kontext
	                
	                Node URLNode = (answerElement.getElementsByTagName("URL")).item(0);
	                rows[i][6] =  URLNode.getTextContent() + ";"; //URL
	                
	                Node ratingNode = (resultElement.getElementsByTagName("Rating")).item(0);	
	                rows[i][5] = ratingNode.getTextContent() + "%;" ; //hodnocení
	            }
						
			} catch (Exception e) {
				e.printStackTrace();
			}	
			
		return rows;
	}
	
	/**
	 * Zapíše data do CSV souboru.
	 * @param output
	 * @param data
	 */
	public void writeDownData(String output, String[][] data) {
		
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(new File(output)));
			bw.write('\ufeff'); //BOM
			bw.write("Počet otázek;" + testResultsCount + System.lineSeparator());
			bw.write("Celkové hodnocení;" + overallRating + System.lineSeparator() + System.lineSeparator());
			for (int i = 0; i < data.length; i++) {
				String row1 = "";
				String row2 = "";
				for (int j = 0; j < data[i].length / 2; j++) {
					row1 += data[i][j];
					//row2 += data[i][j +  data[i].length/2];
				}
				for (int j = data[i].length/2; j < data[i].length - 1; j++) {
					row2 += data[i][j];
					
				}
				
				//System.out.println(row);
				bw.write(HEADER1 + System.lineSeparator());
				bw.write(row1 + System.lineSeparator());
				bw.write(HEADER2 + System.lineSeparator());
				bw.write(row2 + System.lineSeparator());
				bw.write(data[i][7] + System.lineSeparator() + System.lineSeparator());
				
			}
			bw.close();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}
		
	//---------------------------------main---------------------------------
	
	public static void main(String[] args) {
		XMLtoCSV conversion = new XMLtoCSV();
		String[][] data = conversion.loadData(INPUT_FILE_NAME);
		conversion.writeDownData(OUTPUT_FILE_NAME, data);
	}
	
	
}


