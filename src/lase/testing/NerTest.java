package lase.testing;

import gate.AnnotationSet;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import lase.util.Corpus;

public class NerTest {

	private static final String testFolder = "data/NER_test";
	private static final String serializedCorporaFile = testFolder + "/corpora.ser";
	private static final String questionsFile = testFolder + "/questions.txt";
	private static final String XMLFile = testFolder + "/document";
	private PassageRetrievalAdapter loader = new PassageRetrievalAdapter();
	
	public NerTest() {
	}

	public static void main(String[] args) {
		try {
			new NerTest().runTest();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public void runTest() throws IOException
	{
		File corporaFile = new File(serializedCorporaFile);
		List<String> questions = loadQuestions();
		Map<String,List<Corpus>> corporaMap = null;
		if(corporaFile.exists())
		{
			corporaMap = loader.deserializeCorpora(corporaFile);
		}
		else
		{
			corporaMap = loader.loadCorpora(questions);
			loader.serializeCorpora(corporaMap, corporaFile);
		}
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd(HH.mm)");
		Calendar cal = Calendar.getInstance();
		String date = dateFormat.format(cal.getTime());
		//File resultFile = new File(testFolder+"/result"+date+".txt");
		//PrintWriter pw = new PrintWriter(resultFile,"UTF-8");
		NerXMLWriter xml = new NerXMLWriter(XMLFile);
		NerTagger nerTagger = new NerTagger(xml);
		for(String question:questions)
		{
			nerTagger.analyze(question,NerTagger.writeAlways);
		}
		for(Map.Entry<String, List<Corpus>> me:corporaMap.entrySet())
		{
			//String taggedQuestion = nerTagger.analyze(me.getKey());
			//pw.println(taggedQuestion);
			for(Corpus corpus:me.getValue())
			{
				for(Corpus.Entry ce:corpus)
				{
					nerTagger.analyze(ce.getText(),NerTagger.writeLongOtherEntities);
					//pw.println(taggedSnippet);
				}
			}
			//pw.println();
		}
		//pw.close();
		xml.closeWriter();
	}
	
	private List<String> loadQuestions() throws IOException
	{
		BufferedReader bfr = new BufferedReader(new InputStreamReader(new FileInputStream(questionsFile),"UTF-8"));
		List<String> questions = new ArrayList<String>();
		String question = null;
		while((question = bfr.readLine())!=null)
		{
			questions.add(question);
		}
		bfr.close();
		return questions;
	}

}
