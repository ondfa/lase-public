package lase.testing;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import lase.answersearch.Answer;
import lase.answersearch.AnswerToken;
import lase.util.Corpus.SourcePage;

/**
 * grafické uživatelské rozhraní pro hodnocení odpovědí
 * @author Gabriela Hessová
 * @version 1.00
 * @since 2.0
 */
public class AnswerEvaluationGUI extends JFrame implements TestInterface {

	private static final long serialVersionUID = 1L;
	private static Color bg = new Color(240, 250, 255);
	
	/**
	 * reference na jádro testu potřebná k získávání informací o testu
	 */
	private TestCore test;
	
	/**
	 * seznam instancí {@code Entry} obsahující otázku, odpověď a očekávanou odpověď,
	 * do kterých se uloží hodnocení
	 */
	private Entry[] entries;
	
	/**
	 * index aktuálně zobrazené otázky
	 */
	int index;
	
	/**
	 * JList pro přepínání mezi otázkami
	 */
	final JList<String> list;
	
	JTextField queryField;
	JTextField shortAnswerField;
	JTextField expectedAnswerField;
	JTextArea contextAnswerArea;
	
	String[] labels = {"  Otázka:         ", "  Očekávaná odpověď:   ", "  Nalezená odpověď:      "};	
	
	JLabel label;
	JButton doneBTN;
	JPanel content;
	
	private boolean complete = false;
	
	/**
	 * název výstupního souboru bez přípony:
	 * je nastaven v metodě setOutputFileName(), která je volána v metodě writeDownResults()
	 */
	String outputFile; 
	
	/**
	 * Konstruktor
	 * @param queries
	 * @param answer
	 */
	public AnswerEvaluationGUI(TestCore test, Entry[] entries) {
		
		this.test = test;
		this.entries = entries;
		
		this.index = 0;		
		
		String[] data = new String[entries.length];
		for (int i = 0; i < entries.length; i++) {
			data[i] = "Otázka " + i;
		}
		list = new JList<String>(data);
		list.setSelectedIndex(index);
	
		
		initContent();
		this.setTitle("Vyhodnocení správnosti odpovědí poskytnutých systémem LASE");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.pack();
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
	
	/**
	 * Vytvoří obsah okna.
	 */
	private void initContent() {
		content = new JPanel(new BorderLayout());
		JPanel panel = new JPanel(new BorderLayout());
		panel.setBackground(bg);
		panel.add(getTextPanel(), BorderLayout.WEST);
		panel.add(getRightPanel(), BorderLayout.EAST);
		content.add(panel, BorderLayout.NORTH);
		content.add(getBottomPanel(), BorderLayout.SOUTH);
		this.add(content);
	}
	
	/**
	 * Vytvoří panel s texty v horní levé části okna.
	 * @return
	 */
	private Container getTextPanel() {
		
		int columns = 50;
		
		JPanel panel = new JPanel(new BorderLayout());
		
		JPanel topPanel = new JPanel(new BorderLayout());
		
		queryField = new JTextField();
		queryField.setBackground(bg);
		queryField.setColumns(columns);
		queryField.setText(labels[0] + entries[index].question);
		queryField.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 15));
		queryField.setEditable(false);
		topPanel.add(queryField, BorderLayout.NORTH);
		
		expectedAnswerField = new JTextField();
		expectedAnswerField.setBackground(bg);
		expectedAnswerField.setColumns(columns);
		expectedAnswerField.setText(labels[1] + entries[index].expectedAnswer);
		expectedAnswerField.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 15));
		expectedAnswerField.setEditable(false);
		topPanel.add(expectedAnswerField, BorderLayout.CENTER);	
		
		shortAnswerField = new JTextField();
		shortAnswerField.setBackground(bg);
		shortAnswerField.setColumns(columns);
		shortAnswerField.setText(entries[index].getAnswer()!=null? labels[2] + entries[index].getAnswer().toString():"Odpověď nenalezena");
		shortAnswerField.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 15));
		shortAnswerField.setEditable(false);
		topPanel.add(shortAnswerField, BorderLayout.SOUTH);		
		
		panel.add(topPanel, BorderLayout.CENTER);
		
		contextAnswerArea = new JTextArea();
		contextAnswerArea.setLineWrap(true);
		contextAnswerArea.setWrapStyleWord(true);
		contextAnswerArea.setColumns(columns);
		contextAnswerArea.setFont(new Font(Font.SANS_SERIF, Font.ITALIC, 15));
		contextAnswerArea.setRows(10);
		contextAnswerArea.setText("  " + (entries[index].getAnswer()!=null ? entries[index].getAnswer().getContext() : "Odpověď nenalezena"));
		contextAnswerArea.setEditable(false);
		JScrollPane scroller = new JScrollPane(contextAnswerArea);

		panel.add(scroller, BorderLayout.SOUTH);
		panel.setBackground(bg);
		return panel;
	}
	
	/**
	 * Vytvoří dolní panel, který obsahuje tlačítka.
	 * @return
	 */
	private Container getBottomPanel() {
		
		JPanel panel = new JPanel(new FlowLayout());
		
		label = new JLabel("OTÁZKA " + (index + 1) + "/" + entries.length + ":");
		panel.add(label);
		
		JLabel label2 = new JLabel("   Ohodnoťte správnost odpovědi:  ");
		panel.add(label2);
		
		JButton[] buttons = new JButton[5];		

		for (int i = 0; i < buttons.length; i++) {
			
			final int value = 100 - i * 25;
			String description = "" + value + "%";
			
			buttons[i] = new JButton(description);
			buttons[i].addActionListener(new ActionListener() {
			
				@Override
				public void actionPerformed(ActionEvent arg0) {
					entries[index].setRating(value);
					nextQuestion();
					
				}
			});
			panel.add(buttons[i]);
		}
		
		
		doneBTN = new JButton("HOTOVO");
		doneBTN.setEnabled(false);
		doneBTN.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent arg0) {				
				complete();
				showResults();
			}
		});
		panel.setBackground(bg);
		panel.add(doneBTN);
		
		return panel;
		
	}
	
	/**
	 * Vytvoří pravý panel, obsahující seznam otázek.
	 * @return
	 */
	private Container getRightPanel() {
		JPanel panel = new JPanel(new BorderLayout());
		panel.setBackground(bg);
		
		list.setBackground(panel.getBackground());
		list.addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent arg0) {
				index = list.getSelectedIndex();
				update();
				
			}
		});
		
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		JScrollPane scroller = new JScrollPane(list);
		scroller.setSize(90, 275);
		scroller.setPreferredSize(scroller.getSize());
		panel.add(scroller, BorderLayout.NORTH);

		return panel;
	}
	
	/**
	 * Přejde na další otázku.
	 */
	private void nextQuestion() {
		if (index == entries.length - 1) {
			index = 0;
		}
		else {
			index++;
		}
		update();
	}
	
	/**
	 * Aktualizuje obsah okna.
	 */
	private void update() {
		queryField.setText(labels[0] + entries[index].question);
		expectedAnswerField.setText(labels[1] + entries[index].expectedAnswer);
		shortAnswerField.setText(entries[index].getAnswer() != null ? labels[2] + entries[index].getAnswer().toString() : "Odpověď nenalezena.");
		contextAnswerArea.setText(entries[index].getAnswer() != null ? "  " + entries[index].getAnswer().getContext() : "Odpověď nenalezena.");
		list.setSelectedIndex(index);
		label.setText("OTÁZKA " + (index + 1) + "/" + entries.length + ":");
		if (finished()) {
			doneBTN.setEnabled(true);
		}		
	}
	
	/**
	 * Vrátí true, jestliže je všem odpovědím přiřazeno hodnocení.
	 * @return
	 */
	private boolean finished() {
		for (int i = 0; i < entries.length; i++) {
			if (entries[i].getRating() == -1) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Změní obsah okna tak, aby zobrazovalo výsledné hodnocení.
	 */
	private void showResults() {
		
		Dimension dimension = content.getSize();
		this.remove(content);

		content = new JPanel();
		content.setLayout(new BorderLayout());
		content.setSize(dimension);
		content.setPreferredSize(content.getSize());
		content.add(getResultsPanel(), BorderLayout.CENTER);
		//createTable();
		this.add(content);
		this.pack();
		this.setLocationRelativeTo(null);
		this.repaint();
		
	}
	
	/**
	 * Vytvoří panel s výsledným hodnocením.
	 * @return
	 */
	private Container getResultsPanel() {
		JPanel panel = new JPanel(new BorderLayout());
		
		JTextPane textPane = new JTextPane();
		String offset = "          ";
		
		String overall = "\n" + offset + "Celková úspěšnost: " + String.format("%.2f",test.getOverallRating(entries)) + "%";
		overall += "\n\n" + offset + "Celkem otázek: " + entries.length;
		int[] frequencies = test.getRatingFrequencies(entries);
		overall +="\n" + offset + "    s hodnocením 100%:     " + frequencies[4];
		overall +="\n" + offset + "    s hodnocením 75%:     " + frequencies[3];
		overall +="\n" + offset + "    s hodnocením 50%:     " + frequencies[2];
		overall +="\n" + offset + "    s hodnocením 25%:     " + frequencies[1];
		overall +="\n" + offset + "    s hodnocením 0%:     " + frequencies[0];
		
		textPane.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 15));
		
		String info = offset + "Výsledky jsou zapsány v souboru";
		this.outputFile = test.getOutputFileName();
		String resultsXMLFile = outputFile + ".xml";

		info += "\n" + offset + offset + offset + resultsXMLFile;
		
		overall += "\n\n  " + info;
		textPane.setText(overall);
		panel.add(textPane, BorderLayout.CENTER);
		
		
		JPanel buttonP = new JPanel();
		JButton closeBTN = new JButton("Zavřít");
		closeBTN.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
				
			}
		});
		
		buttonP.add(closeBTN);
		buttonP.setBackground(Color.WHITE);
		
		panel.add(buttonP, BorderLayout.SOUTH);


		return panel;
	}
	
	
	
	/**
	 * metoda zavolaná po ohodnocení všech odpovědí, probudí vlákno, které čeká na výsledek
	 */
	private synchronized void complete()
	{
		notify();
	}
	
	/* (non-Javadoc)
	 * @see lase.testing.TestGUI#getResults()
	 */
	@Override
	public synchronized Entry[] getResults()
	{
		if(!complete)
		{
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return entries;
	}
	
	
	/*Zkušební metoda main*/
	public static void main(String[] args) {
		
		String[] queries = new String[2];
		queries[0] = "Kdo založil Karlovu univerzitu?";
		queries[1] = "Kdo je prezidentem USA?";
		
		String[] expected = new String[2];
		expected[0] = "Karel IV.";
		expected[1] = "Barrack Obama";
		
		AnswerToken ansToken = new AnswerToken(null, "Karel") {
		};
		AnswerToken ansToken2 = new AnswerToken(null, "IV.") {
		};
		
		java.util.List<AnswerToken> tokenList = new java.util.ArrayList<AnswerToken>();
		
		tokenList.add(ansToken);
		tokenList.add(ansToken2);
		
		Answer ans = new Answer(tokenList, new SourcePage("ahoj", "www.ahoj.cz"), "Byl to Karel IV.");
		
		ansToken = new AnswerToken(null, "Barrack") {
		};
		ansToken2 = new AnswerToken(null, "Obama") {
		};
		
		tokenList = new java.util.ArrayList<AnswerToken>();
		
		tokenList.add(ansToken);
		tokenList.add(ansToken2);
		
		Answer ans2 = new Answer(tokenList, new SourcePage("bla", "www.bla.cz"), "Prezidentem USA je jak známo Barrack Obama. \n :)");
		
		Answer[] answers = {ans, ans2};
		
		Entry[] entries = new Entry[queries.length];
		
		for (int i = 0; i < entries.length; i++) {
			entries[i] = new Entry(queries[i], expected[i]);
		}
		
		for (int i = 0; i < entries.length; i++) {
			entries[i].setAnswer(answers[i]);
		}
		TestCore test = new OverallTest();
		TestInterface gui = new AnswerEvaluationGUI(test, entries);
		entries = gui.getResults();
		String outputFile = test.getOutputFileName();
		test.writeDownResults(outputFile, entries);
	}
	
	
	

}
