package lase.testing;

/**
 * rozhraní řídící třídy funkčního testu
 * @author Ondřej Pražák
 * @version 1.00
 * @since 2.0
 *
 */
public interface TestCore {

	/**
	 * Vyhledá odpovědi na dané otázky a uloží je do entries
	 * @param entries otázky a očekávané odpovědi
	 */
	public abstract void findAnswers(Entry[] entries);

	/**
	 * Načte otázky ze vstupního souboru.
	 * @param inputFile
	 * @return
	 */
	public abstract Entry[] readData(String inputFile);

	/**
	 * zapíše výsledky do výstupního souboru
	 * @param outputFile jméno výstupního souboru
	 * @param entries seznam {@code Entry} s ohodnocenými odpověďmi
	 */
	public abstract void writeDownResults(String outputFile, Entry[] entries);

	/**
	 * Nastaví název výstupního souboru podle aktuálního času.
	 */
	public abstract String getOutputFileName();
	
	/**
	 * přesměruje standartní výstup do souboru
	 */
	public void setOut();
	
	/**
	 * nastaví zpět standartní výstup
	 */
	public void resetOut();
	
	/**
	 * 
	 * @param entries otázky s ohodnocenými odpověďmi
	 * @return průměrné hodnocení odpovědí
	 */
	public double getOverallRating(Entry[] entries);
	
	/**
	 * 
	 * @param entries otázky s ohodnocenými odpověďmi
	 * @return počty jednotlivých procentuálních ohodnocení
	 */
	public int[] getRatingFrequencies(Entry[] entries);

}