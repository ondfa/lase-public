package lase.testing;

import java.util.Scanner;

/**
 * Rozhraní pro hodnocení odpovědí v příkazovém řádku
 * 
 * @author Ondřej Pražák
 * @version 1.0
 * @since 2.0
 *
 */
public class ComandLineInterface implements TestInterface {

	/**
	 * seznam instancí {@code Entry} obsahující otázku, odpověď a očekávanou odpověď,
	 * do kterých se uloží hodnocení
	 */
	private final Entry[] entries;


	public ComandLineInterface(Entry[] entries) {
		super();
		this.entries = entries;
	}


	/* (non-Javadoc)
	 * @see lase.testing.TestInterface#getResults()
	 */
	@Override
	public Entry[] getResults() {
		Scanner sc = new Scanner(System.in);
		for(int i = 0;i<entries.length;i++)
		{
			System.out.println("Otázka: "+entries[i].question);
			System.out.println("Očekávaná odpověď: "+entries[i].expectedAnswer);
			System.out.println("Odpověď: "+entries[i].getAnswer());
			System.out.println("Ohodnoť odpověď(0-100%:");
			int rank = sc.nextInt();
			if(rank<0 ||rank>100)
			{
				System.out.println("hodnocení musí být mezi 0 a 100");
				i--;
				continue;
			}
			entries[i].setRating(rank);
		}
		sc.close();
		return entries;
	}

}
