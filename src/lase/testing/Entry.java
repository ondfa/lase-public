package lase.testing;

import lase.answersearch.Answer;

/**
 * Třída {@code Entry} je přepravkou uchovávající otázky, očekávanou a skutečnou odpověď
 * a hodnocení uživatelem, obsahuje také pár doplňujících informací o odpovědi
 * @author Gabriela Hessová
 * @version 1.00
 * @since 2.0
 *
 */
public class Entry {
	
	public final String question;
	public final String expectedAnswer;
	private Answer answer;
	
	/**
	 * četnost odpovědi - kolikrát byla tako odpověď v corpusu nalezena
	 */
	private int answerCount;
	private int rating;
	
	/**
	 * informace zda byla nalezena šablona
	 */
	private boolean templateFound = false;
	
	
	public Entry(String question, String expectedAnswer) {
		this.question = question;
		this.expectedAnswer = expectedAnswer;
		this.rating = -1;
	}
	
	/**
	 * @return the answer
	 */
	public Answer getAnswer() {
		return answer;
	}

	/**
	 * @param answer the answer to set
	 */
	public void setAnswer(Answer answer) {
		this.answer = answer;
	}

	/**
	 * @return the rating
	 */
	public int getRating() {
		return rating;
	}

	/**
	 * @param rating the rating to set
	 */
	public void setRating(int rating) {
		this.rating = rating;
	}
	
	/**
	 * @return the answerCount
	 */
	public int getAnswerCount() {
		return answerCount;
	}

	/**
	 * @param answerCount the answerCount to set
	 */
	public void setAnswerCount(int answerCount) {
		this.answerCount = answerCount;
	}

	public boolean isTemplateFound() {
		return templateFound;
	}

	public void setTemplateFound(boolean templateFound) {
		this.templateFound = templateFound;
	}
	
	

	
}
