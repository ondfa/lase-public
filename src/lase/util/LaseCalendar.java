package lase.util;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Calendar;

/**
 * <p>
 * Kalendář systému LASE. Slouží k jednoduchému uchování časových údajů v
 * případě, že s nimi není třeba provádět žádné operace, které umožňuje
 * {@link java.util.Calendar}, který ovšem tím pádem přináší řadu komplikací.
 * </p>
 * 
 * <p>
 * Po vytvoření prázdného kalendáře není nastavené žádné pole (pole = položka
 * kalendáře, např. rok).
 * 
 * S údaji v kalendáři se pracuje čtveřicí metody:
 * <ul>
 * <li>{@link #set(int, int)}</li>
 * <li>{@link #unset(int)}</li>
 * <li>{@link #isSet(int)}</li>
 * <li>{@link #get(int)}</li>
 * </ul>
 * Pole kalendáře, nemusí být nastaveno (nebo jej lze odnastavit voláním
 * {@link #unset(int)}). To umožňuje například zjitit, zda kalendář obsahuje
 * pouza datum, pouze čas nebo obojí. Případně může obsahovat pouze měsíc a rok,
 * ale den nemusí být nastaven, atp.
 * 
 * @author Rudolf Šíma
 * 
 */
public class LaseCalendar implements Cloneable, Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 *	Počet políček v kalendáři. Po statické inicializaci má hodnotu 6.
	 */
	private static transient int fieldsCount = 0;

	/**
	 * Index údaje kalendáře, který označuje rok. Tato konstanta se používá jako
	 * identifikátor údaje kalendáře (pole kalendáře) v následujících metodách:
	 * <ul>
	 * <li>{@link #set(int, int)}</li>
	 * <li>{@link #unset(int)}</li>
	 * <li>{@link #isSet(int)}</li>
	 * <li>{@link #get(int)}</li>
	 * </ul>
	 */
	public static final transient int YEAR = fieldsCount++;
	
	/**
	 * Index údaje kalendáře, který označuje měsíc. Tato konstanta se používá jako
	 * identifikátor údaje kalendáře (pole kalendáře) v následujících metodách:
	 * <ul>
	 * <li>{@link #set(int, int)}</li>
	 * <li>{@link #unset(int)}</li>
	 * <li>{@link #isSet(int)}</li>
	 * <li>{@link #get(int)}</li>
	 * </ul>
	 */
	public static final transient int MONTH = fieldsCount++;
	
	/**
	 * Index údaje kalendáře, který označuje den. Tato konstanta se používá jako
	 * identifikátor údaje kalendáře (pole kalendáře) v následujících metodách:
	 * <ul>
	 * <li>{@link #set(int, int)}</li>
	 * <li>{@link #unset(int)}</li>
	 * <li>{@link #isSet(int)}</li>
	 * <li>{@link #get(int)}</li>
	 * </ul>
	 */
	public static final transient int DAY = fieldsCount++;
	
	/**
	 * Index údaje kalendáře, který označuje hodinu. Tato konstanta se používá jako
	 * identifikátor údaje kalendáře (pole kalendáře) v následujících metodách:
	 * <ul>
	 * <li>{@link #set(int, int)}</li>
	 * <li>{@link #unset(int)}</li>
	 * <li>{@link #isSet(int)}</li>
	 * <li>{@link #get(int)}</li>
	 * </ul>
	 */
	public static final transient int HOUR = fieldsCount++;
	
	/**
	 * Index údaje kalendáře, který označuje minutu. Tato konstanta se používá jako
	 * identifikátor údaje kalendáře (pole kalendáře) v následujících metodách:
	 * <ul>
	 * <li>{@link #set(int, int)}</li>
	 * <li>{@link #unset(int)}</li>
	 * <li>{@link #isSet(int)}</li>
	 * <li>{@link #get(int)}</li>
	 * </ul>
	 */
	public static final transient int MINUTE = fieldsCount++;
	
	/**
	 * Index údaje kalendáře, který označuje sekundu. Tato konstanta se používá jako
	 * identifikátor údaje kalendáře (pole kalendáře) v následujících metodách:
	 * <ul>
	 * <li>{@link #set(int, int)}</li>
	 * <li>{@link #unset(int)}</li>
	 * <li>{@link #isSet(int)}</li>
	 * <li>{@link #get(int)}</li>
	 * </ul>
	 */
	public static final transient int SECOND = fieldsCount++;

	/**
	 * Konstanta udávající hodnotu pole kalendáře, která není nastaveno, tj.
	 * nebylo vůbec nestaveno nebo bylo odnastaveno voláním {@link #unset(int)}.
	 */
	public static final transient int fieldNotSet = -1;
	
	/**
	 * Prázdný kalendář, ve kterém není nastaveno žádné pole (údaj), tj. všechna
	 * mají hodnotu {@link #fieldNotSet}. Tato konstanta se využívá k
	 * inicializaci nově vytvořeného prázdného kalendáře.
	 */
	private static final transient int[] emptyCalendar = new int[fieldsCount];
	static {
		for (int i = 0; i < emptyCalendar.length; i++)
			emptyCalendar[i] = fieldNotSet;
	}

	/**
	 * Pole údajů kalendáře. Při vytvoření prázdného kalendáře je inicializováno
	 * obsahem pole {@link #emptyCalendar} a dále může být měněno volání
	 * následujících metod:
	 * <ul>
	 * <li>{@link #set(int, int)}</li>
	 * <li>{@link #unset(int)}</li>
	 * <li>{@link #isSet(int)}</li>
	 * <li>{@link #get(int)}</li>
	 * </ul>
	 */
	private int[] fields;

	/**
	 * Vytvoří nový prázdný kalendář, ve kterém není nastaven žádný údaj.
	 */
	public LaseCalendar() {
		unsetAll();
	}

	/**
	 * Vytvoří nový kalendář a inicializuje všechna jeho pole. Pole je
	 * při inicializaci označit jako nenastavená tak, že se na místě příslušného
	 * parametru použije jako argument konstanta
	 * {@link LaseCalendar#fieldNotSet}.
	 * 
	 * @param year
	 *            Údaj označující rok.
	 * @param month
	 *            Údaj označující měsíc.
	 * @param day
	 *            Údaj označující den.
	 * @param hour
	 *            Údaj označující hodinu.
	 * @param minute
	 *            Údaj označující minutu.
	 * @param second
	 *            Údaj označující sekundu.
	 */
	public LaseCalendar(int year, int month, int day, int hour, int minute, int second) {
		fields = new int[fieldsCount];
		fields[YEAR] = year;
		fields[MONTH] = month;
		fields[DAY] = day;
		fields[HOUR] = hour;
		fields[MINUTE] = minute;
		fields[SECOND] = second;
	}

	/**
	 * Zjistí, zda je dané pole kalendáře nastaveno.
	 * 
	 * @param field
	 *            Zkoumané pole kalendáře. Možné hodnoty jsou:
	 *            <ul>
	 *            <li>YEAR</li>
	 *            <li>MONTH</li>
	 *            <li>DAY</li>
	 *            <li>HOUR</li>
	 *            <li>MINUTE</li>
	 *            <li>SECOND</li>
	 *            </ul>
	 *            
	 * @return Vrací {@code true}, je-li pole kalendáře nastaveno, jinak vrací
	 *         {@code false}.
	 */
	public boolean isSet(int field) {
		return fields[field] > fieldNotSet;
	}

	/**
	 * Zjistí hodnotu pole kalendáře. Před voláním této metodu je třeba
	 * zajistit, aby bylo pole nastaveno, jinak dojde k výjimce. Zjistit, zda je
	 * pole nastaveno lze voláním metody {@link #isSet(int)}.
	 * 
	 * @param field
	 *            Pole kalendáře, jehož hodnota se má zjistit. Možné hodnoty jsou:
	 *            <ul>
	 *            <li>YEAR</li>
	 *            <li>MONTH</li>
	 *            <li>DAY</li>
	 *            <li>HOUR</li>
	 *            <li>MINUTE</li>
	 *            <li>SECOND</li>
	 *            </ul>
	 * 
	 * @return Vrací hodnotu uloženou v daném poli kalendáře.
	 * 
	 * @throws IllegalStateException
	 *             Vyhozena v případě, že pole kalendáře není nastaveno.
	 */
	public int get(int field) {
		if (field < 0)
			throw new IllegalStateException("Field not set.");
		return fields[field];
	}

	/**
	 * Nastaví hodnotu pole kalendáře.
	 * 
	 * @param field
	 *            Pole kalendáře, jehož hodnota bude nastavena. Možné hodnoty
	 *            jsou:
	 *            <ul>
	 *            <li>YEAR</li>
	 *            <li>MONTH</li>
	 *            <li>DAY</li>
	 *            <li>HOUR</li>
	 *            <li>MINUTE</li>
	 *            <li>SECOND</li>
	 *            </ul>
	 * 
	 * @param value
	 *            Nová hodnota pole, která bude nastavena.
	 */
	public void set(int field, int value) {
		fields[field] = value;
	}

	/**
	 * Odnastaví (zruší nastavení) daného pole kalendář. Po odnastavení se bude
	 * pole nacházet ve stejném stavu jako po vytvoření prázdného kalendáře,
	 * tedy metoda {@link #isSet(int)} bude vracet pro toto pole {@code false} a
	 * volání {@link #get(int)} způsobí vyhození {@link IllegalStateException}.
	 * 
	 * @param field
	 *            Pole kalendáře, které má být odnastaveno. Možné hodnoty jsou
	 *            <ul>
	 *            <li>YEAR</li>
	 *            <li>MONTH</li>
	 *            <li>DAY</li>
	 *            <li>HOUR</li>
	 *            <li>MINUTE</li>
	 *            <li>SECOND</li>
	 *            </ul>
	 */
	public void unset(int field) {
		fields[field] = fieldNotSet;
	}
	
	/**
	 * Odnastaví (zruší nastavení) všech polí kalendáře. Po odnastavení se budou
	 * pole nacházet ve stejném stavu jako po vytvoření prázdného kalendáře,
	 * tedy metoda {@link #isSet(int)} bude vracet pro všechna pole {@code
	 * false} a volání {@link #get(int)} způsobí vyhození
	 * {@link IllegalStateException}.
	 */
	public void unsetAll() {
		fields = Arrays.copyOf(emptyCalendar, emptyCalendar.length);
	}

	/**
	 * Vytvoří kalendář {@link java.util.Calendar} na základě údajů tohoto
	 * kalendáře.
	 * 
	 * @return Vrací vytvořený kalendář.
	 */
	public Calendar getCalendar() {
		// TODO - Implementovat vytvoření kalendáře.
		throw new NoSuchMethodError("Calendar creation not implemented.");
	}

	/**
	 * Vrací řetězcem reprezentovaný obsah kalendáře pro ladicí účely.
	 */
	@Override
	public String toString() {
		return "LaseCalendar[" + Arrays.toString(fields) + "]";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return Arrays.hashCode(fields);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LaseCalendar other = (LaseCalendar) obj;
		return Arrays.equals(fields, other.fields);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() {
		LaseCalendar copy;
		try {
			copy = (LaseCalendar) super.clone();
		} catch (CloneNotSupportedException unthrowable) {
			throw new AssertionError();
		}
		copy.fields = Arrays.copyOf(fields, fields.length);
		return copy;
	}
}