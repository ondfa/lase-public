package lase.util;

import java.io.Serializable;
import java.util.Arrays;

/**
 * <p>
 * Generický wrapper pole.
 * </p>
 * 
 * <p>
 * Použití tohoto wrapperu je výhodné například v případě generických metod s
 * proměnným počtem parametrů, při jejichž volání není možné užít přímo pole
 * samotné, protože by neplnilo funkci jednoho argumentu, ale tolika argumentů,
 * kolik má prvků.
 * </p>
 * 
 * @author Rudolf Šíma
 * 
 * @param <T>
 *            Typ prvků pole.
 */
public class ArrayWrapper<T> implements Serializable {
	private static final long serialVersionUID = 1L;
	
	/**
	 * Pole obalené tímto wrapper.
	 */
	private T[] array;
	
	/**
	 * Vytvoří nový prázdný wrapper, který neobsahuje žádné pole. Toto pole lze
	 * později nastavit voláním metody {@link #setArray(Object[])}.
	 */
	public ArrayWrapper() {
	}
	
	/**
	 * Vytvoří nový wrapper do kterého umístí dané pole.
	 * 
	 * @param array
	 *            - Pole jehož reference se umístí do wrapper.
	 */
	public ArrayWrapper(T[] array) {
		setArray(array);
	}

	/**
	 * Vrací pole umístěné ve wrapperu. Pokud je wrapper prázdný, vrací {@code
	 * null}.
	 * 
	 * @return Vrací pole ve wrapper nebo {@code null}.
	 */
	public T[] getArray() {
		return array;
	}

	/**
	 * Umístí pole do wrapperu, které případě nahradí pole, které ve wrapperu
	 * bylo dříve.
	 * 
	 * @param array - Pole které má být umístěno do wrapperu.
	 */
	public void setArray(T[] array) {
		this.array = array;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return Arrays.hashCode(array);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ArrayWrapper<?> other = (ArrayWrapper<?>) obj;
		if (!Arrays.equals(array, other.array))
			return false;
		return true;
	}
	
	/* (non-Javadoc)
	 * @see Arrays#toString()
	 */
	@Override
	public String toString() {
		return Arrays.toString(array);
	}
}
