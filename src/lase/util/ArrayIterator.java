package lase.util;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * <p>
 * Jednosměrný iterátor pole bez možnosti odebírání prvků.
 * </p>
 * 
 * <p>
 * Při použití tohoto iterátoru není možné odebírat prvky pole. Volání metody
 * {@link #remove()} vždy způsobí vyhození výjimky
 * {@link UnsupportedOperationException}.
 * </p>
 * 
 * @author Rudolf Šíma
 * 
 * @param <T>
 *            Datový typ prvků pole.
 */
public class ArrayIterator<T> implements Iterator<T> {
	/**
	 *	Následující index v poli.
	 */
	private int nextIndex = 0;
	
	/**
	 *	Pole, přes jehož prvky se iterace provádí.
	 */
	private T[] array;

	/**
	 * Vytvořní nový read-only iterátor pole
	 * 
	 * @param array
	 *            Pole přes jehož prvky se iteruje.
	 */
	public ArrayIterator(T[] array) {
		this.array = array;
	}

	/**
	 * Vrací {@code true}, je-li k dispozici alespoň jeden prvek pole, který dosud
	 * nebyl vrácen. Jinak vací {@code false}.
	 * 
	 * @return Vrací {@code true}, je-li k dispozici alespoň jeden prvek pole,
	 *         který dosud nebyl vrácen. Jinak vací {@code false}.
	 */
	@Override
	public boolean hasNext() {
		return nextIndex < array.length;
	}

	/**
	 * Vrací další prvek pole s indexem o 1 vyšším, než který byl vrácen při
	 * předchozím volání.
	 * 
	 * @return Vrací následující prvek pole.
	 * 
	 * @throws NoSuchElementException
	 *             Vyhozena v případě, že již není k dispozici žádná prvek,
	 *             který by byl vrácen (předchozí vrácený prvek byl poslední
	 *             resp. je to první volání {@link #next()} a pole, přes které
	 *             se provádí iterace, má nulovou délku.)
	 */
	@Override
	public T next() {
		if (nextIndex > array.length)
			throw new NoSuchElementException();
		return array[nextIndex++];
	}

	/**
	 * Odebrání prvku tímto iterátorem není možné. Volání metody {@link #remove()} vždy
	 * způsobí vyhození výjimky {@link NoSuchElementException}.
	 */
	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}

	/**
	 * Vrací instanci rozhraní Iterable odpovídající tomuto iterátoru, což
	 * umožňuje snadné použití v cyklech nebo použití jako argument při volání
	 * metody, která má parametr typu iterable.
	 * 
	 * @return Vrací instanci rozhraní Iterable.
	 */
	public Iterable<T> getIterable() {
		return IterationUtils.newIterable(this);
	}
}
