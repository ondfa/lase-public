package lase.util;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.NoSuchElementException;

/**
 * <p>
 * Tokenizátor řetězce, který narozdíl třídy {@link java.util.StringTokenizer}
 * umožňuje použít víceznakové oddělovače.
 * </p>
 * 
 * <p><strong>Poznámka:</strong> Tato třída by mohla být implementována mnohem efektivněji,
 * pokud by vznikla potřeba, aby pracovala rychle.</p>
 * 
 * @author Rudolf Šíma
 * 
 */
public class MulticharDelimiterStringTokenizer{

	/**
	 * Řetězec určený k tokenizaci.
	 */
	private String string;
	
	/**
	 * Seznam oddělovačů.
	 */
	private LinkedList<String> delimiters;

	/**
	 * Vytvoří nový tokenizátor, nastaví řetězec určený k tokenizaci a seznam oddělovačů
	 * @param string - Řetězec určený k tokenizaci.
	 * @param delimiters - Seznam oddělovačů.
	 */
	public MulticharDelimiterStringTokenizer(String string, String... delimiters) {
		this.string = string;
		this.delimiters = new LinkedList<String>();
		for (String delim : delimiters)
			this.delimiters.add(delim);
	}

	/**
	 * Zjistí, ze ještě zbývají nějaké tokeny, které dosud nebyly vráceny.
	 * 
	 * @return Vrací {@code true}, pokud řetězec ještě obsahuje nějaké zbývající
	 *         tokeny. Jinak vrací {@code false}.
	 */
	public boolean hasNextToken() {
		return !string.isEmpty();
	}

	/**
	 * Vrací následující token, tj. ten, který následuje po předchozím vráceném,
	 * případně první token v případě prvního volání této metody. Každý nalezený
	 * oddělovač je také vráce touto metodou jako jeden z tokenů.
	 * 
	 * @return Vrací následující token.
	 * 
	 * @throws NoSuchElementException
	 *             Vyhozena v případě, že již není k dispozici žádný zbývající
	 *             token, tj. metoda {@link #hasNextToken()} vrací {@code false}.
	 */
	public String nextToken() {
		if (string.isEmpty())
			throw new NoSuchElementException();
		int shortestLength = string.length();
		String shortestDelim = null; 
		for (String delim : delimiters) {
			if (string.startsWith(delim)) {
				shortestDelim = delim;
				if (delim.length() < shortestLength)
					shortestLength = delim.length();
			}
		}
		if (shortestDelim != null) {
			string = string.substring(shortestLength);
			return shortestDelim;
		}
		int minIndex = string.length();
		Iterator<String> delimIt = delimiters.iterator();
		while(delimIt.hasNext()) {
			int index = string.indexOf(delimIt.next());
			if (index > 0) {
				if (index < minIndex)
					minIndex = index;
			} else {
				delimIt.remove();
			}
		}
		String result = string.substring(0, minIndex); 
		string = string.substring(minIndex);
		return result;
	}
}
