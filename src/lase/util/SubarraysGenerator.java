package lase.util;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * Generátor podmnožin pole. Generuje všechny podmnožiny daného pole složené ze
 * sousedních prvků. Je možné omezit maximální a minimálné délku generovaných
 * polí.
 * </p>
 * 
 * <p>
 * <strong>Příklad:</strong> V případě pole s prvky [1, 2, 3, 4] bez omezení
 * délky budou generovány tyto podmnožiny:
 * <pre>
 * [1]
 * [2]
 * [3]
 * [4]
 * [1, 2]
 * [2, 3]
 * [3, 4]
 * [1, 2, 3]
 * [2, 3, 4]
 * [1, 2, 3, 4]
 * </pre>
 * 
 * Při omezení délky od dvou do tří znaků budou generovány tyto podmnožiny:
 * <pre>
 * [1, 2]
 * [2, 3]
 * [3, 4]
 * [1, 2, 3]
 * [2, 3, 4]
 * </pre>
 * </p>
 * 
 * 
 * @author Rudolf Šíma
 * 
 */
public class SubarraysGenerator {
	
	/**
	 * Maximální délka generovaných polí.
	 */
	private int maxLen;
	
	/**
	 * Minimální délka generovaných polí.
	 */
	private int minLen;

	/**
	 * Vytvoří nový generátor a nastaví minimální a maximální délku generovaných
	 * polí.
	 * 
	 * @param minimumSubstringLength
	 *            Minimální délka generovaných polí.
	 * @param maximumSubstringLength
	 *            Maximální délka generovaných polí.
	 */
	public SubarraysGenerator(int minimumSubstringLength, int maximumSubstringLength) {
		this.minLen = minimumSubstringLength;
		this.maxLen = maximumSubstringLength;
	}

	/**
	 * Vytvoří nový generátor a nastaví minimální délku generovaných polí.
	 * Maximální délka je nastavena na nekonečno.
	 * 
	 * @param minimumSubstringLength
	 *            Minimální délka generovaných polí.
	 */
	public SubarraysGenerator(int minimumSubstringLength) {
		this(minimumSubstringLength, -1);
	}

	/**
	 * Vytvoří nový generátor bez nastavení minimální a maximální délky
	 * generovaných polí, tzn. minimální délka je nastavena na hodnotu 1,
	 * maximální délka na nekonečno.
	 */
	public SubarraysGenerator() {
		this(1, -1);
	}

	/**
	 * <p>
	 * Generuje podmnožiny pole {@code array} složené ze sousedních prvků s
	 * ohledem na nastavené minimální a maximální délky generovaných polí.
	 * </p>
	 * 
	 * <p>
	 * <strong>Příklad:</strong> V případě pole s prvky [1, 2, 3, 4] bez omezení
	 * délky budou generovány tyto podmnožiny:
	 * <pre>
	 * [1]
	 * [2]
	 * [3]
	 * [4]
	 * [1, 2]
	 * [2, 3]
	 * [3, 4]
	 * [1, 2, 3]
	 * [2, 3, 4]
	 * [1, 2, 3, 4]
	 * </pre>
	 * Při omezení délky od dvou do tří znaků budou generovány tyto podmnožiny:
	 * 
	 * <pre>
	 * [1, 2]
	 * [2, 3]
	 * [3, 4]
	 * [1, 2, 3]
	 * [2, 3, 4]
	 * </pre>
	 * </p>
	 * 
	 * @param array
	 *            Pole jehož podmnožiny budou generovány.
	 * 
	 * @return Vrací seznam generovaných polí.
	 */
	public List<Object[]> generate(Object[] array) {
		List<Object[]> result = new ArrayList<Object[]>();
		int len = maxLen >= 0 ? Math.min(maxLen, array.length)
				: array.length;
		for (int l = minLen; l <= len; l++) {
			for (int i = 0; i < array.length - l + 1; i++) {
				Object[] newSubstr = (Object[]) new Object[l];
				for (int k = 0; k < newSubstr.length; k++) {
					newSubstr[k] = array[i + k];
				}
				result.add(newSubstr);
			}
		}
		return result;
	}
}
