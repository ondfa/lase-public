package lase.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.RandomAccess;

/**
 * Seznam objektů typu {@code T} s náhodným přístupem (implementovaný třídou
 * {@link ArrayList}), který neumožňuje změnu své struktury. Při vytvoření je
 * inicializován seznamem ({@link List}), jehož prvky jsou zkopírovány do
 * interního kontejneru. Od této chvíle nelze strukturo kontejneru změnit.
 * 
 * @author Rudolf Šíma
 * 
 * @param <T>
 *            Typ objektů, které mohou být do vektoru vloženy.
 */
public class ReadOnlyVector<T> implements Iterable<T>, RandomAccess, Serializable {
	private static final long serialVersionUID = 1L;
	
	/**
	 * Seznam s náhodným přístupem implementující kontejner tohoto vektoru.
	 */
	private List<T> list;

	/**
	 * Vytvoří nový prázdný vektor. Tento vektor je vždy read-only, proto již
	 * nelze přidávat žádné prvky. Pro inicializaci seznamem prvků je nutné
	 * použít konstruktor {@link ReadOnlyVector#ReadOnlyVector(List)}.
	 */
	public ReadOnlyVector() {
		list = new ArrayList<T>();
	}

	/**
	 * Vytvoří nový vektor a inicializuje jej seznamem prvků. Tento seznam je
	 * zkopírován do interního kontejneru tohoto vektoru a již není možné měnit
	 * jeho strukturu, tj. přidávat nebo odebírat prvky.
	 * 
	 * @param list
	 *            Seznam, jehož prvky budou zkopírovány do tohoto vektoru.
	 */
	public ReadOnlyVector(List<T> list) {
		this.list = new ArrayList<T>(list);
	}

	/**
	 * Vrací prvek vektoru se zadaným indexem v čase O(1).
	 * 
	 * @param index
	 *            Index prvku, který bude vrácen.
	 * 
	 * @return Vrací prvek na pozici {@code index}.
	 * 
	 * @throws IndexOutOfBoundsException
	 *             v případě, že vektor neobsahuje prvek s tímto indexem.
	 */
	public T item(int index) {
		return list.get(index);
	}
	
	/**
	 * Zjistí velikost (počet prvků) vektoru.
	 * 
	 * @return Vrací velikost vektoru.
	 */
	public int size() {
		return list.size();
	}

	/**
	 * Vrací read-only iterátor prvků tohoto vektoru. Tímto iterátorem
	 * tedy nelze odebírat prvky.
	 */
	public Iterator<T> iterator() {
		return IterationUtils.readOnlyIterator(list.iterator());
	}
}
