package lase.cache;

import java.util.NoSuchElementException;

/**
 * Runtime verze výjimky <code>{@link NoSuchElementException}</code>.
 * 
 * @author Rudolf Šíma
 * 
 * @deprecated Používat {@link lase.cache2.Cache}.
 * 
 */
@Deprecated
public class NoSuchMethodRuntimeException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public NoSuchMethodRuntimeException() {
	}

	public NoSuchMethodRuntimeException(String message, Throwable cause) {
		super(message, cause);
	}

	public NoSuchMethodRuntimeException(String message) {
		super(message);
	}

	public NoSuchMethodRuntimeException(Throwable cause) {
		super(cause);
	}
}