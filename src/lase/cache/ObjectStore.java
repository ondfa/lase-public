package lase.cache;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Utilita sloužící k serializaci objektů pro rychlejší načtení při ladění.
 * 
 * @author Rudolf Šíma
 * 
 * @deprecated Tato třída není používána a může být odstraněna. Navíc slouží
 *             pouze pro účely ladění, na případné výjimky není z hlediska
 *             volajícího možné reagovat.
 */
@Deprecated
public class ObjectStore {
		
	public static Object readObject(Class<?> storedClass) {
		String filename = getFilename(storedClass.getName());
		try {
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filename));
			return ois.readObject();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static void writeObject(Object obj) {
		String filename = getFilename(obj.getClass().getName());
		try {
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filename));
			oos.writeObject(obj);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static String getFilename(String className) {
		return "cache/" + className + ".cache";
	}
	
}
