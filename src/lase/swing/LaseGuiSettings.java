package lase.swing;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;

/**
 * Nastavení GUI pro LASE schopné serializovat samo sebe do XML souboru
 * {@value #settingsPath}.
 * 
 * @author Rudolf Šíma
 * 
 * @see LaseGui
 * 
 */
public class LaseGuiSettings implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * Relativní cesta k souboru, do kterého se nastavení ukládá a ze kterého se
	 * načítá.
	 */
	public transient static final String settingsPath = "data/gui/settings.xml";

	/**
	 * Poslední hledaná odpověď.
	 */
	private String lastQuestion;

	/**
	 * Načte nastavení ze souboru {@link #settingsPath}.
	 * 
	 * @return Vrací načtené nastavení.
	 * 
	 * @throws IOException
	 *             Vyhozena v případě, že při načítání nastavení došlo k chybě.
	 */
	public static LaseGuiSettings loadSettings() throws IOException {
		FileInputStream fis;
		try {
			fis = new FileInputStream(settingsPath);
		} catch (FileNotFoundException e) {
			return new LaseGuiSettings();
		}
		try {
			XMLDecoder xmlDecoder = new XMLDecoder(fis);
			try {
				try {
					return LaseGuiSettings.class.cast(xmlDecoder.readObject());
				} catch (ArrayIndexOutOfBoundsException e) {
					return new LaseGuiSettings();
				}
			} finally {
				xmlDecoder.close();
			}
		} finally {
			fis.close();
		}
	}

	/**
	 * Uloží nastavení do souboru {@link #settingsPath}.
	 * 
	 * @throws IOException
	 *             Vyhozena v případě, že při ukládání nastaveí došlo k chybě.
	 */
	public void saveSettings() throws IOException {
		FileOutputStream fos = new FileOutputStream(settingsPath);
		try {
			XMLEncoder xmlEncoder = new XMLEncoder(fos);
			try {
				xmlEncoder.writeObject(this);
			} finally {
				xmlEncoder.close();
			}
		} finally {
			fos.close();
		}
	}

	/**
	 * Vrací poslední uživatelem zadanou otázku.
	 * 
	 * @return Vrací poslední hledanou otázku.
	 */
	public String getLastQuestion() {
		return lastQuestion;
	}

	/**
	 * Nastaví poslední uživatelem zadanou otázku.
	 * 
	 * @param lastQuestion
	 *            Poslední hledaná otázka.
	 */
	public void setLastQuestion(String lastQuestion) {
		this.lastQuestion = lastQuestion;
	}
}
