package lase.swing;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.swing.JTextArea;

/**
 * {@link OutputStream}, který připojuje text do {@link JTextArea}. 
 * 
 * @author Rudolf Šíma
 *
 */
class TextAreaOutputStream extends OutputStream {
	
	/**
	 * JTextArea kam se text přidává.
	 */
	private JTextArea textArea;
	
	/**
	 * Buffer, do kterého se ukládají byty mezi jednotlivými voláními
	 * {@link #flush()}.
	 */
	private ByteArrayOutputStream buffer = new ByteArrayOutputStream();

	public TextAreaOutputStream(JTextArea textArea) {
		this.textArea = textArea;
	}

	/**
	 * Připojí obsah bufferu vytvořeného voláním {@link #write(int)} na konce
	 * textového obsahu {@link JTextArea}.
	 * 
	 * @see java.io.OutputStream#flush()
	 */
	@Override
	public synchronized void flush() {
		String str = new String(buffer.toByteArray());
		buffer.reset();
		textArea.append(str);
		textArea.getDocument().getLength();
		textArea.setCaretPosition(textArea.getText().length());
	}

	/**
	 * Přidá byte do bufferu.
	 * 
	 * @see java.io.OutputStream#write(int)
	 */
	@Override
	public synchronized void write(int b) throws IOException {
		buffer.write(b);
	}
}