package lase.tools.tempgen;

import java.io.IOException;
import java.io.Reader;

import lase.template.BadTemplateException;
import lase.template.ReferenceToken;
import lase.template.Template;
import lase.template.TemplateCreator;
import lase.template.Token.Modifier;
import lase.tools.tempgen.Tokenizer.Token;

/**
 * Překladač šablon systému LASE. Generuje šablony ({@link Template}) z formátu
 * jednoduchého pro zápis
 * 
 * @author Rudolf Šíma
 * 
 */
public class Compiler {
	
	/**
	 * Ukončující token značící konec zdrojového textu.
	 */
	private static Token endToken = new Token("EOF", true);
	
	/**
	 * Token nového řádku.
	 */
	private static Token newLineToken = new Token("\n", true);
	
	/**
	 * Lexikální analyzátor.
	 */
	private Tokenizer analyser;
	
	/**
	 * Právě zpracovávaný token
	 */
	private Token curToken;
	
	/**
	 * Právě zpracovávaný řádek zdrojového textu.
	 */
	private int curLine = 1;
	
	/**
	 * Objekt, kterému jsou oznamovány chyby a předávány doknončené šablony.
	 */
	private Callback callback;
	
	/**
	 * Builder tokenů šablony.
	 */
	private TokenBuilder tokenBuilder = new TokenBuilder();
	
	/**
	 * Nástroj pro vytváření šablon.
	 */
	private TemplateCreator templateCreator = new TemplateCreator();

	/**
	 * Vytvoří nový překladač a nastaví objek, kterému jsou oznamovány chyby a
	 * předávány doknončené šablony.
	 * 
	 * @param callback
	 *            Objekt, kterému jsou oznamovány chyby a předávány doknončené
	 *            šablony.
	 */
	public Compiler(Callback callback) {
		this.callback = callback;
		
	}

	/**
	 * Provede překlad zdrojového textu.
	 * 
	 * @param input
	 *            Zdroj, ze kterého se čte zdrojový text.
	 */
	public void parse(Reader input)  {
		Preprocessor preprocessor = new Preprocessor(input); 
		analyser = new Tokenizer(preprocessor.getReader());
	
		readToken();
		try {
			start();
			expect(endToken.getValue(), endToken.isDelimiter());
		} catch (CompilationException e){
			// útěk z rekurze při chybě
		}
	}

	/**
	 * Zápíše dokončený token prostřednictvím nástroje pro vytváření šablon (
	 * {@link TemplateCreator}) do vytvářené šablony.
	 */
	private void flushToken() {
		lase.template.Token newToken;
		try {
			if ("ref".equals(tokenBuilder.type)) {
				try {
					int refId = Integer.parseInt(tokenBuilder.value);
					newToken = new ReferenceToken(refId);
				} catch (NumberFormatException e) {
					callback.handleSyntaxError(curLine, tokenBuilder.value);
					throw new CompilationException();
				}
			} else {
				newToken = lase.template.Token.newToken(0, tokenBuilder.type, tokenBuilder.value);
			}
			if (tokenBuilder.answer)
				newToken.addModifier(Modifier.ANSWER);
			if (tokenBuilder.optional)
				newToken.addModifier(Modifier.OPT);
		} catch (BadTemplateException e) {
			callback.handleSyntaxError(curLine, tokenBuilder.type);
			throw new CompilationException();
		}
		tokenBuilder.reset();
		templateCreator.addToken(newToken);
	}
	
	/**
	 * Odešle dokončenou šablonu posluchači.
	 */
	private void flushTemplate() {
		Template t = templateCreator.getTemplate();
		templateCreator = new TemplateCreator();
		callback.handleTemplate(t);
	}

	/**
	 * Načte token z lexikálního analyzátoru, nastaví proměnnou
	 * {@link #curToken} a inkrementuje {@link #curLine}.
	 */
	private void readToken()  {
		try {
			curToken = analyser.nextToken();
		} catch (IOException e) {
			callback.handleIOException(e);
		}
		if (curToken == null) {
			curToken = endToken;
		}
		if (newLineToken.matches(curToken))
			curLine++;
	}

	/**
	 * Standardní metoda {@code expect} rekurzivního sestupu.
	 * 
	 * @param value
	 *            Řetězec, který je očekáván (case insensitive).
	 * @param delimiter
	 *            Udává, zda je očekávaný řetězec oddělovačem.
	 */
	private void expect(String value, boolean delimiter) {
		if (!accept(value, delimiter)) {
			String curValue = curToken.getValue();
			String token = "\n".equals(curValue) ? "\\n" : curValue;
			callback.handleSyntaxError(curLine, token);
			throw new CompilationException();
		}
	}

	/**
	 * Standardní metoda {@code accept} rekurzivního sestupu.
	 * 
	 * @param value
	 *            Akceptovatelný řetězec (case insensitive).
	 * @param delimiter
	 *            Udává, zda je akceptovatelný řetězec oddělovačem.
	 * 
	 * @return Vrací {@code true}, byl-li řetězec akceptován. Jinak vrací
	 *         {@code false}.
	 */
	private boolean accept(String value, boolean delimiter) {
		Token token = new Token(value, delimiter);
		if ((value == null && delimiter == curToken.isDelimiter()) || curToken.matches(token)) {
			readToken();
			return true;
		}
		return false;
	}

	/**
	 * Otestuje nový řádek.
	 */
	@SuppressWarnings("unused")
	private void expectNewLine()  {
		expect("\n", true);
		acceptNewLine();
	}

	/**
	 * Akceptuje nový řádek.
	 * 
	 * @return Vrací {@code true} byl-li nový řádek nalezen a akceptován. Jinak
	 *         vrací {@code false}.
	 */
	private boolean acceptNewLine() {
		boolean result = accept("\n", true);
		while(accept("\n", true))
			;
		return result;
	}
	
	/**
	 * Spustí překald rekurzivním sesuptem.
	 */
	private void start()  {
		/* Testovací překladač, pro jazyk bez symbolu [template].
		/* Pro návrat k předchozímu odkomentovat následující 2 zakomenované řádky
		 * a odstranit 2 po nich následující. */
		// while (accept("[", true))
		// template();
		while (accept("\n", true))
			;
		while (accept("Q", false))
			template();
	}
	
	/**
	 * Přeloží jednu šablonu rekurzivním sestupem.
	 */
	private void template()  {
		/* Testovací překladač, pro jazyk bez symbolu [template].
		 * Pro návrat k předchozímu odkomentovat následující 3 řádky. */
		// expect("template", false);
		// expect("]", true);
		// expectNewLine();
		query();
		while(accept("A", false)){
			expect(":", true);
			templateCreator.newAnswerPattern();
			tokenList();
		}
		flushTemplate();
	}
	
	/**
	 * Přeloží vzor otázky rekurzivním sestupem.
	 */
	private void query() {
		/* Testovací překladač, pro jazyk bez symbolu [template].
		 * Pro návrat k předchozímu odkomentovat následující řádek. */
		//expect("Q", false);
		expect(":", true);
		tokenList();
	}
	
	/**
	 * Přeloží seznam tokenů (vzoru otázky nebo odpovědi) rekurzivním sestupem.
	 */
	private void tokenList() {
		while(!acceptNewLine()) {
			if (accept("[", true)) {
				optionalToken();
			} else {
				token();
			}
			flushToken();
		}
	}
	
	/**
	 * Přeloží jeden volitelný token rekurzivním sestupem.
	 */
	private void optionalToken() {
		token();
		expect("]", true);
		tokenBuilder.optional = true;
	}
	
	/**
	 * Přeloží jeden token rekurzivním sestupem.
	 */
	private void token() {
		if (accept("$", true)) {
			tokenBuilder.type = "ref";
			tokenBuilder.value = curToken.getValue();
			expect(null, false);
		} else {
			if (accept("@", true)) {
				tokenBuilder.type = curToken.getValue();
				expect(null, false);
				if (accept(":", true)) {
					tokenBuilder.value = curToken.getValue();
					expect(null, false);
				}
			} else {
				tokenBuilder.value = curToken.getValue();
				expect(null, false);
			}
		}
		if (accept("?", true))
			tokenBuilder.answer = true;
	}

	/**
	 * Rozhraní objektu, kterému jsou předávány dokončené šablony a který je
	 * informován o chybách v překladu nebo v načítání zdrojového textu.
	 * 
	 * @author Rudolf Šíma
	 * 
	 */
	public static interface Callback {

		/**
		 * Předáná dokončené šablony posluchači.
		 * 
		 * @param t
		 *            Dokončená šablona.
		 */
		public void handleTemplate(Template t);

		/**
		 * Informace o chybě ve zdrojovém textu.
		 * 
		 * @param line
		 *            Řádek, na kterém se vyskytla chyba ve zdrojovém textu.
		 * @param unexpectedToken
		 *            Neočekávaný token, který chybu způsobil.
		 */
		public void handleSyntaxError(int line, String unexpectedToken);

		/**
		 * Informace o vyhození výjimky {@link IOException} při čtení zdrojového
		 * textu.
		 * 
		 * @param e
		 *            Vyhozená výjimka.
		 */
		public void handleIOException(IOException e);
	}

	/**
	 * Struktura, do které jsou průběžně ukládána data potřebná k vytvoření
	 * jednoho tokenu šablony.
	 * 
	 * @author Rudolf Šíma
	 * 
	 */
	private static class TokenBuilder {
		
		/**
		 * Vytvoří nový prázný {@link TokenBuilder}.
		 */
		public TokenBuilder() {
			reset();
		}
		
		
		/**
		 * Řetězcový obsah tokenu šablony.
		 */
		String value;
		
		/**
		 * Řetězec určující typ tokenu šablony.
		 */
		String type;
		
		/**
		 * Udává, zda je token šablony volitelný.
		 */
		boolean optional;
		
		/**
		 * Udává, zda je token šablony odpovědí.
		 */
		boolean answer;

		/**
		 * Resetuje {@link TokenBuilder}, nastaví všechny atribute na výchozí
		 * hodnoty, které mají po vytvoření nové instance.
		 */
		void reset() {
			value = "";
			type = "word";
			optional = false;
			answer = false;
		}
	}
	
	/**
	 * Výjimka, která slouží k úniku z rekurze při chybě v překladu.
	 * 
	 * @author Rudolf Šíma
	 * 
	 */
	private static class CompilationException  extends RuntimeException {
		private static final long serialVersionUID = 1L;
	}
}
