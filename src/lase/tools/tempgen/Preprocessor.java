package lase.tools.tempgen;

import java.io.IOException;
import java.io.Reader;

/**
 * Preprocesor překladače šablon. Slouží k odstranění komentářů.
 * 
 * @author Rudolf Šíma
 *
 */
class Preprocessor {
	
	/**
	 * Zdroj, ze kterého se čte zdrojový text.
	 */
	private Reader input;

	/**
	 * Vytvoří nový preprocesor a nastaví zdroj zdrojového textu.
	 * 
	 * @param input
	 *            Zdroj, ze kterého se čte zdrojový text.
	 */
	public Preprocessor(Reader input) {
		this.input = input;
	}

	/**
	 * Vytvoří novou instanci zdroje preprocesorem zpracovaného zdrojového
	 * textu, který zajistí odstranění komentářů.
	 * 
	 * @return Vrací zdroj preprocesorem zpracovaného zdrojového textu.
	 */
	public Reader getReader() {
		return new PreprocessorReader(input);
	}
	
	/**
	 * Zdroj preprocesorem zpracovaného zdrojového textu. Zajišťuje
	 * ostranění komentářů.
	 * 
	 * @author Rudolf Šíma
	 *
	 */
	private static class PreprocessorReader extends Reader {
		
		/**
		 * Vstup preprocesoru.
		 */
		private Reader input;
		
		/**
		 * Je-li nastavena na {@code true}, jsou čtené znaky ignorovány.
		 */
		private boolean ignore;
		
		/**
		 * Udává, zda již bylo dosaženo konce vstupního proudu.
		 */
		private boolean eos;

		/**
		 * Vytvoří nový zdroj preprocesorem zpracovaného textu.
		 * 
		 * @param input
		 *            Zdroj, ze kterého se čte původní zdrojový text s
		 *            komentáři.
		 */
		public PreprocessorReader(Reader input) {
			this.input = input;
		}
		
		/**
		 * Uzavře zdroj, ze kterého se čte původní zdrojový text s komentáři.
		 */
		@Override
		public void close() throws IOException {
			input.close();
		}

		/**
		 * Naplní buffer znaky zdrojového textu, ze kterého jsou vynechány
		 * případné komentáře.
		 */
		@Override
		public int read(char[] cbuf, int off, int len) throws IOException {
			int ret, index, c;
			if (eos)
				return -1;
			for (index = 0, ret = 0; ret < len; ret++) {
				c = input.read();
				if (c == -1)
				{
					eos = true;
					if (ret > 0)
						return ret;
					return -1;
				}
				if (c == '#')
					ignore = true;
				else if (c == '\n')
					ignore = false;
				
				if (!ignore) {
					cbuf[index++] = (char) c;
				}
			}
			return ret;
		}
	}
}
