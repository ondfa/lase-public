package lase.websearch;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import lase.util.Corpus;
import lase.util.LaseException;

public class SeznamSnippetWebSearchTest {

	private static WebSearch web;
	private static Corpus corpus;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		web = new SeznamSnippetWebSearch();
		String[] strings = {"Karel Čapek"};
		corpus = web.search(strings, null);
	}

	@Test
	public void testSearchHasEntries() throws LaseException {
		assertTrue(corpus.size()>0);
	}
	
	@Test
	public void testSearchHasURL() throws LaseException {
		assertNotNull(corpus.getEntry(0).getSource().getUrl());
	}
	
	@Test
	public void testSearchHasTitle() throws LaseException {
		assertTrue(corpus.getEntry(0).getSource().getTitle().length()>0);
	}
	
	@Test
	public void testSearchHasText() throws LaseException {
		assertTrue(corpus.getEntry(0).getText().length()>0);
	}
	
	@Test
	public void testSearchHasURL2() throws LaseException {
		assertNotNull(corpus.getEntry(corpus.size()-1).getSource().getUrl());
	}
	
	@Test
	public void testSearchHasTitle2() throws LaseException {
		assertTrue(corpus.getEntry(corpus.size()-1).getSource().getTitle().length()>0);
	}
	
	@Test
	public void testSearchHasText2() throws LaseException {
		assertTrue(corpus.getEntry(corpus.size()-1).getText().length()>0);
	}

}
