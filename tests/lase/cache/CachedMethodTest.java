package lase.cache;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

@Deprecated
public class CachedMethodTest {

	@Test
	public void test() throws Exception {
		TestClass testObject = new TestClass();
		CachedMethod<String> method = new CachedMethod<String>(testObject, "method", Integer.TYPE, Double.TYPE, String.class);
		
		// 1. běh 
		for (int i = 0; i < 10; i++) {
			String value = method.invoke(i, (double)i, Integer.toString(i));
			System.out.println(value);
			assertEquals(true, testObject.readCalled());
		}
		
		// 2. běh
		for (int i = 0; i < 10; i++) {
			String value = method.invoke(i, (double)i, Integer.toString(i));
			System.out.println(value);
			assertEquals(false, testObject.readCalled());
		}
	}
	
	public static class TestClass {
		private boolean methodCalled;
		
		public String method(int arg1, double arg2, String arg3 ) {
			methodCalled = true;
			return String.format("(%d, %f, %s)", arg1, arg2, arg3);
		}
		
		public boolean readCalled() {
			boolean called = methodCalled;
			methodCalled = false;
			return called;
		}
	};
}