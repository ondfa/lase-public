package lase.answersearch;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import lase.util.Corpus;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AnswerGroupTest {
	
	private List<Answer> answers;
	private AnswerComparator comparator;

	/**
	 * Řadí tokeny typu TEXT podle prvního znaku. Tokeny lišící se v jiných znacích
	 * jsou vyhodnoceny jako neporovnatelné
	 */
	private static class TestComparator implements AnswerComparator {
		private static final long serialVersionUID = 1L;

		@Override
		public Precision compare(Answer o1, Answer o2) {
			if (o1 == null || o2 == null)
				return Precision.INCOMPARABLE;
			if (o1.size() != 1 || o2.size() != 1)
				throw new IllegalArgumentException();
			AnswerToken t1 = o1.getToken(0);
			AnswerToken t2 = o2.getToken(0);
			if (t1.getType() != AnswerToken.Type.TEXT || t1.getType() != AnswerToken.Type.TEXT)
				throw new IllegalArgumentException();
			String s1 = t1.getText();
			String s2 = t2.getText();
			if (s1.length() < 1 || s1.length() < 1)
				throw new IllegalArgumentException();
			char c1 = s1.charAt(0);
			char c2 = s2.charAt(0);
			Precision firstCharVerdict;
			if (c1 > c2)
				firstCharVerdict = Precision.LESS_PRECISE;
			else if (c1 < c2)
				firstCharVerdict = Precision.MORE_PRECISE;
			else
				firstCharVerdict = Precision.EQUAL;
			String subStr1 = s1.substring(1);
			String subStr2 = s2.substring(1);
			if (subStr1.equals(subStr2))
				return firstCharVerdict;
			else
				return Precision.INCOMPARABLE;
		}
		
	}

	@Before
	public void setUp() throws Exception {
		Answer[] ansArr = new Answer[] {
				createSimpleAnswer("a1"),
				createSimpleAnswer("b3"),
				createSimpleAnswer("a2"),
				createSimpleAnswer("c1"),
				createSimpleAnswer("b1"),
				createSimpleAnswer("a4"),
				createSimpleAnswer("a1"),
				createSimpleAnswer("a3"),
				createSimpleAnswer("c1"),
				createSimpleAnswer("b3"),
				createSimpleAnswer("c3"),
				createSimpleAnswer("f3")
		};
		answers = Arrays.asList(ansArr);
		comparator = new TestComparator();
	}

	@After
	public void tearDown() throws Exception {
	}
	
	private static Answer createSimpleAnswer(String text) {
		return new Answer(Arrays.asList(new AnswerToken[] {new TextAnswerToken(text)}), new Corpus.SourcePage("", ""));
	}
	
	@Test
	public void sortAnswersTest() {
		List<AnswerGroup> answerGroups = AnswerGroup.sortAnswers(answers, comparator);
		assertEquals(4, answerGroups.size());
		Collections.sort(answerGroups, new SizeComparator(true));
		assertEquals(answerGroups.get(0).getBestAnswer().getToken(0).getText(), "a3");
	}
}
