package lase.answersearch;

import static org.junit.Assert.assertEquals;

import java.util.Iterator;
import java.util.List;

import lase.morphology.CharFilter;
import lase.util.Corpus;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.varia.NullAppender;
import org.junit.BeforeClass;
import org.junit.Test;

public class SimpleCorpusTokenizerTest {	
	
	private CharFilter charFilter = CharFilter.getPreferredFilter();
	
	@BeforeClass
	public static void init() {
		BasicConfigurator.configure(new NullAppender());
	}
	
	@Test
	@Deprecated
	public void tokenizerTest() {
		String testStr = charFilter.substitute("Karel IV. (14. května 1316, Praha – 29. listopadu 1378, Praha)");
		Corpus testCorpus = new Corpus();
		testCorpus.addPart(testStr, "", "");
		 
		CorpusTokenizer modelTokenizer = new MorphWordCorpusTokenizer(testCorpus);
		CorpusTokenizer testedTokenizer = new SimpleCorpusTokenizer(testCorpus);
		
		while (modelTokenizer.hasNext()) {
			List<String> modelTokens = modelTokenizer.next();
			List<String> testedTokens = testedTokenizer.next();
			assertEquals(modelTokens.size(), testedTokens.size());
			
			Iterator<String> it = testedTokens.iterator();
			for (String modelToken : modelTokens) {
				String t = it.next();
				//System.out.println(modelToken + " " + t);
				assertEquals(modelToken, t);
			}
		}
	}
}