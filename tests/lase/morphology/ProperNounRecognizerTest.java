package lase.morphology;

import static org.junit.Assert.assertEquals;

import java.util.List;

import lase.app.LaseApp;
import lase.app.SharedObjects;
import lase.morphology.ProperNounRecognizer.ProperNoun;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

@Deprecated
public class ProperNounRecognizerTest {
	private ProperNounRecognizer recognizer;
	private MorphologicalAnalyser morphAnal; 
	private Tokenizer tokenizer;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		LaseApp.initializeDefaults();
	}
	
	@Before
	public void setUp() throws Exception {
		recognizer = new ProperNounRecognizer();
		morphAnal = SharedObjects.getMorphologicalAnalyser();
		tokenizer = SharedObjects.getTokenizer();
	}

	@Test
	public void testAnalyse() throws Exception {
		String s = "Rudolf Šíma (21.5.1968—20.9.2011) je taký týpek, co neudělal státnice.";
		List<String> tokens = tokenizer.tokenize(s);
		List<MorphWord> morphWords = morphAnal.analyse(tokens);
		List<ProperNoun> properNouns = recognizer.analyse(morphWords);
		for (ProperNoun pn : properNouns) {
			System.out.println(pn);
		}
		assertEquals(1, properNouns.size());
		assertEquals("Rudolf Šíma", properNouns.get(0).getWords());
	}

}
