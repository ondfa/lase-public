package lase.template;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import lase.template.NamedEntityToken.Subtype;
import lase.template.Token.Modifier;

import org.apache.log4j.Logger;
import org.apache.log4j.varia.NullAppender;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class SearchPatternTest {
	private List<Query> queries = new ArrayList<Query>();
	private SearchPattern pattern;
	private NamedEntitySearch nes1;
	private NamedEntitySearch nes2;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Logger.getRootLogger().addAppender(new NullAppender());
	}
	
	@Before
	public void setUp() throws Exception {
		pattern = new SearchPattern();
		pattern.add(new QueryToken(new WordToken(1, "číslo")));
		Token answer = new NamedEntityToken(2, Subtype.NUMBER);
		answer.addModifier(Modifier.ANSWER);
		pattern.add(new QueryToken(answer, "", null));
		pattern.add(new QueryToken(new WordToken(3, "znamená")));
		
		List<QueryToken> query = new ArrayList<QueryToken>();
		query.add(new QueryToken(new WordToken(0, "číslo")));
		answer = new NamedEntityToken(0, Subtype.NUMBER);
		query.add(new QueryToken(answer, "2", 2));
		query.add(new QueryToken(new WordToken(0, "znamená")));
		queries.add(new Query(query));
		
		query = new ArrayList<QueryToken>();
		query.add(new QueryToken(new WordToken(0, "číslo")));
		answer = new NamedEntityToken(0, Subtype.PERSON);
		query.add(new QueryToken(answer, "Petr Parléř", null));
		query.add(new QueryToken(new WordToken(0, "znamená")));
		queries.add(new Query(query));
		
		nes1 = new NamedEntitySearch("číslo 7 znamená");
		nes2 = new NamedEntitySearch("číslo 12:00 znamená");	
	}
	
	@Test
	public void testMatchQueries1() {
		TemplateMatchResult[] matchResults = pattern.matchQueries(queries);
		assertEquals(1, matchResults.length);
		
		Map<Token, QueryToken> map = matchResults[0].getTokenMap();
		for (QueryToken qt : pattern) {
			if (qt.getToken().hasModifier(Modifier.ANSWER)) {
				QueryToken t = map.get(qt.getToken());
				assertEquals("2", t.getOriginalWord());
			}
		}
	}
	
	@Test
	public void testMatchQueries2() {
		TemplateMatchResult[] matchResults = pattern.matchQueries(nes1);
		assertEquals(1, matchResults.length);
		
		Map<Token, QueryToken> map = matchResults[0].getTokenMap();
		for (QueryToken qt : pattern) {
			if (qt.getToken().hasModifier(Modifier.ANSWER)) {
				QueryToken t = map.get(qt.getToken());
				assertEquals("7", t.getOriginalWord());
			}
		}
	}
	
	// TODO - selže -- opravit test ;)
	@Test
	public void testMatchQueries3() {
		TemplateMatchResult[] matchResults = pattern.matchQueries(nes2);
		assertEquals(0, matchResults.length);
	}

	public void testGetSearchStrings() {
	
	}
}
